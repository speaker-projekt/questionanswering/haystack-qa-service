import json
import os
import re
import configparser
from pathlib import Path

from qa_pipeline.monolingual_qa_pipeline import MonolingualQAPipeline
from qa_pipeline.multilingual_qa_pipeline import MultilingualQAPipeline
from utils import start_es_instance, load_model


def initialize_qa_pipeline(pipeline_type: str):
    """
    Initializes basic monolingual QA pipeline with global parameters.
    :param pipeline_type: type of the pipeline ("monolingual", "multilingual")
    :return: (Initialized QA pipeline, status message)
    """
    config = configparser.ConfigParser()
    config.read('qa_pipeline/pipeline_config.ini')
    data_path = config[pipeline_type]['data_path']
    model_path = config[pipeline_type]['model_path']

    if not os.listdir(data_path):  # if empty
        return None, f'QA pipeline NOT initialized: no data in the data folder\n'
    if not Path(model_path).exists():
        return None, "QA pipeline NOT initialized: no model located, you can loaded with " \
                     "/load_model request"  #

    start_es_instance()
    print('Pipeline Type: ',pipeline_type)
    if pipeline_type == "monolingual":
        qa_pipeline = MonolingualQAPipeline(model_path,
                                            config[pipeline_type]['retriever'],
                                            config[pipeline_type]['lang'])
    else:
        qa_pipeline = MultilingualQAPipeline(model_path)
    qa_pipeline.populate_document_store(data_path)
    return qa_pipeline, "QA pipeline initialized successfully"


def filter_prediction_by_lang(prediction, langs):
    filtered_prediction = {}
    for lang in langs:
        filtered_prediction[lang] = []
    for answer_data in prediction['answers']:
        answer_lang = answer_data['meta']['lang']
        if answer_lang in langs:
            filtered_prediction[answer_lang].append(answer_data)
    return filtered_prediction


class ServiceQA:
    """A class-wrapper designed to interact with the QA pipeline"""
    def __init__(self):
        self.qa_pipeline = None
        self.pipeline_type = None

    def init_with_local_data(self, pipeline_type):
        """
        Initializes the QA pipeline with local data.
        :return: status message, str
        """
        self.pipeline_type = pipeline_type
        self.qa_pipeline, msg = initialize_qa_pipeline(pipeline_type)
        return f'{msg} \n'

    def set_document_languages(self, langs: str):
        if not self.pipeline_type == "multilingual":
            return f"It is possible to set desired document languages for multilingual pipelines only." \
                   f" Current pipeline type is {self.pipeline_type}"
        lang_list = re.findall(r'\w+', langs)
        if lang_list:
            lacking_langs = self.qa_pipeline.set_document_languages(lang_list)
            if not lacking_langs:
                return f"The system will look into documents on the following languages: {', '.join(lang_list)}.\n"
            else:
                return f"The system will look into documents on the following languages:" \
                       f" {', '.join((set(lang_list) - set(lacking_langs)))}. " \
                       f"There is no documents on languages {', '.join(lacking_langs)}\n"
        else:
            return f'The provided input is invalid. Correct format: \"en, de, es\" (for example). \n'

    def predict_answer(self, query, verbose=False):
        """
        Predicts answers for the given query.
        :param query: user query containing question
        :param verbose: defines should answers be returned with all metadata or not
        :return: answer predictions with metadata in json format or an error message
        """
        if self.qa_pipeline:
            if not query:
                return 'No query was provided.\n'
            predictions = self.qa_pipeline.predict(query)
            print("Predictions: ", predictions)
            if not verbose and self.pipeline_type == "multilingual":
                essential_predictions = []
                for lang, answers in predictions.items():
                    for answer in answers:
                        #essential_predictions.append({"answer": answer["answer"],"score": answer["score"],"lang": lang})
                        print("Answer: ", answer)
                        #essential_predictions.append({"answer": answer.answer,"score": answer.score,"lang": lang})
                        #essential_predictions.append({"answer": answer.answer,"score": answer.score,"lang": lang,"context":answer.context,"document_id":answer.document_id})
                        essential_predictions.append({"answer": answer.answer,"score": answer.score,"lang": lang,"context":answer.context,"document_id":answer.document_ids[0]})
                    ranked_predictions = sorted(essential_predictions, reverse=True, key=lambda ans: ans["score"])
                    return json.dumps(ranked_predictions)
            else:
                return json.dumps(predictions)
        else:
            return 'The QA pipeline is NOT initialized: try uploading the documents again.\n'


#if __name__ == '__main__':
#    service = ServiceQA()
#    msg = load_model('multilingual')
#    msg = service.init_with_local_data('multilingual')


