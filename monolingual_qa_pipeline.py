import configparser
#from haystack.document_store.elasticsearch import ElasticsearchDocumentStore
from haystack.document_stores import ElasticsearchDocumentStore
#from haystack.preprocessor.cleaning import clean_wiki_text
from haystack.utils import clean_wiki_text
#from haystack.preprocessor.utils import convert_files_to_dicts
from haystack.utils import convert_files_to_docs
from haystack.pipelines import ExtractiveQAPipeline
from haystack.nodes import ElasticsearchRetriever
from haystack.nodes import DensePassageRetriever
from haystack.nodes import FARMReader


class MonolingualQAPipeline:
    """A class describing the basic monolingual Question Answering Pipeline based on deepset's Haystack framework"""
    def __init__(self, reader_model_name_or_path: str, retriever: str, lang: str):
        """
        :param reader_model_name_or_path: directory of a saved model or the name of a public model
        :param retriever: type of the retriever (dense or sparse)
        :param lang: name of the language the documents are written in (influences tokenization)
        """
        # if run outside Docker, host="localhost"
        self.document_store = ElasticsearchDocumentStore(host="elasticsearch", username="", password="",
                                                         index="document",
                                                         analyzer=lang)
        if retriever == 'sparse':
            self.retriever = ElasticsearchRetriever(document_store=self.document_store)
        elif retriever == 'dense':
            config = configparser.ConfigParser()
            config.read('qa_pipeline/pipeline_config.ini')
            query_emb_model = config['DPR_models']['query_embedding_model']
            passage_emb_model = config['DPR_models']['passage_embedding_model']
            self.retriever = DensePassageRetriever(document_store=self.document_store,
                                                   query_embedding_model=query_emb_model,
                                                   passage_embedding_model=passage_emb_model)
        else:
            raise Exception(f"retriever argument can be either 'dense' or 'sparse'."
                            f" The value of retriever is {retriever}")
        # the reader model can work on CPU, but it is recommended that they are run using GPUs to keep query times low
        self.reader = FARMReader(model_name_or_path=reader_model_name_or_path)
        self.pipe = ExtractiveQAPipeline(self.reader, self.retriever)

    #
    def populate_document_store(self, data_path: str):
        """
        Populates the document store with documents located in the folder
        :param data_path: path to documents, str
        """
        self.clear_document_store()
        #doc_dicts = convert_files_to_dicts(dir_path=data_path, clean_func=clean_wiki_text, split_paragraphs=True)
        doc_dicts = convert_files_to_docs(dir_path=data_path, clean_func=clean_wiki_text, split_paragraphs=True)
        self.document_store.write_documents(doc_dicts)
        if isinstance(self.retriever, DensePassageRetriever):
            self.document_store.update_embeddings(self.retriever)

    def clear_document_store(self):
        """
        Clears the document index.
        """
        self.document_store.delete_all_documents(index="document")

    def predict(self, query: str) -> dict:
        """
        Calculates predictions for the given user query (question + filters (TBD))
        :param query: user query containing question
        :return: predictions, dict containing answers along with prediction metadata
        """
        prediction = self.pipe.run(query=query)  # , top_k_retriever=10, top_k_reader=5)
        return prediction.get("answers", [])
