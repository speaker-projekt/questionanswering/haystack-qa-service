FROM python:3.7-stretch
LABEL maintainer="julian.moreno_schneider@dfki.de"

RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y


COPY requirements.txt .
RUN pip3 install -r requirements.txt

# WORKDIR /local_models
# RUN python load_model.py

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ADD . .
# EXPOSE 8080
WORKDIR /
VOLUME /data/user_data
ENTRYPOINT FLASK_APP=flask_controller.py flask run --host=0.0.0.0 --port=5000
#CMD ["/bin/bash"]

