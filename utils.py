import subprocess
import time
import os
from haystack.nodes import FARMReader
from pathlib import Path


# mapping of ISO 69-1 language codes to language names (supported by elasticsearch analyzer)
iso_to_language = {"ar": "arabic", "hy": "armenian", "eu": "basque", "bn": "bengali",
                   "bg": "bulgarian", "ca": "catalan", "cs": "czech", "da": "danish", "nl": "dutch",
                   "en": "english", "et": "estonian", "fi": "finnish", "fr": "french",
                   "gl": "galician", "de": "german", "el": "greek",
                   "hi": "hindi", "hu": "hungarian", "id": "indonesian", "ga": "irish",
                   "it": "italian", "lv": "latvian", "lt": "lithuanian", "no": "norwegian",
                   "fa": "persian", "pt": "portuguese", "ro": "romanian", "ru": "russian",
                   "es": "spanish", "sv": "swedish", "tr": "turkish", "th": "thai"}


def start_es_instance():
    """
    Starts ElasticSearch server instance.
    """
    status = subprocess.run(
        ['docker run -d -p 9200:9200 -e "discovery.type=single-node" elasticsearch:7.9.2'], shell=True)
    if status.returncode:
        print("Failed to launch Elasticsearch: an Elasticsearch instance exists")
        return
    time.sleep(15)


def load_model(lang: str) -> str:
    """
    Loads an extractive (reader) model for the provided language (german or multilingual for the moment).
    :param lang: language in form "lg".
    :return: a message containing the status, str
    """
    if lang == "de":
        save_path = "local_models/gelectra"
        model_name = "deepset/gelectra-large-germanquad"
    else:
        save_path = "local_models/roberta"
        model_name = "deepset/gelectra-large-germanquad"
    if Path(save_path).exists():
        return f'ERROR: model {model_name} for language -{lang}- already loaded'
    FARMReader(model_name_or_path=model_name, use_gpu=False).save(save_path)
    return f'Model {model_name} for language -{lang}- downloaded'
