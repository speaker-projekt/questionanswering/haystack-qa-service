Tucson [ˈtuːsɑn] ist eine Stadt im US-Bundesstaat Arizona. Die Stadt ist Verwaltungssitz (County Seat) von Pima County. Die Einwohnerzahl beträgt ca. 530.000 (Schätzung 2016, U.S. Census Bureau); in der Metropolregion Tucson leben über 1 Million Einwohner. Tucson ist die größte Stadt im südlichen Arizona und nach Phoenix die zweitgrößte in Arizona.
Der Name Tucson entstammt der Sprache der Tohono-O’Odham-Indianer. Ihr Name des Gebiets, Chuk Shon, bedeutet „am Fuß des schwarzen Berges“ (angelehnt an den Sentinel Peak in der Nähe von Downtown Tucson).


== Geschichte ==
Tucson war bereits um 12.000 v. Chr. durch Paläoindianer besiedelt. Nahe dem heutigen Stadtzentrum wurden Reste eines Dorfes gefunden, das hier etwa 1000 v. Chr. stand. Zwischen 1200 v. Chr. und 150 n. Chr. wurde die Gegend um Tucson landwirtschaftlich genutzt. Die Stadt gehört damit zu den am längsten fortwährend besiedelten Orten der USA. Die damaligen Einwohner bauten Bewässerungskanäle, um die Mais- und Bohnenfelder zu bewirtschaften. Zwischen 600 und 1450 n. Chr. siedelte die Hohokam-Kultur in diesem Gebiet.
Die Mission San Xavier del Bac wurde nahe dem heutigen Tucson im Jahr 1699 durch den italienischen jesuitischen Missionar Pater Eusebio Francisco Kino gegründet. Die Stadt wurde schließlich am 20. August 1775 mit dem Bau eines Presidios (Festung) unter Leitung von Hugo O’Conor durch die Spanier gegründet. Nach der Unabhängigkeit Mexikos von Spanien 1821 gehörte Tucson zu Mexiko. Mit dem Gadsden-Kauf fiel die Stadt 1853 an die Vereinigten Staaten. Von 1867 bis 1877 war Tucson die Hauptstadt des Arizona-Territoriums. 1885 wurde hier die University of Arizona gegründet. Nach dem Zweiten Weltkrieg ließen sich hier viele ehemalige Soldaten nieder, und mit dem Aufkommen von Klimaanlagen zogen immer mehr Menschen nach Tucson. Die Bevölkerung wuchs von 8000 Einwohnern (1880) auf 120.000 (1950) und 220.000 (1960).


=== Einwohnerentwicklung ===
¹ 1980–2010: Volkszählungsergebnisse;


== Geographie ==

Die Stadt liegt in einer Hochebene, umgeben von fünf bis zu 2880 m hohen Bergketten, den Santa Catalina Mountains und Tortolita Mountains im Norden, den Santa Rita Mountains im Süden, den Rincon Mountains im Osten sowie den Tucson Mountains im Westen. Die Stadt liegt am Santa Cruz River, welcher bis auf regenreiche Wochen während des Sommer-Monsuns ausgetrocknet ist.
Die am nächsten gelegenen größeren Städte sind Phoenix (Arizona) im Norden, El Paso (Texas) im Osten, die Schwesterstädte Nogales (Arizona) und Nogales (Sonora) im Süden, sowie San Diego (Kalifornien) im Westen.


== Wirtschaft ==

Die wichtigsten Arbeitgeber der Stadt sind neben der University of Arizona die Kreis- und Stadtverwaltung, die Elektronikindustrie, die Verteidigungs- und Luftfahrtindustrie sowie der Service- und Tourismussektor.
Zu den größten privaten Arbeitgebern in Tucson zählen Raytheon Missile Systems, Texas Instruments, IBM, Intuit Inc., America Online, Universal Avionics, Misys Healthcare Systems sowie Bombardier. Seit dem 6. Juli 2007 gibt es hier auch eine Niederlassung der „Solon America Corporation“. Erklärtes Ziel des Unternehmens ist die Umwandlung des Staates Arizona zum „Solar Valley“ der Vereinigten Staaten.
Die 3,5 Millionen Touristen jährlich bringen Einnahmen von etwa 2,2 Mrd. US-Dollar.
Ein weiterer wichtiger Wirtschaftsfaktor ist der Militärflugplatz der US-Air Force.
Die Arbeitslosenrate betrug im Juni 2010 8,6 %, nachdem sie 2007 noch bei 3,7 % lag. 2007 lag das durchschnittliche Pro-Kopf-Einkommen bei 26.732 US-Dollar.


== Verkehr ==

Tucson ist über die Autobahn I-10 im Osten mit New Mexico bzw. El Paso (Texas) verbunden und im Westen mit Phoenix bzw. Los Angeles und San Diego (I-8). Richtung Süden führt die Interstate 19 nach Nogales, (Arizona/Mexiko) bzw. Hermosillo (Sonora, Mexiko).
Die Stadt besitzt einen internationalen Flughafen, den Tucson International Airport, der jährlich von rund 1,7 Millionen Passagieren genutzt wird (2012).Außerdem gibt es einen Bahnhof, der dreimal wöchentlich vom Zug Sunset Limited der Amtrak in Richtung New Orleans bzw. Los Angeles bedient wird.
In Tucson gab es eine historische Straßenbahn, den Old Pueblo Trolley, welcher an Wochenenden den Campus der University of Arizona mit der Kneipenmeile 4th Avenue verband.
Im Mai 2006 hat die Bevölkerung von Tucson dem Vorschlag der Stadtregierung zugestimmt, ein modernes Straßenbahnnetz zu bauen. Die neue Straßenbahnstrecke führt auf einer Länge von 6,3 Kilometern von der Innenstadt über die 4th Avenue und die Universität bis zum Universitätskrankenhaus. Die Aufnahme des Regelbetriebes erfolgte am 25. Juli 2014. Der Betrieb der historischen Straßenbahn Old Pueblo Trolley wurde mit dem Baubeginn der neuen Straßenbahn im Oktober 2011 nach über 18 Jahren eingestellt. Die Betreiberfirma Old Pueblo Trolley, Inc. verhandelt mit der Stadt Tucson über einen möglichen Parallelbetrieb historischer Fahrzeuge mit der neuen Bahn.Die Firma Suntran betreibt ein relativ dichtes Busnetz.


== Bildung ==

Tucson ist Sitz der University of Arizona, der ältesten und zweitgrößten Universität in Arizona. Die Universität wurde 1885 gegründet und hat heute etwa 37.000 Studenten.
Außerdem gibt es das Pima Community College mit ca. 75.000 Studenten, welches zweijährige Studiengänge anbietet.


== Kultur ==
In Tucson sind die Arizona Opera Company, das Tucson Symphony Orchestra, das Southern Arizona Symphony Orchestra, der Tucson Boys Chorus, der Tucson Girls Chorus, sowie das Civic Orchestra of Tucson beheimatet.
Es gibt drei Theater, die Arizona Theatre Company, das Invisible Theatre und das Gaslight Theatre. Der Laff's Comedy Club ist das einzige Kabarett der Stadt.
Zu den wichtigsten Museen der Stadt gehören die Arizona Historical Society, das Tucson Museum of Art mit 6.000 Exponaten unter anderem aus prä-hispanischer Zeit, das University of Arizona Art Museum mit Kunstwerken von u. a. Franz Kline, Jackson Pollock und Mark Rothko, das Center of Create Photography und das Museum of Contemporary Art.
Außerhalb der Stadt befindet sich das Arizona-Sonora Desert Museum, das Pima Air Museum und das Titan Missile Museum.
Der National Park Service weist für Tucson 179 Bauwerke und Stätten aus, die im National Register of Historic Places (NRHP) eingetragen sind (Stand 24. November 2018).


=== Regelmäßige Veranstaltungen ===
Einer der Höhepunkte im Jahreskalender Tucsons ist die „Fiesta de los Vaqueros“ (die Feier der Cowboys), die auch als Tucson Rodeo bekannt ist. Für die Veranstaltung wird ein Preisgeld von mehreren Hunderttausend Euro ausgelobt, um das die Bezwinger vor täglich zehntausenden Besuchern aus dem ganzen Land kämpfen. Das Tucson Rodeo fand im Jahr 1925 zum ersten Mal statt und gilt heute als einer der Top-Wettbewerbe im professionellen Rodeosport.


== Nachtleben ==
Das studentische Nachtleben findet vor allem in den Bars auf dem University Boulevard statt, unmittelbar am Haupteingang zum Campus. Hier wird auch oft Livemusik gespielt. Die Kneipenmeile von Tucson ist die 4th Avenue, die man von der Universität mit der Straßenbahn erreichen kann. Folgt man der 4th Avenue Richtung Innenstadt, kommt man auf die Congress Street, wo es weitere Kneipen und Clubs, wie etwa den Club Congress, gibt. Feinere Etablissements findet man vor allem in den Foothills, beispielsweise in der La Encantada Shopping Mall.
Es gibt zahlreiche Kinos in der Stadt, die in der Regel Blockbuster-Filme zeigen. Das Loft Cinema auf dem Speedway wenige Kilometer östlich der Universität ist auf alternative und internationale Filme spezialisiert. Jedes Jahr im April findet ein internationales Film-Festival statt.
Informationen über aktuelle Veranstaltungen kann man der kostenlosen „Tucson Weekly“ entnehmen, die in den meisten Bars und Restaurants ausliegt.


== Sehenswürdigkeiten ==
Das Arizona-Sonora Desert Museum im Nordwesten der Stadt ist ein Zoo und Botanischer Garten. Es beherbergt nahezu alle Pflanzen und Tiere, die in der Sonora-Wüste vorkommen.
Angegliedert an die Davis-Monthan Air Force Base ist das 309th Aerospace Maintenance and Regeneration Group (AMARG), in dessen Bereich rund 4.200 Militärflugzeuge aller Art eingemottet sind bzw. durch Ersatzteilgewinnung verwertet werden.
Im Nordosten der Stadt bei Oro Valley befindet sich die öffentlich zugängliche Biosphäre 2.
Der als Arboretum ausgewiesene Campus der University of Arizona mit seinen zahlreichen seltenen Bäumen und Pflanzen, sowie verschiedenen Museen und Veranstaltungsstätten.
Die Mission San Xavier del Bac, eine alte spanische Missionskirche aus dem 17. Jahrhundert (etwa 16 km südlich der Stadt).
Die berühmten Old Tucson Studios im Nordwesten Tucsons waren lange Zeit Drehort vieler Westernfilme und Fernsehserien; heute dienen sie als Touristenattraktion mit täglichen Shows und Events.
Das Pima Air Museum mit einer Vielzahl historischer Militärflugzeugen (u. a. Lockheed SR-71 „Blackbird“). Von dort aus kann das AMARC besucht werden.
Nordwestlich und östlich der Stadt befindet sich der Saguaro-Nationalpark mit seinen Riesenkakteen (Carnegiea gigantea).
Südlich von Tucson liegt das Titan Missile Museum.
Ted De Grazia Gallery in the Sun Swan Street
John Charles Fremont House in Downtown Tucson


== Stadtteile ==
Colonia Solana
El Encanto
El Rio
Emery Park
Sam Hughes


== Städtepartnerschaften ==
Griechenland Trikala, Griechenland
Irak Sulaimaniyya, Irak
Irland County Roscommon, Irland
Kasachstan Almaty, Kasachstan
Mexiko Ciudad Obregón, Mexiko
Mexiko Guadalajara, Mexiko
Spanien Segovia, Spanien
Ungarn Pécs, Ungarn
Italien Fiesole, Italien


== Persönlichkeiten ==


=== Söhne und Töchter der Stadt ===
Chris-Pin Martin (1893–1953), Schauspieler
Robert McBride (1911–2007), Komponist
Lalo Guerrero (1916–2005), Sänger, Gitarrist und Songwriter
Zenna Henderson (1917–1983), Schriftstellerin
Ulysses Kay (1917–1995), Komponist
Robert Abernathy (1924–1990), Science-Fiction-Autor
Bill Cheesbourg (1927–1995), Autorennfahrer
William Docker Browning (1931–2008), Bundesrichter
Barbara Eden (* 1931), Schauspielerin
Dennis DeConcini (* 1937), Politiker
Keith Vreeland (1937/38–2018), Jazzmusiker und Hochschullehrer
Larry Pine (* 1945), Schauspieler
Linda Ronstadt (* 1946), Sängerin
Raúl Grijalva (* 1948), Politiker
Tom Udall (* 1948), Politiker
Thad W. Allen (* 1949), Admiral
Ken Hom (* 1949), Koch und Autor
Mark Udall (* 1950), Politiker
Rico Saccani (* 1952), Dirigent
W. Hugh Woodin (* 1955), Mathematiker
Scott M. Lanyon (* 1956), Ornithologe
Raymond Pettibon (* 1957), Künstler
Douglas J. Cuomo (* 1958), Komponist
Brad Johnson (* 1959), Schauspieler
Chris Cacavas (* 1961), Sänger und Songwriter
Anthony James Corcoran (* 1963), katholischer Ordensgeistlicher und Apostolischer Administrator von Kirgisistan
Jim Grabb (* 1964), Tennisspieler
Tony Malaby (* 1964), Musiker
Sean Elliott (* 1968), Basketballspieler
Gabrielle Giffords (* 1970), Politikerin
Savannah Guthrie (* 1971), Journalistin und Fernsehmoderatorin
Sharon Leal (* 1972), Schauspielerin
Tiffany Lott-Hogan (* 1975), Leichtathletin, Siebenkämpferin
Taryn Manning (* 1978), Sängerin und Schauspielerin
Angélica Celaya (* 1982), US-amerikanisch-mexikanische Schauspielerin
Cate Hall (* 1983), Pokerspielerin
Lacey Nymeyer (* 1985), Schwimmerin
Ryan Kalil (* 1985), Footballspieler
Kaylee DeFer (* 1986), Schauspielerin
Mika Boorem (* 1987), Schauspielerin
Martin Spanjers (* 1987), Schauspieler
Brooks Reed (* 1988), Footballspieler
Will Claye (* 1991), Weit- und Dreispringer
Dominic Janes (* 1994), Schauspieler
Amanda Benson (* 1995), Volleyballspielerin


=== Berühmte Bewohner der Stadt ===
Harley Brown (* 1939), Porträtmaler und Illustrator; Mitglied der Tucson 7
Duane Bryers (1911–2012), Maler und Illustrator; Mitglied der Tucson 7
Noam Chomsky (* 1928), Linguist und Philosoph, Professor an der University of Arizona
Don Crowley (* 1926), Maler und Illustrator; Mitglied der Tucson 7
Rich Hopkins (* 1958), Gitarrist und Songwriter
Otto Krayer (1899–1982), Arzt, Pharmakologe und Universitätsprofessor
Robert „Bob“ Kuhn (1920–2007), Maler und Illustrator; Mitglied der Tucson 7
Paul McCartney (* 1942), Musiker und Sänger, Mitglied der Beatles
Lute Olson (1934–2020), erfolgreicher Basketballmanager
Howe Gelb, Musiker mit Vorfahren aus Österreich; mastermind von Giant Sand
Ken Riley (1919–2015), Maler und Illustrator; Mitglied der Tucson 7
Gordon Tullock (1922–2014), Professor für Rechtswissenschaft und Volkswirtschaft, Ökonom


== Klimatabelle ==


== Weblinks ==

City of Tucson
University of Arizona
Tucson International Airport
Veranstaltungmagazin Tucson
San Xavier del Bac Mission
Arizona Sonora Desert Museum – Zoo und Botanischer Garten bei Tucson, Arizona (englisch)
PIMA Air Museum


== Literatur ==
Michael F. Logan: Desert Cities: The Environmental History of Phoenix and Tucson. University of Pittsburgh Press, Pittsburgh 2006, ISBN 978-0-8229-4294-8.


== Einzelnachweise ==