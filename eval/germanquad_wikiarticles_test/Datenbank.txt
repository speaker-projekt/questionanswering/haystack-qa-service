Eine Datenbank, auch Datenbanksystem genannt, ist ein System zur elektronischen Datenverwaltung. Die wesentliche Aufgabe einer Datenbank ist es, große Datenmengen effizient, widerspruchsfrei und dauerhaft zu speichern und benötigte Teilmengen in unterschiedlichen, bedarfsgerechten Darstellungsformen für Benutzer und Anwendungsprogramme bereitzustellen.
Eine Datenbank besteht aus zwei Teilen: der Verwaltungssoftware, genannt Datenbankmanagementsystem (DBMS), und der Menge der zu verwaltenden Daten, der Datenbank (DB) im engeren Sinn, zum Teil auch „Datenbasis“ genannt. Die Verwaltungssoftware organisiert intern die strukturierte Speicherung der Daten und kontrolliert alle lesenden und schreibenden Zugriffe auf die Datenbank. Zur Abfrage und Verwaltung der Daten bietet ein Datenbanksystem eine Datenbanksprache an.
Die gebräuchlichste Form einer Datenbank ist eine relationale Datenbank. Die Struktur der Daten wird durch ein Datenbankmodell festgelegt.

Zu unterscheiden ist der hier beschriebene Begriff der Datenbank (bestehend aus DBMS und Daten) von Datenbankanwendungen: Letztere sind (häufig zur Anwendungssoftware gehörende) Computerprogramme, die ihre jeweils individuell erforderlichen Daten unter Nutzung eines Datenbanksystems verwalten und speichern. Beispiele: Auftragsverwaltung, Bestellwesen, Kunden- und Adressverwaltung, Rechnungserstellung.
Im Sprachgebrauch werden gelegentlich (und begrifflich unkorrekt) auch nicht mit Datenbanksystemen verwaltete Daten als „Datenbank“ bezeichnet: eine Menge thematisch zusammenhängender Dateien.


== Geschichte ==
Ausgehend von Problemen bei der Verarbeitung von Daten in einfachen Dateien wurde in den 1960er Jahren das Konzept eingeführt, Daten durch eine separate Softwareschicht zwischen Betriebssystem (Dateiverwaltung) und Anwendungsprogramm zu verwalten. Dieses Konzept begegnete der Fehlentwicklung, dass Datenspeicher in Form von Dateien in der Regel für eine spezielle Anwendung konzipiert wurden und ein erheblicher Teil des Tagesgeschäfts mit Umkopieren, Mischen und Restrukturieren der Dateien belastet war.
Eines der ersten großen DBMS war IMS mit der Sprache DL/I (Data Language One). Die damit verwalteten Datenbanken waren hierarchisch strukturiert. Parallel dazu definierte CODASYL ein Modell für netzwerkartig strukturierte Datenbanken.
Einen wesentlichen Fortschritt erzielte in den 1960er und 1970er Jahren Edgar F. Codd mit seiner Forschungsarbeit am IBM Almaden Research Center. Codd entwickelte die Grundlagen des ersten experimentellen relationalen Datenbanksystems System R. Die Berkeley Group folgte mit Ingres und der Abfragesprache QUEL.
Oracle (damals noch unter den Firmennamen SDL und RSI) verwertete die Ergebnisse des System R und führte SQL zum kommerziellen Erfolg. IBM folgte mit SQL/DS und DB2. Die relationalen Datenbanksysteme verdrängten in den 1980er Jahren die hierarchischen und netzwerkartigen Systeme und der Großteil der Behörden, Konzerne, Institute und mittelständischen Unternehmen stellte seine IT auf Datenbanksysteme um.
Während in den 1990er Jahren wenige kommerzielle Hersteller von Datenbank-Software faktisch den Markt beherrschten (namentlich IBM, Informix, dBASE, Microsoft SQL Server und Oracle), erlangten in den 2000ern die Open-Source-Datenbankmanagementsysteme eine immer größere Bedeutung. Vor allem MySQL und PostgreSQL erzielten signifikante Marktanteile. Als Reaktion begannen die führenden kommerziellen Hersteller, gebührenfreie Versionen ihrer Datenbank-Software anzubieten. Etwa seit 2001 ist aufgrund mangelnder Skalierbarkeit relationaler Datenbanken die Bedeutung der NoSQL-Systeme gewachsen.
Einen Stammbaum der Datenbanksysteme findet man als Genealogy of Relational Database Management Systems beim Hasso-Plattner-Institut.


== Bedeutung ==
Datenbanksysteme sind heute ein zentraler Bestandteil der Unternehmenssoftware. Damit stellen sie einen kritischen Teil vieler Unternehmen und Behörden dar. Von der Verfügbarkeit, Vollständigkeit und Richtigkeit der Daten hängt die Aktionsfähigkeit eines Unternehmens ab. Die Datensicherheit ist daher ein wichtiger und gesetzlich vorgeschriebener Bestandteil der IT eines Unternehmens oder einer Behörde.


== Komponenten eines Datenbanksystems ==

Das Datenbanksystem ist das ausgeführte DBMS zusammen mit den zu verwaltenden Daten der Datenbank. Eine Datenbank gewährleistet die persistente Speicherung sowie die Konsistenz der Nutzdaten einer Institution und bietet für die benutzenden Datenbankanwendungen mit dem DBMS Schnittstellen zur Abfrage, Auswertung, Veränderung und Verwaltung dieser Daten.


=== Datenbankmanagementsystem ===
Das Datenbankmanagementsystem (DBMS) ist die eingesetzte Software, die für das Datenbanksystem installiert und konfiguriert wird. Das DBMS legt das Datenbankmodell fest, hat einen Großteil der unten angeführten Anforderungen zu sichern und entscheidet maßgeblich über Funktionalität und Geschwindigkeit des Systems. Datenbankmanagementsysteme selbst sind hochkomplexe Softwaresysteme.
Für Datenbankmanagementsystem wird (selten) auch der Begriff Datenbankverwaltungssystem (DBVS) verwendet.
Gängig ist die Abkürzung RDBMS für ein relationales Datenbankmanagementsystem.


=== Datenbank ===
In der Theorie versteht man unter Datenbank (englisch database) einen logisch zusammengehörigen Datenbestand. Dieser Datenbestand wird von einem laufenden DBMS verwaltet und für Anwendungssysteme und Benutzer unsichtbar auf nichtflüchtigen Speichermedien abgelegt. Um einen effizienten Zugriff auf die Datenbank zu gewährleisten, verwaltet das DBMS in der Regel eine Speicherhierarchie, die insbesondere auch einen schnellen Zwischenspeicher (Pufferpool) umfasst. Zur Wahrung der Konsistenz des Datenbestandes müssen sich alle Anwendungssysteme an das DBMS wenden, um die Datenbank nutzen zu können. Allein administrativen Tätigkeiten, wie zum Beispiel der Datensicherung, ist der direkte Zugriff auf den Speicher erlaubt. Die logische Struktur der zu speichernden Daten wird bei der Datenmodellierung als Datenmodell erarbeitet und festgelegt und in ihrer finalen Form im und nach den Syntaxregeln des DBMS gespeichert. Hierzu erzeugt, nutzt und verwaltet das DBMS einen „Systemkatalog“ (Data-Dictionary) mit Metainformationen zum Datenbestand, beispielsweise über seine Struktur, seine Datenfelder (Name, Länge, Format …), Zugriffsregeln, Integritätsbedingungen usw. 
Einzelne DBMS-Hersteller verwenden geringfügig voneinander abweichende Begrifflichkeiten dafür, was man genau unter einer Datenbank versteht: entweder alle Daten, die von einem laufenden DBMS bzw. der Instanz verwaltet werden, oder nur die jeweils inhaltlich zusammengehörigen Daten. Bei verteilten Datenbanken gibt es auch im Modell mehrere Datenbanken auf unterschiedlichen Systemen, die miteinander verbunden sind.


== Beispiele ==
Alle Banken und Versicherungen arbeiten mit Datenbanksystemen, in der Regel mit relationalen DBMS. Im Datenbanksystem sind alle Kunden- und Kontoinformationen, Buchungen und andere Daten strukturiert abgelegt. In diesem Einsatzumfeld haben Datenschutz und Datensicherheit hohe Priorität. Datenbanksysteme werden hier zum Tagesgeschäft (OLTP) sowie periodisch oder ad-hoc zu beliebigen anderen Zwecken (wie im Marketing, Controlling, Rechnungswesen und vielen anderen Bereichen; siehe auch OLAP) verwendet.
Faktisch alle mittelständischen Unternehmen und Großkonzerne arbeiten zur Ressourcenplanung mit ERP-Systemen, deren Datenteil in Form von Datenbanksystemen vorliegt.
Dieser Artikel in seiner in der Wikipedia vorliegenden Fassung wird neben allen anderen dort enthaltenen Artikeln durch ein Datenbanksystem verwaltet (Wikipedia-Technik).
Marktforschungsinstitute tragen eigene und Fremddaten in Data-Warehouses (Datenlagern) zusammen.


== Funktionen eines DBMS ==
Die wesentlichen Funktionen von heutigen Datenbankmanagementsystemen sind:

Speicherung, Überschreibung und Löschung von Daten
Verwaltung der Metadaten
Vorkehrungen zur Datensicherheit
Vorkehrungen zum Datenschutz
Vorkehrungen zur Datenintegrität
Ermöglichung des Mehrbenutzerbetriebs durch das Transaktionskonzept
Optimierung von Abfragen
Ermöglichung von Triggern und Stored Procedures
Bereitstellung von Kennzahlen über Technik und Betrieb des DBMS


=== Datensicherheit ===
Das RDBMS speichert die relationalen Daten auf einem Speichermedium. Neben den eigentlichen Daten werden ebenfalls Informationen über die Datenschemata und Zugriffsrechte von Benutzern gespeichert. Letztere sind wichtig, um die Datensicherheit zu garantieren. Dazu gehört sowohl Schutz gegen Datenverlust sowie Schutz gegen unerlaubten Zugriff. Die Metadaten eines DBMS werden auch als das data dictionary oder Katalog des Systems bezeichnet.
Ein weiterer wichtiger Aspekt von Datenbanken ist das Sichern des Datenbestandes durch Backups. In der Praxis ist dies oft ein nicht zu vernachlässigendes Performance-Problem, da während eines Backups Daten nur sehr eingeschränkt modifiziert werden dürfen.


=== Transaktionen ===
Ein weiterer wichtiger Teil der Datensicherheit ist das Transaktionskonzept, das Daten gegen Race Conditions durch den parallelen Zugriff mehrerer Benutzer schützt. Andernfalls könnten Daten von verschiedenen Benutzern gleichzeitig geändert werden. Das Ergebnis der Änderungen würde dann vom Zufall abhängen oder Daten könnten inkonsistent werden. Vereinfacht dargestellt, sperren Transaktionen Daten vorübergehend für den Zugriff durch andere Benutzer, bis eine Transaktion durch einen Commit beendet wird oder erfolgte Änderungen durch einen Rollback unwirksam gemacht werden. Danach sind die Daten wieder für andere Transaktionen frei.


=== Datenintegrität ===
Die Integrität der Daten kann durch Constraints sichergestellt werden. Dies sind Regeln im Managementsystem, die beschreiben, wie Daten verändert werden dürfen. Der wichtigste Vertreter bei relationalen Datenbanksystemen ist der Foreign Key Constraint. Dieser verhindert, dass Daten gelöscht werden können, die von einer anderen Tabelle noch benötigt, d. h. über einen Foreign Key referenziert werden. Siehe Hauptartikel referentielle Integrität.
Andere Integritätsbedingungen regeln zum Beispiel, ob Duplikate erlaubt sind oder welche Inhalte einzelne Datenfelder enthalten dürfen („Bereichsintegrität“, inkl. Prüfung auf erlaubte Leerinhalte).


=== Abfrageoptimierung ===

Damit Daten abgefragt und verändert werden können, stellt das DBMS eine Datenbanksprache zur Verfügung. Eine Abfrage an das Datenbanksystem wird dabei zunächst in die logischen Operationen der relationalen Algebra übersetzt. Danach werden sogenannte Datenbankoperatoren ausgewählt, die die logische Operation tatsächlich auf den Daten ausführt. Die Wahl der Operatoren und die Reihenfolge ihrer Ausführung nennt man das Erstellen eines Ausführungsplans durch den Abfrageoptimierer. Der Optimierer ist ein besonders komplexer Teil der Datenbanksoftware und hat wesentlichen Einfluss auf die Effizienz des Gesamtsystems.
Bei der Abfrageoptimierung spielen Indizes eine wichtige Rolle. Sie dienen dazu, schnell einen bestimmten Datensatz zu finden. Welche Daten einen Index erhalten, wird mit dem Datenbankschema festgelegt, kann aber später von einem Datenbankadministrator angepasst werden.


=== Anwendungsunterstützung ===
Zur Unterstützung von Datenbankapplikationen bieten Datenbanksysteme Trigger und Stored Procedures an. Ein Trigger löst eine Aktion in der Datenbank aus, wenn ein bestimmtes Ereignis eingetreten ist, häufig bei Einfüge- oder Änderungsoperationen. Stored Procedures dienen dem Ausführen von Scripten in der Datenbank. Da Stored Procedures innerhalb des Datenbanksystems ausgeführt werden, sind sie oft der effizienteste Weg, Daten zu manipulieren. Datenbanken, die Trigger und Stored Procedures unterstützen, heißen auch aktive Datenbanken.


=== Sprachen ===
Eine Datenbank stellt als Schnittstelle eine Datenbanksprache für die folgenden Zwecke zur Verfügung:

Datenabfrage und -manipulation (DML)
Verwaltung der Datenbank und Definition der Datenstrukturen (DDL)
Berechtigungssteuerung (DCL)Bei den relationalen DBMS sind diese Kategorien in einer Sprache (SQL) vereint, bei anderen Systemen existiert aber durchaus eine Trennung in Form unterschiedlicher Sprachen.


=== Mehrbenutzerfähigkeit ===
Für den Zugriff auf die Daten werden Berechtigungen verwaltet. Ohne Berechtigung kann die entsprechende Operation nicht durchgeführt werden.
Für den (pseudo-)gleichzeitigen Zugriff mehrerer Anwendungen bzw. Anwender regelt das DBMS Konkurrenzsituationen.

Es werden Sperren (engl. locks) verwaltet.
Es werden Systemprotokolle (engl. logs bzw. log files) verwaltet.
Die Datenbank arbeitet transaktionsorientiert.Diese Gruppe von Anforderungen zeichnet Datenbanksysteme im engeren Sinne gegenüber funktional erweiterten Dateisystemen aus.
Fehler in einer Datenbank, die durch unzulässigen parallelen Datenbankzugriff auftreten, werden Anomalien im Mehrbenutzerbetrieb genannt.


== Verschiedene Formen von Datenbanksystemen ==


=== Datenbankmodell ===
Grundlage für die Strukturierung der Daten und ihrer Beziehungen zueinander ist das Datenbankmodell, das durch den DBMS-Hersteller festgelegt wird. Je nach Datenbankmodell muss das Datenbankschema an bestimmte Strukturierungsmöglichkeiten angepasst werden:

hierarchisch: Die Datenobjekte können ausschließlich in einer Eltern-Kind-Beziehung zueinander stehen.
netzwerkartig: Die Datenobjekte werden miteinander in Netzen verbunden.
relational: Die Daten werden zeilenweise in Tabellen verwaltet. Es kann beliebige Beziehungen zwischen Daten geben. Sie werden durch Werte bestimmter Tabellenspalten festgelegt.
objektorientiert: Die Beziehungen zwischen Datenobjekten werden vom Datenbanksystem selbst verwaltet. Objekte können Eigenschaften und Daten von anderen Objekten erben.
dokumentenorientiert: Die zu speichernden Objekte werden als Dokumente mit möglicherweise verschiedenen Attributen, d. h. ohne die Voraussetzung der Strukturgleichheit, gespeichert.Es existiert eine Vielzahl von Misch- und Nebenformen, wie zum Beispiel das objektrelationale Modell.


=== Ausrichtung ===
Klassischerweise unterscheidet man eine Ausrichtung des Systems auf viele kleine Abfragen (OLTP) oder lang andauernder Auswertungen (OLAP). Es ist aber durchaus gängig, dass dasselbe System beiden Anforderungen gerecht werden muss und zum Beispiel tagsüber für den OLTP- und nachts für den OLAP-Betrieb „gefahren“ wird. Ein Datenbankadministrator arbeitet dann unterschiedliche Konfigurationen aus (Hauptspeicher des Servers, Prozess-Anzahl, Optimierungsstrategie beim Zugriff etc.).


== Siehe auch ==
Digital Bibliography & Library Project (DBLP): bibliographisches Verzeichnis mit dem Schwerpunkt Datenbanksysteme und Logikprogrammierung.
Liste der Datenbankmanagementsysteme
eingebettetes Datenbanksystem
Datasource
Datenbanksicherheit
Textgenerierung


== Literatur ==
Ramez Elmasri, Shamkant B. Navathe: Grundlagen von Datenbanksystemen. 3. Auflage Ausgabe Grundstudium. Pearson Studium, München u. a. 2005, ISBN 3-8273-7153-8.
Andreas Heuer, Gunter Saake: Datenbanken. Konzepte und Sprachen. 2., aktualisierte und erweiterte Auflage. mitp-Verlag, Bonn 2000, ISBN 3-8266-0619-1.
Alfons Kemper, André Eickler: Datenbanksysteme. Eine Einführung. 7., aktualisierte und erweiterte Auflage. Oldenbourg Verlag, München u. a. 2009, ISBN 978-3-486-59018-0.
Thomas Kudraß (Hrsg.): Taschenbuch Datenbanken. Fachbuchverlag Leipzig im Carl.Hanser-Verlag, München 2007, ISBN 978-3-446-40944-6.
T. William Olle: The Codasyl Approach to Data Base Management. Wiley, Chichester 1978, ISBN 0-471-99579-7.
Gottfried Vossen: Datenmodelle, Datenbanksprachen und Datenbankmanagementsysteme. 5., korrigierte und ergänzte Auflage. Oldenbourg Verlag, München u. a. 2008, ISBN 3-486-27574-7.


== Weblinks ==

Bundesamt für Sicherheit in der Informationstechnik: Geeignete Auswahl einer Datenbank-Software
Datenbankwiki an der FH Köln
Datenbanken-verstehen, ein Portal rund um Datenbanken, Datenbanktheorie und Datenbankpraxis


== Einzelnachweise ==