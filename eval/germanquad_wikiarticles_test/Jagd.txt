Jagd ist das Aufspüren, Verfolgen, Fangen und Erlegen von Wild durch Jäger. In der deutschen Jägersprache traditionell auch Weidwerk oder seltener Waidwerk genannt, ist die Jagd das Handwerk des Jägers. Unerlaubte Jagd bezeichnet man als Wilderei. Die Jagd zählt, zusammen mit der ebenfalls auf Gewinnung von Naturprodukten gerichteten  Landwirtschaft, Forstwirtschaft und Fischerei, zur Urproduktion.


== Begriff ==


=== Terminologie ===
Die hier behandelte Bedeutung des Wortes Jagd – das Aufspüren, Verfolgen, Fangen und Erlegen von Wild durch Jäger – lässt sich von mehreren verwandten, aber abweichenden Bedeutungen abgrenzen. So wird das Wort Jagd auch als Bezeichnung für eine einzelne Jagdveranstaltung („die Jagd findet kommenden Freitag statt“), die Gesamtheit der Beteiligten an einer bestimmten Jagdveranstaltung („die Jagd bricht auf“) und als Kurzform für den Begriff Jagdrevier („die Jagd grenzt an den Staatsforst“) verwendet. Die Bezeichnung Jagd wird zudem in einem umgangssprachlich übertragenen Sinn und losgelöst vom Kontext der Tötung von Wildtieren als Synonym zu den Begriffen Verfolgung und Hetze benutzt („die Jagd auf die Räuber“). Bezogen auf die Jagd in Deutschland wird Jagd in einem weiteren Sinne teilweise auch mit der Hege und Pflege des Wildes assoziiert.


=== Etymologie ===
Das Wort Jagd stammt über das mittelhochdeutsche jaget vom althochdeutschen jagōd ab, einer Ableitung vom ebenfalls althochdeutschen Verb jagōn „schnell verfolgen, hetzen, zu fangen oder zu erlegen suchen, eilen“. Die Herkunft von jagōn bzw. jagōd ist etymologisch dunkel.Ebenso wie andere jagdliche Komposita mit weid (z. B. -mann oder -gerechtigkeit) hat das Wort Weidwerk die indogermanische Wurzel *uid- mit der Bedeutung „sich Nahrung verschaffen“, die im Laufe der Sprachentwicklung zu althochdeutsch weida, später mittel- und neuhochdeutsch weid wurde.


== Ziele und Motive ==
Die Jagd wurde historisch und wird gegenwärtig aus verschiedenen und jeweils unterschiedlich gewichteten Gründen betrieben:
Gewinnung von Wildbret als Lebensmittel
Verwertung anderer Teile des Tieres, wie Felle, Hörner, Geweihe, Sehnen und ähnliches zur Fertigung von Werkzeug, Kleidung, Gebrauchsgegenständen, Andenken, Trophäen, u. ä.
Regulierung von Wildtierpopulationen (Bestandsregulierung), etwa zur Reduktion von Wildschäden an Nutzpflanzen in der Landwirtschaft oder durch Wildverbiss an der Naturverjüngung im Wald , zur Eindämmung von Tierseuchen oder zur Erreichung naturschutzfachlicher Ziele im Rahmen des Wildtiermanagements
abstraktere Motive, darunter Erwerbstätigkeit, handwerklicher Ehrgeiz, Erholung und Abwechslung vom Alltag, Freude an der Jagd, Interaktion mit Familie und (Jagd-)Freunden, Naturerfahrung, soziale Distinktion und Tradition
		
		
		
		
		


== Geschichte ==

Die Jagd gehört zu den ursprünglichsten Tätigkeiten in der Menschheitsgeschichte und ist älter als der anatomisch moderne Mensch (Homo sapiens) selbst.Die ältesten unumstrittenen archäologischen Belege für Jagd stammen aus dem Altpleistozän und fallen zeitlich mit der Entstehung und Ausbreitung des Homo erectus vor rund 1,7 Millionen Jahren zusammen. Von da an bis in die Zeit um 10.000 v. Chr. – und in Teilen darüber hinaus – lebte nahezu die gesamte Menschheit als Jäger und Sammler. Die Jagd ermöglichte durch die mit ihr verbundene Notwendigkeit zur Spezialisierung, Arbeitsteilung und Vorausplanung der Jäger, etwa bei der Produktion von Werkzeugen und Jagdwaffen, bedeutende Schritte in der Evolution des Menschen. Die gemeinsam durchgeführte Jagd förderte die sozialen und kommunikativen Fähigkeiten und bildete eine der Grundlagen der menschlichen Kultur.Die Jagd diente zur Nahrungsversorgung und lieferte neben Fleisch tierische Nebenprodukte wie Knochen für Werkzeuge oder auch für Flöten und Kunstwerke, Felle als Bekleidung, für Schuhe, für Decken, für Behausungen (Zelte) und Tragetaschen sowie Sehnen zum Nähen und für Bögen. Im Jungpaläolithikum und Magdalenien finden sich erste Höhlenmalereien und figürliche Kunstwerke der eiszeitlichen Jäger. Ursprünglich wurden die Jagdtiere zum Beispiel in eine Enge getrieben. Die ältesten Jagdformen sind die Hetz- bzw. Ausdauerjagd, die Lauer- und die Fallenjagd.Mit der sich im Zuge der neolithischen Revolution verbreitenden Sesshaftwerdung des Menschen und dem Beginn von Ackerbau und Viehzucht bekam die Jagd als Ernährungsquelle in weiten Teilen der Bevölkerung nachrangige Bedeutung. Zugleich ergaben sich durch die veränderten Lebensumstände im Schutz des kultivierten Landes vor Wildschäden und der Bekämpfung von Raubtieren zum Schutz von Nutzvieh auch neue Verwendungszwecke für die Jagd.


== Jägersprache ==

Die Jägersprache gehört zu den ältesten existenten Fachsprachen und dient heute vornehmlich der präzisen Verständigung der Jäger untereinander. Da sich die Jägersprache von der Gemeinsprache in der Lexik und Phraseologie, nicht aber in der Syntax unterscheidet, kann sie als jagdlicher Fachwortschatz betrachtet werden. Dieser jagdspezifische Fachwortschatz hat sich in mehreren Sprachen entwickelt, so beispielsweise im Deutschen, Englischen und Spanischen.Deutsche Jägersprache
Die deutsche Jägersprache hat ihre schriftlich tradierten Ursprünge im 8. Jahrhundert und umfasst einen Wortschatz von etwa 13.000 Ausdrücken mit rund 40.000 definierten Bedeutungen. Heute sind davon in der jagdlichen Praxis maximal 2000 Termini in Verwendung. Aufgrund der veränderten Jagdpraxis sind inzwischen zahlreiche ältere Termini obsolet geworden (z. B. „Kloben“, eine Klemmfalle für Vögel, und andere Begriffe mit Bezug zur Fangjagd von Singvögeln). Lange Zeit dominierten die von Anfang an präsenten und von der vorherrschenden Jagdtechnik bestimmten jagdlichen Fachausdrücke (z. B. „Saufeder“ für den langgeschäfteten, mit einem Riemen umwickelten Spieß für die Wildschweinjagd oder „Hetzen“ für das Verfolgen des Wildes durch den Jagdhund). Erst in der Neuzeit und insbesondere im 18. Jahrhundert kamen, ausgehend von den Berufsjägern der Landesfürsten, standessprachliche Begriffe hinzu, die auf soziale Distinktion von Außenstehenden abzielten und keinen praktischen Mehrwert hatten (z. B. „Teller“ für die Ohren des Wildschweins oder „Lunte“ für den Schwanz des Fuchses). Hierbei gab es innerhalb des deutschen Sprachraums regionale Unterschiede, da sich, etwa in der Schweiz aufgrund der abweichenden Entwicklung des Jagdrechts kein standesprachlicher Wortschatz bildete und die Jägerspräche weitgehend auf Fachausdrücke beschränkt blieb. Einige Wörter und Redewendungen aus der Jägersprache haben Eingang in den allgemeinen Sprachgebrauch gefunden (z. B. „Blattschuss“ als Synonym zu Volltreffer).


== Jagdrecht ==
Das Wort Jagdrecht hat zwei unterschiedliche Bedeutungsinhalte, zu deren expliziter Unterscheidung man auch vom objektiven Jagdrecht einerseits und vom subjektiven Jagdrecht andererseits spricht.


=== Objektives Jagdrecht ===

Das objektive Jagdrecht umfasst alle Rechtsnormen, die sich mit der Jagd befassen. Art und Umfang dieser Normen können sich von Ort zu Ort teils erheblich voneinander unterscheiden, wie beispielsweise zwischen rezenten Jäger-und-Sammler-Gemeinschaften im Amazonasbecken einerseits und Staatswesen in Mitteleuropa andererseits. Generell bilden Regelungen zum rechtlichen Status des Wildes und zum Inhaber des subjektiven Jagdrechts typischerweise das Fundament des objektiven Jagdrechts. Darüber hinaus existieren oft weitergehende Regelungen, so z. B. Restriktionen zu den jagdbaren Wildarten, Jagd- und Schonzeiten für das jagdbare Wild, Verbote bestimmter Jagdwaffen und -methoden, Voraussetzungen für Ausstellung eines Jagdscheins, Meldepflichten für erlegtes Wild, Vorschriften zum Umgang mit Wildbret und zahlenmäßige oder qualitative Begrenzungen beim Erlegen von jagdbaren Wild.Siehe auch: Objektives Recht


=== Subjektives Jagdrecht ===
Das subjektive Jagdrecht ist das einem einzelnen Rechtssubjekt zustehende Recht zur Jagd. In vielen Jurisdiktionen steht das subjektive Jagdrecht dem Grundeigentümer zu, so etwa in den Ländern West-, Mittel-, Nordeuropas und des Baltikums sowie in Namibia und Simbabwe. In diesen Staaten stellt das subjektive Jagdrecht ein dem einzelnen Berechtigten persönlich zustehendes, privatrechtliches Recht an seinem Grundeigentum dar. In anderen Ländern, wie etwa Italien, Polen, Rumänien und der Schweiz, liegt das subjektive Jagdrecht dagegen als hoheitliches Recht beim Staat selbst, der es Dritten durch Vergabe von Jagdlizenzen und Verpachtung von Jagdrevieren zugänglich macht oder in Eigenregie ausübt.Einige Jurisdiktionen garantieren dem Bürger ein in der Verfassung verbrieftes Grundrecht zur Jagd, gegenwärtig etwa die Verfassungen mehrerer US-Bundesstaaten, historisch unter anderem die im Zuge der deutschen Revolution von 1848/49 verabschiedete Frankfurter Reichsverfassung. In einigen Rechtsordnungen ist das subjektive Jagdrecht zudem als Eigentum beziehungsweise als dessen Bestandteil grundlegend geschützt, so etwa in Deutschland über die verfassungsrechtliche Eigentumsgarantie nach Art. 14 Grundgesetz.


== Jagdwaffen ==

Die Handfeuerwaffe ist heute, insbesondere in den Industrienationen, die dominierende Jagdwaffe, in einigen Jurisdiktionen, teils eingeschränkt auf bestimmte Einsatzzwecke, sind aber auch Bogen, Armbrust, Falle, Spieß, Speer und Lanze gebräuchlich. Vor allem historisch wurden bzw. werden von rezenten Jäger-und-Sammler-Gemeinschaften auch Netze, Blasrohre, (Pfeil-)Gifte sowie Feuer jagdlich genutzt.In weiten Teilen von Subsahara-Afrika und Südostasien stellt die vergleichsweise günstige, vor etwaigen Strafverfolgern leicht zu verbergende und zu den vorwiegend terrestrischen Wildarten passende Fallenjagd mit Schlingen die vorherrschende Jagdform dar. Im Amazonas-Regenwald ist sie dagegen weniger üblich, da aufgrund des höheren Anteils von kleinen, als Baumbewohner lebenden Wildarten bevorzugt verschiedene Projektilwaffen – wie etwa Blasrohre, Pfeil und Bogen sowie Handfeuerwaffen – verwendet werden.
Daneben sind verschiedene andere Blankwaffen bzw. Schneidwerkzeuge in Gebrauch, insbesondere Messer, die, neben ihren Verwendung als Universalwerkzeug, auch als Waffe zum Abfangen von verletztem Wild genutzt werden.


== Jagdarten ==
Im Laufe der Zeit haben sich die verschiedensten Jagdarten entwickelt, welche auf speziellen Situationen bzw. die Jagd auf bestimmte Tierarten abgestimmt sind. Es existieren mehrere Möglichkeiten, um zumindest einen Teil der Jagdarten zu systematisieren. Eine der geläufigsten Gliederungen unterscheidet nach Anzahl der beteiligten Jäger.


=== Einzeljagd ===
Zur Einzeljagd werden Jagdarten gerechnet, die von einem Jäger alleine ausgeübt werden bzw. prinzipiell alleine ausgeübt werden können:

Ansitzjagd, bezogen auf Tiere auch als Lauerjagd bezeichnet: Bei der Ansitzjagd lauert der Jäger an einer geeigneten Stelle, etwa auf einem Hochsitz oder hinter einem Ansitzschirm, dem Wild auf. Das vorbeiziehende Wild kann so in Ruhe beobachtet, angesprochen (erkannt und bestimmt) und gegebenenfalls sicher erlegt werden.
Pirschjagd: Hierbei begeht der Jäger vorsichtig und leise das zu bejagende Gebiet, er pirscht gegen den Wind, um unbemerkt möglichst nahe ans Wild zu kommen. Spuren wird er nicht blindlings folgen, sondern nur dann, wenn sie vielversprechend sind. Dazu ist eine gute Revierkenntnis erforderlich. Fährten im Neuschnee sind leicht zu erkennen. Deshalb dienen sie als besonders gute Grundlage für die Entscheidung, an bestimmten Stellen zu pirschen.


=== Gesellschaftsjagd ===
Zur Gesellschaftsjagd, seltener auch als Gruppenjagd bezeichnet, werden Jagdarten gezählt, die von mehreren Jägern gemeinsam ausgeübt werden:

Sammel- oder Gemeinschaftsansitz: Gemeinsame Ansitzjagd mehrerer Jäger.
Treibjagd oder Bewegungsjagd: Sammelbegriff für alle Jagdarten, bei denen das Wild von Treibern oder Hunden beunruhigt und aus seinen Einständen getrieben oder gedrückt wird.
Drückjagd, im Gebirge aufgrund der abgeriegelten Zwangswechsel des Wildes auch Riegeljagd genannt: Form der Treibjagd, bei der Schalenwild gedrückt, d. h. von Treibern und zumeist auch von Jagdhunden vergleichsweise langsam in Richtung der vorher aufgestellten Jäger gescheucht wird. Dabei wird versucht, das Wild bewusst langsam aus seinen Einständen (etwa in Dickungen, Brombeerverhauen, Schilfgürteln etc.) heraus und in Bewegung zu bringen, um einen sicheren Schuss zu ermöglichen.
Stöberjagd: Variante der Drückjagd, bei der das Wild ausschließlich von spurlaut und solo jagenden Stöberhunden in Bewegung gebracht wird.
Vorsteh- oder Standtreiben: Variante der Treibjagd, bei der die bejagte Fläche von Schützen auf festen Standplätzen umstellt und anschließend von einer Treiberwehr durchkämmt wird.
Erntejagd: Jagd während der Ernte, bei der das betreffende Feld vor dem Abernten von Jägern umstellt wird, um das durch schwindende Deckung und die als „Treiber“ fungierenden Erntemaschinen aufgeschreckte Wild beim Ausbrechen aus dem abgestellten Bereich zu erlegen. Erntejagden werden insbesondere auf Wildschweine in Maisfeldern angewandt.
Streifjagd oder Streife: Variante der Treibjagd, bei der Treiber und Jäger das Gelände gegen den Wind in breiter Front absuchen.
Böhmische Streife: Insbesondere auf Hasen ausgeübte Variante der Streifjagd, bei der die Flanken vorgezogen werden, wodurch die Front von Schützen und Treibern ein „U“ bildet.
Kesseltreiben: Variante der Treibjagd, bei der aus Sicherheitsgründen nur mit Schrot geschossen wird. Abwechselnd postierte Schützen und Treiber bilden dabei einen Kreis – den Kessel – von rund einem Kilometer Durchmesser. Wenn der Kessel geschlossen ist, marschieren Schützen und Treiber gemeinsam auf den Mittelpunkt zu. Den Schützen ist anfänglich erlaubt, in das Treiben hinein zu schießen. Ab einer Gefährdungsdistanz von weniger als 400 Metern Kesseldurchmesser wird auf das Hornsignal „Treiber rein“ hin nur noch nach außen geschossen. Kesseltreiben werden heute zumeist auf offenen Flächen auf Hasen und anderes Niederwild außer Rehwild ausgeübt.
Lappjagd: Variante der Treibjagd, bei der das bejagte Gebiet rundherum mit an Leinen befestigten Stofflappen abgehängt wird, um  das Wild in bestimmte Richtungen zu lenken oder am Ausbrechen zu hindern.


=== Weitere Jagdarten ===
Weitere Jagdarten, die üblicherweise außerhalb der oben dargestellten Systematik von Einzel- und Gesellschaftsjagd stehen, sind:

Ausdauerjagd: Jagd, bei der die Jäger das Wild über lange Strecken zu Tode hetzen bis es vor Erschöpfung und Entkräftung zusammenbricht (heute zum Beispiel noch von den Khoisan und Aborigines praktiziert).
Baujagd: Jagd auf den Fuchs und Dachs in deren Bauen mit dem Terriern (lat. terra) und Dachshund (Dackel, Teckel).
Frettchenjagd, auch Frettieren genannt: Baujagd mit domestizierten Iltissen (Frettchen), um Kaninchen aus ihren unterirdischen Bauen zu treiben und außerhalb des Baues entweder mit Netzen zu fangen oder mit der Flinte zu erlegen.
Beizjagd, auch Falknerei genannt: Jagd mit abgerichteten Greifvögeln.
Brackierjagd, auch Brackieren genannt: Jagd auf Niederwild mit speziellen Hunden (Bracken), die das Wild aufstöbern, über lange Distanzen verfolgen und den Schützen zurücktreiben.
Böhämmerjagd: Jagd auf Bergfinken mit dem Blasrohr in Bergzabern
Bogenjagd: Ausübung der Jagd mit Pfeil und Bogen.
Fallenjagd bzw. Fangjagd: Die Jagd mit Tierfallen, bei der in lebend fangende Fallen (wie etwa Kastenfalle und Wippbrettfalle) und in Totschlagfallen (wie etwa Abzugeisen, Schlagbaum und Schwanenhals) unterschieden werden kann.
Gatterjagd: Jagdform, bei der das Wild in einem geschlossenen Gatter oder Gehege gejagt wird.
Kreisen bzw. Ausneuen: Suche nach frischen Fährten bei Neuschnee (jägersprachlich Neue), um die Präsenz von Wild im Einstand zu bestätigten und dann zu bejagen (so z. B. oft auf Wildschweine angewendet und bei bestätigter Präsenz mit einer anschließenden Drückjagd verbunden).
Lockjagd: Jagd mit Lockmitteln wie Nahrung (Kirrung), Gerüchen, Lauten oder Attrappen.
Hüttenjagd: Lockjagd mit angebundenen Eulen als Lockvogel , insbesondere auf Raben- und Greifvögel.
Parforcejagd, auch Hetzjagd genannt: Das Hetzen von Wild zu Pferd und mit jagender Hundemeute.
Suchjagd: Meist auf Hasen, Kaninchen, Fuchs oder Federwild ausgeübte Jagd, bei der mit Hilfe von Hunden (kurz jagende Hunde oder Vorstehhunde) Felder oder Waldabschnitte durchsucht werden, um flüchtendes Wild zu erlegen. Das Buschieren ist eine solche Jagd unter Verwendung der Flinte.
Wasserjagd: Die Jagd auf Flugwild am und auf dem Wasser mit brauchbaren Hunden.


== Berufsjäger ==

Berufsjäger, im internationalen Kontext verschiedentlich auch als professional hunter (PH), stalker oder gamekeeper bezeichnet, sind berufsmäßig tätige Jäger, die je nach örtlichen Verhältnissen und jagdrechtlichen Rahmenbedingungen in verschiedenen Funktionen aktiv sind. In Europa sind Berufsjäger oft als Angestellte von staatlichen Forstverwaltungen, privaten Großgrundbesitzern sowie Schutzgebietsverwaltungen tätig und für die Organisation des dortigen Jagdbetriebs zuständig, so z. B. für die Regulation des Wildbestandes durch Abschuss und Hege, das Führen von Jagdgästen und die Vermarktung des anfallenden Wildbrets. In Australien existiert eine Branche von Berufsjägern, die sich auf die Produktion von Känguru-Fleisch spezialisiert hat und jährlich mehrere Millionen der Beuteltiere erlegt. In den Ländern des südlichen Afrika, wie etwa Namibia und Südafrika, führen Berufsjäger oft aus dem Ausland stammende Jagdgäste auf Jagdsafaris und kümmern sich um die zwecks Fleischbeschaffung jagenden, einheimischen biltong hunters.


== Jagdtourismus ==

Jagdtourismus, auch Auslandsjagd oder, vor allem in Bezug auf Subsahara-Afrika, (Jagd-)Safari genannt, bildet ein spezielles Segment des Tourismus. Das Reiseziel ist in erster Linie von den vorhandenen Wildarten abhängig, insbesondere Europa, Afrika und Zentralasien sind dabei beliebte Destinationen. Die meisten Jagdtouristen stammen aus Europa und Nordamerika. An einigen der stärker frequentierten Reiseziele richten Grundeigentümer ihre Flächen gezielt auf Jagdtouristen aus, so beispielsweise mit den sporting estates in Schottland oder den hunting lodges, game conservancies und Jagdfarmen in Südafrika und Namibia.Jagdtourismus in Form von Trophäenjagden im Ausland gilt als hoch polarisierendes Thema, vor allem wenn die charismatische Megafauna Afrikas betroffen ist. Die Vorstellung, dass Trophäenjagden im Ausland unter gewissen Umständen eine nachhaltige Landnutzung darstellen und zum Erhalt von Schutzgebieten sowie der darin heimischen Arten dienen können, trifft vielfach auf Unverständnis. Umgekehrt stößt in den betroffenen Ländern, die oftmals bereits einen hohen Anteil ihrer Landesfläche unter Schutz gestellt haben, ein noch weitergehender Nutzungsverzicht durch die Ausweisung neuer Schutzgebiete häufig auf entschiedene Ablehnung bei der einheimischen Bevölkerung.


== Länderstatistiken ==
In manchen der gelisteten Länder braucht nicht jede Art der Jagdausübung bzw. jeder Jäger grundsätzlich eine staatliche Lizenz oder die geltenden Regelungen werden ignoriert (Wilderei), daher handelt es sich bei den Datensätzen zur Zahl der Jäger teilweise um Hochrechnungen basierend auf repräsentativen Umfragen, so etwa im Fall von Kanada und den Vereinigten Staaten.


=== Tabelle ===


=== Diagramm ===


== Jagdliteratur ==

Jagdliteratur sind handgeschriebene und gedruckte Schriften aller Art mit Bezug zur Jagd. Schon bei Tacitus und im Mittelalter finden sich Beschreibungen zur Jagd. Im deutschen Sprachraum wurden bis 1850 etwa 1200 spezielle Jagdbücher publiziert. Heute gibt es neben geschichtlichen Bearbeitungen, Wörterbüchern und Fachliteratur ein weites Feld von unterschiedlichster Belletristik. Lew Nikolajewitsch Tolstoi beschreibt eine winterliche Wolfsjagd in Krieg und Frieden.


== Rezeption in den Künsten ==


=== Malerei ===
Das Thema Jagd beschäftigt kreative Menschen spätestens seit den ersten Höhlenmalereien vor rund 44.000 Jahren. In der europäischen Malerei waren üppige Gemälde mit Jagdszenen Prestige- und Repräsentationsobjekte von Adeligen und wohlhabenden Bürgern. Oft wurden sie mit mythologischen Inhalten verknüpft (z. B. die jagende Göttin Diana). Die Grenze des Genres der Jagdmalerei zu dem der reinen Tiermalerei (z. B. von Jagdhunden) ist fließend. Bei beiden gibt es einen hohen Prozentsatz an Auftragsmalerei.
Aus der Fülle der Künstler, die sich dieses Themas mit unterschiedlichen Absichten und Fähigkeiten annahmen, seien folgende erwähnt: Peter Paul Rubens (phantasievolle mythologische und exotische Jagdszenen), Frans Snyders, Jan Fyt (Auftragsrealismus), Jean Siméon Chardin (in sich ruhende Stillleben mit erlegtem Wild), Eugène Delacroix (orientalische Jagdszenen), Emil Hünten (Parforce-Jagd), Manfred Schatz (Wild und Jagdhunde in der Natur), Carl Otto Fey (Wild in der Natur). Große Jagdmaler des 20. Jahrhunderts sind Bruno Liljefors und Dimitrij von Prokofieff; 1937 erhielten sie in Berlin die Gold- und Silbermedaille der Internationalen Jagdausstellung.

		
		
		


=== Bildhauerei ===
Das Motiv der Jagd in der Kunst hat von Anbeginn zahlreiche Bildhauer zu schöpferischen Werken inspiriert. In der römischen Antike wurde die Göttin Diana häufig in Plastiken und Skulpturen verewigt. Auch Künstler des 19. oder 20. Jahrhunderts haben Werke zum Thema Jagd mit klassischen oder zeitgenössischen Motiven geschaffen, mit Materialien wie Bronze (Augustus Saint-Gaudens) oder Kupfer (Klaus Rudolf Werhand).

		
		
		


== Kritik ==


=== Natur- und Umweltschutz ===
Von einigen Natur- und Umweltschützern aber auch manchen Jägern selbst wird verschiedentlich eine stärkere Ausrichtung der Jagdausübung an Prinzipien der Nachhaltigkeit und Erkenntnissen der Ökologie gefordert. In der Kritik steht dabei u. a. die Überjagung bestimmter Arten (z. B. das durch intensive Jagd für den lokalen Fleischbedarf und Handel verursachte, sog. „empty forest syndrome“ in tropischen Regenwäldern), aber auch die durch Hege-Maßnahmen wie Wildfütterung und Abschuss von als Konkurrenz wahrgenommenen Prädatoren unterstützte, einseitige Förderung von jagdlich interessanten Wildarten, deren zahlenmäßige Zunahme Rückwirkungen auf ihr Ökosystem hat (siehe Populationsdynamik).


=== Wald-Wild-Konflikt ===

Zu hohe Wilddichten von Pflanzenfressern, insbesondere von Schalenwild, können durch Verbiss eine aus ökologischen und wirtschaftlichen Gesichtspunkten angestrebte natürliche Verjüngung des Waldes erschweren oder verhindern. Durch die Bevorzugung bestimmter Baumarten kann selektiver Verbiss zu einem Mischwald gehörende Baumarten aus dem Bestand verdrängen und so die Baumartendiversität verringern. Auch gepflanzte Forstkulturen, die nicht durch Einzelbaumschutz oder Zäunung gesichert werden, sind betroffen. Schälschäden können ältere Waldbestände, die dem Verbiss bereits entwachsen sind, über Jahrzehnte hinweg gefährden sowie im Schadensfall destabilisieren und ökonomisch entwerten.Dieser sogenannte Wald-Wild-Konflikt – zur Verdeutlichung des Zielkonflikts und der Akteure auch als Forst-Jagd- bzw. Waldbesitzer-Jäger-Konflikt bezeichnet – wird von Waldbesitzern, Forstleuten und Naturschutzverbänden im Hinblick auf einen angestrebten Waldumbau hin zu klimastabilen Mischwäldern als bedeutendes Problem betrachtet.


=== Geschossmaterial ===

Blei und andere Metalle, die bei der Munitionsproduktion in einigen Projektil-Typen als Geschossmaterial Verwendung finden, können unter gewissen Umständen eine ökotoxische Wirkung aufweisen. Geschosse oder deren Fragmente können in Boden und Wasser sowie in die Nahrungskette von Wildtieren gelangen, wovon vor allem Wasservögel (z. B. Enten) und einige Beutegreifer (z. B. Seeadler) betroffen sein können, die verluderte Tiere oder Aufbruch fressen.Insbesondere die Verwendung von Munition mit Blei wird aus Gründen des Umweltschutzes (siehe Bleibelastung der Umwelt) und gesundheitlichen Gründen (siehe Bleivergiftung) kritisiert, da im Gegensatz zu anderen Materialien bei Blei keine Wirkschwelle benannt werden kann, unter der die Aufnahme von Blei gesundheitlich unbedenklich ist. Mehrere Jurisdiktionen in Europa und Nordamerika haben daher den Einsatz von Bleimunition – zugunsten von weniger toxischer bzw. bleifreier Munition – eingeschränkt oder verboten. Das Bundesinstitut für Risikobewertung kommt in seinen Studien zu dem Schluss, dass bei Normalverzehrern die zusätzliche Aufnahme über belastetes Wildbret verglichen mit der Gesamtaufnahme von Blei über andere Lebensmittelgruppen toxikologisch unbedeutend und ein gesundheitliches Risiko unwahrscheinlich ist.


=== Jagdunfälle ===
Ein Kritikpunkt an der Jagd sind in Zusammenhang mit der Jagdausübung stehende Unfälle, insbesondere solche, bei denen es zu Personenschäden durch Schusswaffen kommt. Die häufigsten Unfallursachen sind unsachgemäßer Umgang mit der Schusswaffe, das Übersehen des Opfers bzw. das Schießen auf ein nicht sicher identifiziertes Ziel und Querschläger.


=== Tierschutz ===
Bestimmte Jagdpraktiken und teilweise auch die Jagd ganz generell werden unter Verweis auf den Tierschutz kritisiert. Ein prominentes Beispiel für eine gesellschaftliche Diskussion, in der Kritiker vor allem auf den Tierschutz verweisen, ist die Parforce- bzw. Hetzjagd auf den Fuchs zu Pferd und mit Hundemeute, die in Großbritannien bis zu ihrem Verbot im Jahr 2004 eine lange Tradition hatte. Auch der in diversen Jagdgesetzen erlaubte oder geforderte Abschuss von wildernden Katzen und Hunden zum Schutz des Wildes stößt immer wieder auf Kritik. Bei der Beurteilung des Abschusses von streunenden Katzen steht dabei insbesondere deren negative Wirkung auf die Vogelwelt im Zentrum der Diskussion.


=== Tierrechte ===

Tierrechtler lehnen die Jagd grundsätzlich ab, da sie bestimmten Tieren auf Grund von deren Leidensfähigkeit (siehe auch Pathozentrismus) gewisse Rechte ähnlich den Menschenrechten (Recht auf Leben, körperliche Unversehrtheit) zuschreiben und daher eine Gleichbehandlung von Mensch und Tier ohne ein speziesistisches Ausschließen von bestimmter Arten fordern. Aus diesem Grund sehen radikale Teile der Tierrechtsbewegung Akte der Jagdstörung und -sabotage als gerechtfertigt an.


== Siehe auch ==
Angeln (Fischfang)
Vogelfang


== Literatur ==


=== Wissenschaftliche Literatur ===
Richard B. Lee, Richard Daly: Cambridge Encyclopedia of Hunters and Gatherers. Cambridge University Press, Cambridge, UK 1999, ISBN 978-0-521-60919-7.
Christian Ammer, Torsten Vor, Thomas Knoke, Stefan Wagner: Der Wald-Wild-Konflikt. Analyse und Lösungsansätze vor dem Hintergrund rechtlicher, ökologischer und ökonomischer Zusammenhänge (= Göttinger Forstwissenschaften.Band 5). Göttinger Universitätsverlag, Göttingen 2010, ISBN 978-3-941875-84-5, Volltext online (PDF).
Klaus Friedrich Maylein: Die Jagd. Funktion und Raum. Ursachen, Prozesse und Wirkungen funktionalen Wandels der Jagd. Dissertation, Universität Konstanz, 2005, Volltext online (PDF). Verlegt als: Die Jagd – Bedeutung und Ziele. Von den Treibjagden der Steinzeit bis ins 21. Jahrhundert. Wissenschaftliche Beiträge aus dem Tectum-Verlag, Reihe Sozialwissenschaften, Band 28. Tectum-Verlag, Marburg 2010, ISBN 978-3-8288-2182-8, Inhaltsverzeichnis online (PDF).
Katrin Josephine Wagner: Die Sprache der Jäger – Ein Vergleich der Weidmannssprache im deutsch- und englischsprachigen Raum (= Forum für Fachsprachen-Forschung. Band 143). Frank & Timme, Berlin 2018, ISBN 978-3-7329-0455-6, (eingeschränkte Vorschau in der Google-Buchsuche).
Marco Apollonio, Reidar Andersen, Rory Putman (Hrsg.): European ungulates and their management in the 21st century. Cambridge University Press, 2010, ISBN 978-0-521-76061-4.
Peet van der Merwe, Lindie du Plessis: Game farming and hunting tourism. 1. Auflage. African Sun Media, 2014, ISBN 978-0-9922359-1-8.
IUCN SSC (Hrsg.): IUCN SSC Guiding Principles on Trophy Hunting as a Tool for Creating Conservation Incentives. 2012, online (PDF).
IUCN SULi (Hrsg.): Informing decisions on trophy hunting – A Briefing Paper for European Union Decision-makers regarding potential plans for restriction of imports of hunting trophies. 2016, online (PDF).
Johannes Dietlein, Judith Froese (Hrsg.): Jagdliches Eigentum (= Bibliothek des Eigentum. Nr. 17). Springer, 2018, ISBN 978-3-662-54771-7, ISSN 1613-8686.
Barney Dickson, Jonathan Hutton, William A. Adams (Hrsg.): Recreational Hunting, Conservation and Rural Livelihoods: Science and Practice (= Conservation Science and Practice). Wiley-Blackwell, 2009, ISBN 978-1-4443-0318-6.
Brent Lovelock (Hrsg.): Tourism and the consumption of wildlife: hunting, shooting and sport fishing. Routledge, London 2007, ISBN 978-0-203-93432-6.
Rory Putman, Marco Apollonio, Reidar Andersen (Hrsg.): Ungulate Management in Europe: Problems and Practices. Cambridge University Press, 2011, ISBN 978-0-521-76059-1.
Bundesamt für Umwelt (BAFU) (Hrsg.): Wald und Wild – Grundlagen für die Praxis. Wissenschaftliche und methodische Grundlagen zum integralen Management von Reh, Gämse, Rothirsch und ihrem Lebensraum. Umwelt-Wissen Nr. 1013. Bern 2010. 232 S., Volltext online (PDF).
Andreas Haug: Wildlife-Management und Forstwirtschaft unter besonderer Berücksichtigung der Randbedingungen und Möglichkeiten einer Optimierung jagdwirtschaftlicher Aspekte für Waldeigentümer. Tenea, 2004, ISBN 978-3-86504-042-8.


=== Jagdlexika ===
Julia Numßen: Handbuch Jägersprache. BLV, München 2017, ISBN 978-3-8354-1728-1.
Gerhard Seilmeier (Hrsg.): Jagdlexikon. 7. Auflage, BLV, München 1996, ISBN 3-405-15131-7.
Ilse Haseder, Gerhard Stinglwagner: Knaurs Großes Jagdlexikon. Weltbild, Augsburg 2000, ISBN 3-8289-1579-5.
Riesenthals Jagdlexikon. Nachschlag- und Handbuch für Jäger und Jagdfreunde. Neumann, Neudamm 1916; Nachdruck: Weltbild, Augsburg 1999, ISBN 3-8289-4143-5, Volltext im Original online.


=== Jagdgeschichtliche Literatur ===
Ferdinand von Raesfeld: Das deutsche Waidwerk. Nachdruck der Erstausgabe von 1914. Parey, Hamburg 1996, ISBN 3-490-14412-0.
Kurt Lindner: Deutsche Jagdtraktate des 15. und 16. Jahrhunderts. 2 Bände, Berlin 1959 (= Quellen und Studien zur Geschichte der Jagd, 5–6).
Werner Rösener: Die Geschichte der Jagd. Kultur, Gesellschaft und Jagdwesen im Wandel der Zeit. Patmos, Düsseldorf; Artemis, Zürich 2004, ISBN 3-538-07179-9, (Rezension).
Joachim Hamberger: Ein kurzer Abriss der Jagdgeschichte – Von Hirschen und Menschen…. In: LWF aktuell. Nr. 44, 2004, Volltext online (PDF).
Sigrid Schwenk, Gunnar Tilander, Carl Arnold Willemsen (Hrsg.): Et multum et multa: Beiträge zur Literatur, Geschichte und Kultur der Jagd. Festschrift Kurt Lindner. Berlin und New York 1971.
Kurt Lindner: Geschichte des Weidwerks. de Gruyter, Berlin und Leipzig.
Band I: Die Jagd in der Vorzeit, 1937.
Band II: Die Jagd im frühen Mittelalter, 1940.
Alexander Krethlow: Hofjagd, Weidwerk, Wilderei. Kulturgeschichte der Jagd im 19. Jahrhundert. Schöningh, Paderborn 2015, ISBN 978-3-506-78258-8.
Hubertus Hiller: Jäger und Jagd: zur Entwicklung des Jagdwesens in Deutschland zwischen 1848 und 1914. Waxmann Verlag, Münster 2003, ISBN 978-3-8309-1196-8.
Kurt Müller, Hans-Jörg Blankenhorn: Jagd. In: Historisches Lexikon der Schweiz.  28. Januar 2008.
Gerhard Immler: Jagd, Jagdwesen (Mittelalter). In: Historisches Lexikon Bayerns, 2017.
Ulrich Wendt: Kultur und Jagd – ein Birschgang durch die Geschichte. Georg Reimer, Berlin.
I. Band: Das Mittelalter, 1907 (online bei ALO).
II. Band: Die neuere Zeit, 1908 (online bei ALO).
Winfried Freitag: Wald, Waldnutzung. In: Historisches Lexikon Bayerns, 2012.


== Weblinks ==

Linkkatalog zum Thema Jagd bei curlie.org (ehemals DMOZ)
Linkkatalog zum Thema Jagdgegner bei curlie.org (ehemals DMOZ)
Gemeinfreie Literatur zur Jagd online als Volltext bei Lexikus


== Einzelnachweise ==