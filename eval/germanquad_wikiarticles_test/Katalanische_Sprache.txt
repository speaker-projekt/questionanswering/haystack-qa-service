Die katalanische Sprache (Eigenschreibweise català [kətəˈɫa] auf Ostkatalanisch, [kataˈɫa] auf Westkatalanisch) gehört zur Familie der romanischen Sprachen. Katalanisch ist Amtssprache in Andorra sowie, neben dem Spanischen, regionale Amtssprache in Katalonien, auf den Balearen und in Valencia. Es ist außerdem eine der Regionalsprachen Frankreichs (im Roussillon/Département Pyrénées Orientales).
Manche Sprachwissenschaftler rechnen das Katalanische zu den galloromanischen Sprachen, andere ordnen es der Iberoromania zu. Auf der einen Seite besteht eine enge Verwandtschaft mit dem Okzitanischen – das Katalanische hat mehr lautliche und lexikalische Gemeinsamkeiten mit dem Okzitanischen als mit den übrigen Sprachen der iberischen Halbinsel. Auf der anderen Seite stimmt das Katalanische in einigen Merkmalen mit iberoromanischen Sprachen überein, sodass es oft als Brückensprache (llengua-pont) zwischen Galloromanisch und Iberoromanisch bezeichnet wird. Dies mag darin begründet sein, dass der Herrschaftsbereich des iberischen Westgotenreiches bis nach Septimanien reichte, mitten im okzitanischen Sprachkulturgebiet; umgekehrt ging auch der fränkische Einfluss unter Karl dem Großen über die Pyrenäen bis in die Katalanischen Grafschaften. Die Pyrenäen bildeten also in der Entstehungszeit der romanischen Sprachen keine natürliche Grenze zwischen den politischen Reichen und Kulturräumen Galliens und der iberischen Halbinsel.


== Verbreitung ==

Katalanisch hat etwa 11,5 Millionen aktive Sprecher, etwa 12,6 Millionen Menschen verstehen Katalanisch. Das Verbreitungsgebiet der Sprache umfasst folgende Regionen:

In Spanien Katalonien, den größten Teil der Region Valencia – die dortige Variante des Katalanischen heißt auch Valencianisch (valencià) –, die Balearen, wo die Mallorquinisch (mallorquí), Menorquinisch (menorquí) und Ibizenkisch (eivissenc) genannten – zu Balearisch zusammengefassten – Dialekte gesprochen werden; außerdem einen Gebietsstreifen im Osten Aragoniens, die Franja de Aragón mit dem català ribagorçà
in Südfrankreich das Département Pyrénées-Orientales, das auch als „Nordkatalonien“ bezeichnet wird – die dortige Varietät des Katalanischen wird als rosellonès bezeichnet,
in Italien die Stadt Alghero auf Sardinien (katalanisch L'Alguer, wo wenige Menschen noch heute alguerés sprechen, siehe unten),
Andorra.Diese Gebiete bezeichnet man zusammenfassend auch als Països Catalans („Katalanische Länder“). An der nördlichen Sprachgrenze steht heute die Porta dels Països Catalans bei Salses-le-Château.
Das katalanische Sprachgebiet wird als Ganzes in zwei Dialektgruppen eingeteilt: zum einen Ostkatalanisch, wozu der östliche Teil Kataloniens, der katalanischsprachige Teil Frankreichs, die Balearen und Alguer gehören, und zum anderen die westkatalanischen Dialekte. Hauptkriterium für diese Einteilung ist die Aussprache der unbetonten Vokale o, e, und a. Während man sie im Bereich des Westkatalanischen wirklich stets auch so ausspricht, wie sie geschrieben werden, werden in der Aussprache des Ostkatalanischen in unbetonter Position o zu u, e und a zu kurzen offenen Vokalen oder zu einem Schwa-Laut (​[⁠ə⁠]​), der sich ähnlich anhört wie deutsch e in bitte. 
Der Status der Sprache ist je nach Region unterschiedlich. In Andorra ist Katalanisch alleinige Amtssprache, in Spanien regionale Amtssprache. Im „Sprachengesetz von Aragón“ (Ley de Lenguas de Aragón), am 9. Mai 2013 durch das aragónesische Regionalparlament unter Führung der Partido Popular beschlossen, wird die Sprache für den eigenen Regierungsbereich mit dem Glottonym Lengua aragonesa propia del área oriental (LAPAO) bezeichnet. Im französischen Département Pyrénées-Orientales wurde im Dezember 2007 eine Charta zur Förderung der katalanischen Sprache (französisch: Charte en faveur du Catalan) zum Schutz und zur Weiterentwicklung des Katalanischen in Nordkatalonien beschlossen. In Alguer wird die lokale Sprachvarietät stundenweise in der Schule gelehrt.


== Geschichte ==

Katalanisch ist kein Dialekt des Spanischen, sondern eine direkt aus dem Vulgärlatein hervorgegangene Sprache (also auch kein „Primärer Dialekt“, der lediglich der Standardisierung des Spanischen vorausgegangen wäre; im Falle des Spanischen wären dies nur Asturleonesisch, Kastilisch, Navarroaragonesisch). Historisch greifbar wird die katalanische Sprache ab dem 8. bis 10. Jahrhundert in den Grafschaften der Spanischen Mark, also dem Gebiet des karolingischen Reichs beiderseits der Pyrenäen. Katalanische Wörter können in lateinischen Texten des 9. Jahrhunderts nachgewiesen werden, erste vollständige schriftliche Zeugnisse wie z. B. die anonyme Predigtsammlung Homilies d'Organyà entstammen dem späten 12. Jahrhundert. Der Schriftsteller und Philosoph Ramon Llull, der von 1235 bis 1315 lebte, galt als der „Dante der katalanischen Literatur“ und gab der Sprache Glanz und Ansehen.
Im Mittelalter war das Sprachgebiet des Katalanischen noch recht einheitlich. Während des 12. und 13. Jahrhunderts breitete sich das Katalanische im Zuge der territorialen Eroberungen der aragonesischen Krone nach Süden und Osten aus; die Sprachgrenze wurde trotzdem erst nach der Ausweisung der Morisken 1609–1611 und der nachfolgenden Wiederbevölkerung der von ihnen verlassenen Orte festgelegt.

Mit der Ausdehnung des Herrschaftsbereichs des Grafen von Barcelona und des nachfolgenden katalanisch-aragonesischen Staatenbunds in den Mittelmeerraum gewann auch die katalanische Sprache zwischen dem 13. und 15. Jahrhundert an Bedeutung. So erschien als eines der ersten in katalanischer Sprache gedruckten Bücher 1502 in Perpignan ein zweisprachiges Wörterbuch, das Vocabolari molt profitos per apendre Lo Catalan Alamany y Lo Alamany Catalan („Ein gar nützliches Wörterbuch, wodurch Katalanen Deutsch und Deutsche Katalanisch lernen können“). Das Original dieses Wörterbuches bewahrt die Biblioteca de Catalunya in Barcelona auf. Der in Heidelberg geborene katalanische Buchdrucker Joan (Johann) Rosembach hatte es hergestellt. Bereits 1415 verarbeitete der deutsche sprachbegabte Dichter und Diplomat Oswald von Wolkenstein die Katalanische Sprache in seinen Gedichten.Ende des 15. Jahrhunderts vereinigten sich die Kronen von Aragón und Kastilien durch die Heirat der Reyes Católicos, des Königs Ferdinand von Aragón und der Königin Isabella von Kastilien, wobei Kastilien von Anfang an die Oberhand hatte. Dadurch verdrängte die kastilische Sprache (Castellano oder „Spanisch“) das Katalanische als Literatursprache. Als Rechts-, Amts- und Umgangssprache blieb das Katalanische zunächst jedoch noch relativ wenig bedrängt.
Nach Ende des spanischen Erbfolgekrieges (1701–1714) verlor Katalonien seine politische Eigenständigkeit, und die Bourbonen-Könige trieben die Entwicklung des spanischen Zentralstaats in seiner heutigen Form voran. Das kastilische Spanisch wurde als spanische Amtssprache durchgesetzt, und im Jahre 1716 wurde es per Gesetz als Unterrichtssprache verbindlich festgelegt.
1779 ging die Obrigkeit so weit, dass sie sogar Theaterstücke auf Katalanisch verbot.

Das 18. Jahrhundert gilt deshalb als der Tiefpunkt in der katalanischen Sprachgeschichte, als die Zeit der Decadència.
Erst mit der Romantik erlebte das Katalanische im 19. Jahrhundert einen neuen Aufschwung. In der Wiedergeburt, der Renaixença, fand die Sprache wieder Unterstützung und wurde zum Gegenstand linguistischer Forschung. In den 1930er Jahren erlebte das Katalanische eine Blüte, die aber durch den Spanischen Bürgerkrieg und den Sieg der nationalistischen und zentralistischen Franco-Diktatur jäh zerstört wurde.
In den Anfangsjahren der Franco-Diktatur wurde das Katalanische unterdrückt. Viele Ortsnamen im katalanischen Sprachgebiet wurden hispanisiert. Teilweise wurden sogar Personennamen ins Spanische übersetzt. Erst mit Beginn der 1960er Jahre wurde der katalanischen Sprache sehr zögerlich zuerst im kirchlichen Bereich wieder etwas Raum gegeben. Eine wichtige Rolle spielte dabei das Kloster Montserrat, wo auch während der Franco-Diktatur die Messen in der verbotenen katalanischen Sprache gefeiert wurden. Nach Francos Tod 1975 dauerte es noch drei Jahre, bis alle Beschränkungen aufgehoben wurden. In den Jahrzehnten der Unterdrückung hatte das Katalanische aber stark an Boden verloren. Zudem hatte die wohlhabende Region Katalonien viele Spanier aus anderen Teilen des Landes angezogen, die meist nicht oder nur sehr zögerlich Katalanisch lernten.
Dies ist einer der Hauptgründe, warum die katalanische Sprache von Seiten der Regionalregierung in den letzten 25 Jahren so stark gefördert wurde. Dieser normalització genannte Prozess verlief und verläuft nicht reibungslos, da viele der Zugewanderten und ein Teil des zentralistischen Großbürgertums das Katalanische ablehnen. Durch eine geschickte Politik ist es katalanischen Politikern aber gelungen, als unverzichtbare Koalitionspartner der Zentralregierung dieser weitere Zugeständnisse abzuringen und so die einstige Position des Katalanischen zumindest teilweise wiederherzustellen.
Dieser Prozess ist noch immer im Gange und soll nach dem Willen der Befürworter des Katalanischen erst abgeschlossen sein, wenn alle, die im ursprünglichen Verbreitungsgebiet des Katalanischen leben, diese Sprache zumindest als Zweitsprache vollständig beherrschen.
Die Hispanisierung von Personennamen wurde nach der Demokratisierung Spaniens rückgängig gemacht. Auch die ursprünglichen katalanischen Ortsnamen werden wieder offiziell verwendet.
Die heutige Diglossiesituation unterscheidet sich daher deutlich von der Situation unter Franco: Zwar können mehr als 95 % der in Katalonien Lebenden (also auch der Anteil mit katalanischer Muttersprache) Spanisch (Castellano) schreiben, gerade einmal 60 % der Bevölkerung beherrscht aber auch das schriftliche Katalanisch. Das betrifft vor allem Zuwandererfamilien und die ältere Generation der Katalanischsprachigen, die aufgrund der Schul-, Medien- und Sprachpolitik Francos die Schriftform ihrer eigenen Muttersprache – im Gegensatz zum vom zentralistischen System favorisierten Spanisch – nie erlernen konnten. In fast allen offiziellen Bereichen der Gesellschaft hat sich aber das Katalanische durch die normalització durchgesetzt; eine sehr breite Mehrheit innerhalb der Bevölkerung, auch unter den Castellano-Sprachigen, befürwortet die katalanische Sprachenpolitik: 90 % des Schulunterrichts wird auf Katalanisch abgehalten, ein Großteil der Vorlesungen in den Universitäten ist auf Katalanisch, und für den kompletten öffentlichen Dienst müssen Katalanischkenntnisse nachgewiesen werden. In den letzten Jahren hat sich deshalb ein regelrechter Boom in Sprachschulen, die Katalanischzertifikate ausstellen, ergeben. Im alltäglichen Sprachgebrauch neigen Sprecher mit katalanischer Muttersprache zumeist dazu, sich der jeweiligen Muttersprache des Gesprächspartners zu bedienen. Castellano-Sprecher hingegen bleiben auch gegenüber Katalanischsprachigen tendenziell bei ihrer eigenen Sprache. Dies führt zu einer im Verhältnis zur demographischen Verteilung stark überproportionalen Präsenz des Castellano. Es kommt aber durchaus auch vor, dass eine Konversation zweisprachig geführt wird und sich die Gesprächsteilnehmer ihrer jeweiligen Muttersprache bedienen.


=== Heutige Institutionen ===

La Plataforma per la Llengua ist eine zivilgesellschaftliche Organisation, für die mehrere katalanische Intellektuelle und bekannte Persönlichkeiten bürgen. Sie vereinigt in sich eine große Anzahl an Körperschaften und Personen, die sich für die Anerkennung und Verbreitung der katalanischen Sprache einsetzen. Zu ihren Hauptaktivitäten zählen das Erstellen von soziolinguistischen Studien und eine kontinuierliche Überwachung der politischen und gesellschaftlichen Stellung des Katalanischen. Dies geschieht in Zusammenarbeit mit anderen Körperschaften, Stiftungen und öffentlichen Behörden. Seit mehr als 15 Jahren haben ihre Aktivitäten mehrere Auszeichnungen erhalten – wie etwa im Jahr 2008 den „Premi Nacional de Cultura“ (Kulturpreis) der katalanischen Landesregierung für ihr Engagement für die Verbreitung der katalanischen Sprache.


== Grammatik und Wortschatz ==


=== Alphabet und Aussprache ===


==== Orthografische Besonderheiten ====
Die katalanische Schriftsprache bedient sich, ebenso wie die spanische, des Tremas, um die Aussprache des Phonems /u/ deutlich zu machen, wenn dieses zwischen g oder q und e bzw. i steht.Bsp.: llengües ‚Sprachen‘
Bsp.: qüestió ‚Frage‘Darüber hinaus nutzt das Katalanische (anders als das Spanische) das Trema auch, um anzuzeigen, dass i und u nach Vokal silbisch sind:Bsp.: països (dreisilbig, ohne Trema wäre es zweisilbig) ‚Länder‘
Bsp.: diürn (zweisilbig, ohne Trema wäre es einsilbig mit i als Silbenträger) ‚zum Tag gehörig‘Außerdem verwendet das Katalanische zwischen zwei „l“ einen so genannten punt volat (deutsch Mittelpunkt, wörtlich ‚geflogener Punkt‘), wenn die Aussprache [l] beibehalten werden soll.Bsp.: coŀlecció ‚Sammlung‘, Aussprache [l], von manchen Sprechern gelängt gesprochen: [l:]
im Gegensatz zu castellà ‚kastilische Sprache‘, Aussprache [ʎ]Die Verwendung dieses Mittelpunktes ist in ihrer Bedeutung linguistisch gesehen einzigartig. Vergleichbar sind allenfalls die Verwendung des Bindestrichs zur Trennung von „s“ und „ch“ im Engadinischen, des punt interior (deutsch Mittelpunkt) im Gaskognischen oder des punt volat im Franko-Provenzalischen.


=== Morphologie und Syntax ===
Die katalanische Grammatik weist eine Reihe von Ähnlichkeiten zur spanischen und französischen Grammatik auf, aber auch einige Besonderheiten. Im Bereich der Substantive existiert, wie allgemein in den romanischen Sprachen, auch im Katalanischen eine Unterscheidung zwischen Feminina und Maskulina, Kasusformen bilden nur verbundene, klitische Personalpronomen, die bei der 3. Person auch noch zwischen Akkusativ- und Dativobjekt unterscheiden. Katalanisch ist ebenso wie Spanisch, aber anders als Französisch, eine Pro-Drop-Sprache, d. h. erlaubt die Weglassung von Subjektpronomen und benutzt unverbundene Subjektpronomen daher vor allem kontrastiv.
Die Konjugation umfasst einfache (synthetische) Zeitbildungen im Konjunktiv, Konditional, Präteritum und Futur (das auch hier, wie z. B. im Französischen, auf der Basis der Infinitivformen gebildet wird). Es gibt hinsichtlich der Konjugationsmuster drei Verbalklassen, die erste Gruppe mit dem Infinitiv auf -ar, die zweite auf -er und -re, und die dritte auf -ir. Auffällig dabei ist aber, dass nicht alle Verben mit derselben Infinitivendung gleich konjugiert werden, es gibt eine große Zahl von Ausnahmen. Els verbs conjugats von Joan Baptista Xuriguera zählt 120 Konjugationstabellen auf.
Bei den zusammengesetzten Zeiten gibt es auch im Katalanischen die häufig anzutreffenden Passivformen mit dem Hilfsverb „sein“ + Partizip und Perfektbildungen mit dem Hilfsverb „haben“. Daneben besitzt das Katalanische auch noch eine weitere und sehr ungewöhnliche periphrastische Vergangenheitsform (genannt pretèrit perfet perifràstic), die mit einem Hilfsverb anar ‚gehen‘ gebildet wird, dessen Präsensformen vom lateinischen vadere herrühren (vgl. vaig, vas, va, vam/vàrem, vau/vàreu, van/varen), und dazu dem Infinitiv des Vollverbs. Beispiel:
El  teu  germà   va    venir   anit
Der dein Bruder „geht“ kommen letzte-Nacht (bzw. heute-Nacht)
= Dein Bruder kam gestern Nacht

Man vergleiche die äußerlich gleiche französische Konstruktion (il) va venir, die jedoch die Bedeutung eines Futurs hat (ebenso das Westromanische allgemein, so auch span.-port. ir (+ a) + Inf.). Das Katalanische ist die einzige romanische Sprache (und vielleicht auch die einzige überhaupt), bei der das Hilfsverb „gehen“ eine Vergangenheitsform bezeichnet. Allerdings ist zu ergänzen, dass einige Formen in der eigentlichen Bedeutung „gehen“ anders lauten als in dieser Funktion als Hilfsverb, etwa anem ‚wir gehen‘ gegenüber vàrem venir ‚wir kamen‘.
Besonderheiten finden sich auch in der Wortstellung. Meistens wird die Wortstellung im Satz des Katalanischen (wie bei den anderen romanischen Sprachen) als Subjekt-Verb-Objekt (SVO) angegeben. Bei näherem Hinsehen findet sich jedoch, dass vor allem die Stellung des Subjekts maßgeblich je nach der Informationsgliederung variiert: Wenn das Subjekt einem Topik entspricht, d. h. bekannte Information aufnimmt, steht es vor dem Verb, wenn es jedoch neuer Information entspricht, muss es am Satzende stehen. Man sieht dies am Verhalten im Kontext von Fragen:
Dieses Verhalten unterscheidet sich von dem in strikten SVO-Sprachen wie dem Englischen: What did John bring? — John brought the BOOK. und ebenso Who brought the book? – JOHN brought the book.
Weiterhin findet man bei intransitiven Verben, dass es generell „schwer festzustellen ist, ob die Stellung des Subjekts vor oder nach dem Verb neutraler wirkt“. Diese und andere Eigenschaften nachgestellter Subjekte haben dazu geführt, dass einige Linguisten die Stellung des Subjekts am Satzende (also VOS) für die Grundstellung halten. Die Möglichkeit eines nachgestellten Subjekts (Inversion) besteht in anderen romanischen Sprachen ebenfalls, ist aber im Katalanischen (ähnlich wie im Spanischen und Portugiesischen) stärker ausgeweitet als z. B. im Französischen oder Italienischen.


=== Wortschatz im Vergleich zu anderen romanischen Sprachen ===
Katalanisch ist innerhalb der romanischen Sprachen sehr nahe mit dem Okzitanischen verwandt, wie es sich unter anderem am Wortschatz erkennen lässt.
Im Gegensatz zum Spanischen ist es beim Katalanischen nicht zu einer umfangreichen Diphthongierung (von /o/ zu /we/ und von /i/ zu /je/) gekommen. Auch gab es die Entwicklung, mit f anlautende Wörter zu /h/ (heute stumm) zu verschieben, nicht:

Es dienen für das Galloromanische auch andere Etyma als Grundlage als im Iberoromanischen. Auch hier wird die Stellung des Katalanischen als Übergangssprache (llengua-pont) deutlich:

Andere Wörter wiederum deuten auf keine Verwandtschaft mit dem Spanischen oder Französischen:

Eine Besonderheit des Katalanischen ist, dass das auslautende /n/ bei vielen Wörtern geschwunden ist. Bei der Pluralbildung wird es jedoch wieder beachtet:

Das katalanische Lexikon weist auch germanischstämmige Wörter auf, die in der Zeit der Völkerwanderung ihren Weg in die Sprache gefunden haben. Allerdings machen sie nur einen kleinen Teil des Wortschatzes aus:


== Sprachbeispiel ==
Allgemeine Erklärung der Menschenrechte, Artikel 1:

„Tots els éssers humans neixen lliures i iguals en dignitat i en drets. Són dotats de raó i de consciència, i han de comportar-se de forma fraternal els uns amb els altres.“
Deutsch: Alle Menschen sind frei und gleich an Würde und Rechten geboren. Sie sind mit Vernunft und Gewissen begabt und sollen einander im Geist der Brüderlichkeit begegnen.


== Dialektale Gliederung ==
Es ist üblich, das Katalanische in zwei Dialektgruppen einzuteilen, die sich hauptsächlich durch den Vokalismus in unbetonten Silben unterscheiden: Das Westkatalanische (català occidental) und das Ostkatalanische (català oriental). Diese Ausdrücke können insofern  zu Verwechslungen führen, da sie in der Alltagsprache auf die Dialekte innerhalb des eigentlichen Kataloniens bezogen, also im engeren Sinne gebraucht werden.
Das Westkatalanische umfasst:

das Valencianische (valencià) in einem großen Teil des Landes Valencia;das Nordwestkatalanische (català nord-occidental), im westlichen Katalonien gesprochen und deshalb im Alltag als „Westkatalanisch“ bezeichnet, es reicht bis in die Alta Ribagorça in Aragonien und wird auch in Andorra gesprochen.Das Ostkatalanische umfasst:

das Nordkatalanische (català septentrional) oder Roussillonesische (rossillonès) auf französischem Staatsgebiet, mit Zentrum in Perpignan (Perpinyà);das Zentralkatalanische (català central) mit Zentren in Barcelona und Tarragona, auf dem im Wesentlichen die Schriftsprache beruht und das, da im östlichen Katalonien gesprochen, im Alltag als „Ostkatalanisch“ bekannt ist;das Balearische (balear), bestehend aus Ibizenkisch (eivissenc, auf Ibiza und Formentera), Mallorquinisch (mallorquí) und Menorquinisch (menorquí);das Algherische (alguerès) in der Stadt Alghero auf Sardinien, dessen Zuordnung zum Ostkatalanischen allerdings von manchen Gelehrten bestritten wird (siehe unten).Jeder dieser sechs Dialekte mit Ausnahme des Algherischen hat wiederum seine Unterdialekte. Zudem gibt es Übergangsdialekte (parlars de transició), namentlich zwischen Nordwestkatalanisch und Valencianisch (Zentrum: Tortosa) sowie zwischen Roussillonesisch und Zentralkatalanisch.Das Mallorquinische wie auch das Valencianische verfügen über eine größere Anzahl von sprachlichen Dokumenten und Literatur.Besonders hervorzuheben sind die bestimmten Artikel in den balearischen Dialekten (Mallorquinisch, Menorquinisch und Ibizenkisch), die anders als in allen anderen romanischen Sprachen und Dialekten (mit Ausnahme des Sardischen) auf lat. IPSE, IPSA und nicht auf lat. ILLE, ILLA zurückgehen: es, s (mask.), sa, s (fem.) < lat. IPSE, IPSA, vgl. auch sard. su, sa "der", "die". In der Verbalflexion ist die 1. Ps. Sg. hervorzuheben, die dem Verbstamm entspricht: lat. CANTO > mallork. cant aber kat. (Standard): canto. Im Wortschatz zeigen sich auch einige Nähen zur Galloromania: cercar "suchen" (vgl. frz. chercher, ital. cercare) aber kat./ span. buscar, qualque "einige" (vgl. frz. quelque, ital. qualche) aber kat./ span. algun.
Im Valencianischen fällt auf, dass auslautendes -r (auch im Infintiv) im Gegensatz zum Standardkatalanischen gesprochen wird. Die 1. Ps. Sg. Präsens des Verbs lautet auf -e: cante statt canto "ich singe".


== Algherisch ==
In geringem Umfang (rund 20.000 Sprecher) wird eine katalanische Varietät (L’Alguerés) auch in Alghero (kat.: L’Alguer) auf Sardinien gesprochen. Im Deutschen findet man auch den Namen Algueresisch. Die Stadt gehörte seit 1354 zum Königreich Aragón; ihre ursprünglichen Einwohner wurden von katalanischen Siedlern aus Campo de Tarragona, Barcelona, Valencia, Tortosa und Mallorca vertrieben. In dieser Varietät vermischen sich ostkatalanische, valencianische und mallorquinische mit nordkatalanischen Einflüssen aus dem Roussillon und Elementen des Sardischen. Schon in der unterschiedlichen Schreibweise des Namens des Dialekts (westkatalanisch: alguerés oder ostkatalanisch: alguerès) drückt sich die mangelnde Übereinstimmung in der Frage der ost- oder westkatalanischen (valencianischen) Herkunft des Dialekts aus. Der spanisch-italienische Linguist Eduardo Blasco Ferrer konstatiert die Dominanz valencianischer und balearischer Elemente. Der in Alghero geborene Linguist Rafael Caria, der sich für die Anerkennung der Varietät als schulisches Wahlfach einsetzte, hält die älteren Sprachdokumente in lexikalischer, morphologischer und phonetischer Hinsicht für westkatalanisch. Diese Ansicht vertritt auch die Akademie für die valencianische Sprache. Nach dem Abbrechen der Kontakte nach Valencia und den Balearen verstärkten sich jedoch die Einflüsse des Ostkatalanischen (vor allem in phonetischer Hinsicht: kurze offene Vokale) und des Spanischen und Italienischen. Daher wird die Mundart in der von der offiziellen Sprachenpolitik der katalanischen Regierung dominierten internationalen Katalanistik heute zumeist zum Bloc oriental des Katalanischen gezählt, also zu den ostkatalanischen Varietäten gerechnet. Andreu Bosch lässt die Frage offen. Die Varietät ist auf Sardinien als Minderheitensprache anerkannt.Italienische Lehnwörter im Algherischen sind beispielsweise: esvilupo "Entwicklung", guiatxo "Eis", ugual "gleich"; sardischen Ursprungs ist z. B. murendu "Esel".


== Siehe auch ==

Sprachen in Frankreich
Sprachen auf Mallorca
Liste katalanisch-spanischer Ortsnamen im katalanischen Sprachgebiet
Die Nationalhymne von Katalonien


== Literatur ==
Günter Holtus, Michael Metzeltin, Christian Schmitt (Hrsg.): Lexikon der Romanistischen Linguistik. 12 Bände. Niemeyer, Tübingen 1988–2005; Band V,2: Okzitanisch, Katalanisch. 1991.
José Hualde: Catalan. Routledge, London 2005.
Artur Quintana: Handbuch des Katalanischen. 4. Auflage. Editorial Barcino, Barcelona 1997, ISBN 84-7226-671-0.
Max Wheeler: Catalan. In: Martin Harris, Nigel Vincent: The Romance languages. Routledge, London 1988. (Nachdruck 2000). S. 170–208.


== Wörterbücher und Grammatiken ==


=== Bilinguale Wörterbücher ===
Lluís C. Batlle, Günter Haensch, Tilbert D. Stegmann, Gabriele Woith: Diccionari Català – Alemany / Katalanisch – Deutsches Wörterbuch. 1060 Seiten, Barcelona 1991 (Enciclopèdia Catalana), ISBN 84-7739-259-5.
2. erweiterte und aktualisierte Auflage, 1072 Seiten, Barcelona 2005, ISBN 84-412-1399-2.
Lluís C. Batlle, Günter Haensch, (Eckhard Kockers, Tilbert D. Stegmann ab 3. Auflage): Diccionari Alemany – Català / Deutsch – Katalanisches Wörterbuch, 659 Seiten, Barcelona 1993 (= Enciclopèdia Catalana), ISBN 84-85194-18-7.
3. Auflage, 834 Seiten, Barcelona 2006, ISBN 84-412-1452-2.
Diccionari Català – Alemany / Deutsch – Katalanisches Wörterbuch. (bearbeitet von der Redaktion der Enciclopèdia Catalana, Leitung: Marc Sagristà i Artigas), 551 Seiten (Kleinwörterbuch), Barcelona 1996 (Enciclopèdia Catalana), ISBN 84-412-2574-5 (Obsolet. 1998 ersetzt durch Diccionari Bàsic Català–Alemany, Alemany–Català. Neueste Auflage 2008.)
Andrea Rienitz: Diccionari Català – Alemany / Alemany – Català. (Diccionaris Arimany), 187 Seiten (Kleinwörterbuch), Barcelona 1991, ISBN 84-404-6197-6.
Langenscheidts Universal-Wörterbuch Katalanisch (Katalanisch – Deutsch / Deutsch – Katalanisch) (bearbeitet von der Redaktion der Enciclopèdia Catalana, Leitung: Marc Sagristà i Artigas), Berlin/München/Barcelona 1996 und 2000 (Langenscheidt / Enciclopèdia Catalana), 553 Seiten (Kleinwörterbuch), ISBN 3-468-18394-1 (vergriffen, keine Neuauflage)


=== Monolinguale Wörterbücher ===
Institut d’Estudis Catalans: Diccionari de la llengua catalana. Erste Edition (DIEC): 1908 Seiten, Barcelona / Palma / València 1995, ISBN 84-412-2477-3. Zweite Edition (DIEC2): 1762 Seiten, 2007, ISBN 978-84-297-5977-8. Dieses rein katalanischsprachige Werk hat normative Kraft und stellt gewissermaßen das Gegenstück zum deutschen Duden dar.
Gran Enciclopèdia Catalana: Gran Diccionari de la Llengua Catalana (GDLC). 1824 Seiten, ISBN 978-84-412-2790-3. Dieses Werk ist das umfangreichste Wörterbuch der katalanischen Sprache. Es enthält unter anderem alle Einträge des DIEC.


=== Grammatiken ===
Institut d’Estudis Catalans: Gramàtica de la llengua catalana. 1. Auflage, Barcelona, November 2016, ISBN 978-84-9965-316-7, 1438 Seiten
Jenny Brumme: Praktische Grammatik der katalanischen Sprache, Wilhelmsfeld 1997, Gottfried Egert Verlag, ISBN 3-926972-53-X, 430 Seiten


== Weblinks ==

Miniporträt Katalanisch (PDF 343,74 kB)
– Plataforma per la llengua
– Homepage des Deutschen Katalanistenverbandes
– Katalanistik Homepage der Universität Frankfurt
– Kostenlose Online-Version des mehrsprachigen Wörterbuchs der Enciclopèdia Catalana
– Diccionari català-valencià-balear (das DCVB oder auch nach seinem Urheber der Alcover genannt; ein 10-bändiges, einsprachiges, katalanisches Wörterbuch im Netz)
– Freies GPL Deutsch-Katalanisches Wörterbuch
– Umfangreicher, kostenloser Online Sprachkurs
Són bojos, aquests catalans!? kat. Spinnen die, die Katalanen!? - Video (Deutsche Untertitel) – YouTube


== Einzelnachweise ==
(Die Kurzverweise beziehen sich auf die Werke in der obenstehenden Literaturliste)