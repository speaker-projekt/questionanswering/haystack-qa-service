Eine Non-Profit-Organisation (NPO; Deutsch: nicht gewinnorientierte Organisation) verfolgt, im Gegensatz zur For-Profit-Organisation (gewinnorientierte Organisation), keine wirtschaftlichen Gewinnziele. Sie dient beispielsweise sozialen, kulturellen oder wissenschaftlichen Zielen ihrer Mitglieder, die in gemeinnütziger oder eigennütziger Weise verfolgt werden können. Dies ist meistens in einer Satzung festgelegt.


== Definition ==
Der Begriff „Non-Profit-Organisation“ ist weder im alltäglichen Sprachgebrauch noch in den Fachwissenschaften einheitlich und eindeutig definiert. Zahlreiche Autoren weisen darauf hin, dass mit der Bezeichnung „Non-Profit“ zunächst nur eine Negativ-Abgrenzung vorgenommen wird. So werden unter dem Terminus in der Regel Organisationen zusammengefasst, die nicht in erster Linie erwerbswirtschaftliche Ziele verfolgen und keine öffentlichen Verwaltungsbehörden sind. Darunter fallen sowohl verschiedene öffentliche Institutionen (z. B. Schulen, Museen und Theater, Kliniken) als auch sehr unterschiedliche private Vereinigungen (z. B. Parteien, Stiftungen, Bürgerinitiativen, gemeinnützige Organisationen, Selbsthilfegruppen oder Vereine, die lediglich der eigenen Freizeitgestaltung dienen).


=== Ergänzung zu Staat und Markt ===
Non-Profit-Organisationen nehmen bestimmte Zwecke der Bedarfsdeckung, Förderung oder Interessenvertretung bzw. Beeinflussung (Sachzieldominanz) für ihre Mitglieder (Selbsthilfe) oder Dritte wahr. Die Organisationen gehören zum Nonprofit-Bereich.


== Leitung ==
Als Vereine, Verbände, Selbstverwaltungskörperschaften, gemeinnützige Gesellschaften (gGmbH, gUG oder gAG), Genossenschaften oder Stiftungen werden sie von gewählten Ehrenamtlichen geleitet und können durch freiwillige Helfer in ihrer Arbeit unterstützt werden. Ihre Leitungsorgane können gewählt oder, wie bei Stiftungen, durch bestimmte Personen oder Institutionen berufen werden.


== Gemeinnützigkeit ==

In Deutschland wird eine eventuelle Gemeinnützigkeit im Rahmen eines staatlichen Anerkennungsverfahrens bei Beantragung von Gemeinnützigkeitsstatus auf Plausibilität überprüft. Zuständig ist normalerweise das Finanzamt, bei dem eine Befreiung von der Körperschaftsteuer beantragt wird. Der Steuerbescheid dient gleichzeitig als Nachweis der Gemeinnützigkeit und ist Grundlage zur Ausstellung von steuermindernden Zuwendungsbescheinigungen. Typische Rechtsformen von Non-Profit-Organisationen sind dort die gemeinnützige GmbH, die gemeinnützige Aktiengesellschaft und der eingetragene Verein (e. V.).


== Finanzierung und Controlling ==
Die NPOs finanzieren ihre Leistungen (Individualgüter, meritorische Güter oder Kollektivgüter) über Mitgliederbeiträge, Spenden, Zuschüsse, Preise oder Gebühren. Im internationalen Durchschnitt finanzieren sie sich zu 53 % über Entschädigungen für Dienstleistungen, zu 35 % über Staatsbeiträge und nur zu 12 % über Spendeneinnahmen. Insgesamt setzen NPOs weltweit jährlich fast zwei Billionen US-Dollar um. Dies entspricht in etwa dem Bruttoinlandsprodukt von Frankreich.Erzielte Überschüsse dürfen nicht als Kapitalrendite direkt an Mitglieder oder Träger ausgeschüttet werden. Gewisse Rückvergütungen sind im Verhältnis zur Leistungsbeanspruchung möglich. Übergänge von der Privatautonomie zur Staats- oder Marktsteuerung in Teilbereichen sind möglich und häufig.Das Interesse sozialer Dienste am Controlling nimmt in dem Ausmaß zu, in dem die mangelnde Eignung finanzieller Kennzahlen zur Steuerung von Unternehmen erkannt wird, erst recht, wenn diese Unternehmen, wie viele in der sozialen Arbeit, sich nicht am Gewinn orientieren. Wenn auch im sozialen Bereich die Feststellung von Zusammenhängen im naturwissenschaftlichen Sinne schwer möglich ist, wird eine Einschätzung der Ergebnisse der angebotenen Maßnahmen als sinnvoll und machbar gesehen. Dabei wird empfohlen, um der Komplexität sozialer Dienstleistungen gerecht zu werden, auf einen multidimensionalen Rahmen zurückzugreifen. Neben der finanziellen Dimension zählen für NPOs im sozialen Bereich auch der Grad der Auftragserfüllung, die Sicht der Leistungsempfänger sowie jene des Personals.Die genannten Dimensionen sollen folgende grundsätzliche Fragen beantworten:

Auftragserfüllung: Wie sehr erfüllt die soziale Dienstleistung die fachspezifische Zielsetzung?
Leistungsempfänger: Welchen Nutzen sehen die Leistungsempfänger?
Personal: Wie wirkt sich die Dienstleistung auf die Mitarbeiter aus?
Wirtschaftlichkeit: Welche finanziellen Folgen hat die soziale Dienstleistung für die NPO?Weitere bzw. andere Dimensionen werden prinzipiell nicht ausgeschlossen: Es ist immer eine Gratwanderung zwischen Übersichtlichkeit und Vollständigkeit. Die Betrachtung mehrerer Dimensionen ermöglicht es, sich ein ausgewogenes Bild über die Auswirkungen einer Maßnahme zu machen.
Der multidimensionale Ansatz soll sicherstellen, dass Widersprüche im Zielsystem leichter erkannt werden. Mögliche Konflikte können im Vorfeld angesprochen werden, anstatt unterschwellig das Gesamtergebnis der Organisation zu beeinträchtigen. Um allerdings bei der Fülle an Details den Überblick nicht zu verlieren, sind Schlüsselinformationen (wie z. B. Kennzahlen) unumgänglich. Es gilt, sich auf einen Blick ein ausgewogenes Bild von der Wirkung einer sozialen Maßnahme machen zu können.


== Geschichte ==
Der Begriff Non-Profit setzt profitorientiertes Wirtschaften, wie es im Kapitalismus üblich ist, voraus. Kapitalistische Prinzipien wie Privateigentum oder Selbstbestimmung der Arbeitskraft sind neben einem über die Subsistenz hinausgehenden Wohlstands- und Entwicklungsniveau Voraussetzungen für das Entstehen privater Non-Profit-Organisationen. Eine hypothetische staatliche Organisation, die Staatseigentum einsetzt und dazu die Arbeitskraft der Bürger mehr oder wenig zwangsweise einbezieht, kann selbst bei fehlender Gewinnabsicht nicht als Non-Profit-Organisationen bezeichnet werden. Daher haben NPOs in westlichen Staaten eine lange Geschichte. Nichtstaatliche Wohltätigkeit hat es auch in vormodernen Gesellschaften gegeben, historisch insbesondere in der Form von Stiftungen, mit denen beispielsweise Hospitäler oder Armenfürsorge finanziert wurden. Ein solches Stiftungswesen hat beispielsweise in Deutschland und dem Osmanischen Reich in der frühen Neuzeit existiert.


=== USA ===
Die Entstehungsgeschichte der Non-Profit-Organisationen in den USA ist die Geschichte der Bestrebung, neben politischer auch kulturelle Unabhängigkeit von Europa zu erlangen. Die Ursprünge der Non-Profit-Organisationen in den USA erwachsen aus der Ablehnung des europäischen Absolutismus im 18. Jahrhundert. Durch diese Ablehnung der Staatsmacht wird zunehmend nach zivilgesellschaftlichen Konzepten der Selbstverwaltung gesucht. Mit der zunehmenden sozialen Schieflage in Europa zur Zeit der Industrialisierung und den damit verbundenen gesellschaftlichen Veränderungen (z. B. Oktoberrevolution, Sozialistengesetze) wird in den USA nach Möglichkeiten gesucht, das Ausweiten der staatlichen Einflusssphäre zu verhindern.
Besonders Ende des 19. Jahrhunderts wird das Konzept des so genannten Wohlfahrtskapitalismus diskutiert, bei dem Arbeitgeber selbständig Sozialleistungen erbringen. Dies geschieht oft über betriebseigene Non-Profit-Organisationen.
Noch heute werden in den USA viele Aufgaben, die in Europa klassisch dem staatlichen Sektor zugeschrieben werden (Bildung, Kultur etc.), von Non-Profit-Organisationen erfüllt.
Die Klassifikation erfolgt durch das National Center for Charitable Statistics. NPOs können als 501(c) organization von der Steuerpflicht befreit werden.


=== Deutschland ===
Im Mittelalter war das Stiftungswesen christlich geprägt. Möglicherweise bedingt durch den steigenden Wohlstand bürgerlicher Kreise lässt sich ein Aufschwung privat finanzierter Wohltätigkeitsorganisationen ab dem späten 18. Jahrhundert feststellen. Das 19. Jahrhundert ist dann eine Blütezeit solcher Organisationen, ein großer Teil der bürgerlichen Elite war ehrenamtlich in ihnen aktiv oder trug zu ihrer Finanzierung bei. Besonders ist dies in den Stadtstaaten zu erkennen, wo sich Handelsbürger konzentrierten. In Frankfurt am Main gab es beispielsweise die Senckenbergischen Stiftungen (Bürgerhospital und medizinisch-wissenschaftliche Stiftung mit Bibliothek), die Städelsche Stiftung (Kunstschule und Museum), die Polytechnische Gesellschaft (Fortbildung von Arbeitern, Sparkasse für „kleine Leute“), das Rothschildsche Judenhospital (das allerdings nur zahlenden Mitgliedern offenstand), die Waisenhausstiftung sowie eine Armenküche, die von den bürgerlichen Damen des Frauenvereins betrieben und finanziert wurde. Der Frauenverein betrieb auch eine Schule. Das rege Vereinswesen der Zeit lässt sich generell schwer von den rein karitativ-gemeinnützigen Einrichtungen abgrenzen.


=== International ===
Seit Ende des letzten Jahrtausends befindet sich der Non-Profit-Sektor in einer Umbruchsituation, bedingt durch externe wie auch interne Faktoren. Gesellschaftspolitische Entwicklungen führen zu Verschiebungen in der Nachfrage nach sozialen Dienstleistungen, während leere öffentliche Kassen und finanziell geschwächte Sozialversicherungen in vielen europäischen Staaten Non-Profit-Organisationen mit tief greifenden Einschränkungen konfrontieren. Gleichzeitig erfährt der Sektor eine Intensivierung des Wettbewerbs getragen durch gewandelte gesetzliche Rahmenbedingungen – auf nationaler wie auf EU-Ebene – und einen entsprechenden Reformprozess in der öffentlichen Verwaltung, u. a. durch New Public Management.
Soziale Dienste sind von der Ökonomisierung der öffentlichen Hand besonders betroffen und stehen vor der Herausforderung, ihre Rechenschaftslegung auszuweiten und zu vertiefen, um den Nutzen ihrer Arbeit vor Fördergebern, Mitgliedern und Spendern zu belegen. Während sich bei gewinnorientierten Unternehmen die Erfolgsmessung auf einige, wenige quantitative Größen beschränkt – wie etwa der Umsatz oder die Rentabilität, ruft die Frage bei sozialen NPOs große Unsicherheiten hervor. Wann ist etwa die Integration von Zugewanderten erreicht? Oder: Wie verbessert sich die Lebenslage von behinderten Menschen? Es gehört zum Wesen von Anbietern sozialer Dienstleistungen, dass neben der Einhaltung finanzieller Rahmenbedingungen insbesondere Sachziele verfolgt werden, wie etwa die Senkung der Jugendkriminalität oder die Aktivierung von Senioren. Letztere jedoch sind selten so genau formuliert, dass eine Umsetzung ohne weiters möglich und überprüfbar wäre. Die Erfolgsmessung ist umso komplexer, wenn Ziele wirkungs- statt ressourcenorientiert formuliert werden. In den Augen von Anspruchsgruppen, wie Spendern, Ehrenamtlichen oder der öffentlichen Hand, zählen nicht nur die eingesetzten Mittel, sondern insbesondere die erreichten Ergebnisse. Auf der Ebene der eingesetzten Ressourcen richtet die NPO ihre Aufmerksamkeit auf die für ein bestimmtes Ziel notwendige Produktionsfaktoren (Inputs) wie etwa die Anzahl oder die Qualifikation der Mitarbeiter. Das Personal stellt eine der entscheidenden Ressourcen dar. Im Gegensatz dazu betreffen wirkungsbezogene Ziele die Ergebnisse, die erreicht werden sollen – sei es die Menge (Output), die objektive Wirkung (Effect), die subjektive Konsequenz (Impact) oder aber das Ergebnis für das Umfeld (Outcome). Der Übergang von Input- zu Outputzielen ist für die NPO mit erheblichen Folgen verbunden und schlägt sich deutlich in der Ausrichtung der Steuerungsinstrumente nieder. Wird etwa in einer Beratungsstelle für Jugendliche aus der Sicht des Ressourceneinsatzes über Personalstunden nachgedacht, so gilt die Aufmerksamkeit – wenn von Wirkung die Rede ist – der Verbesserung der Lebensumstände von Jugendlichen.


== Arten von Non-Profit-Organisationen ==
Grundsätzlich wird unterschieden zwischen:

Fremdleistungs-NPO (Dienstleistungserbringer für Dritte)
Eigenleistungs-NPO (Mitgliedervereinigung)
Mischformen aus beiden


=== Sozialer und ökologischer Bereich ===
Sozialwirtschaft
Soziale Arbeit, Unterstützung für Benachteiligte, Fürsorge
Schutz und Rettung
Umwelt-, Natur- und Tierschutz
Entwicklungszusammenarbeit
Das Ziel von in der Entwicklungszusammenarbeit tätigen Organisationen ist die nachhaltige und langfristige Verbesserung der Lebensumstände der Menschen in Entwicklungsländern.
Politische NGOs
Politische NGOs befassen sich mit Themen wie Globalisierung, Welthandel und sozialen Benachteiligungen.
Menschen- und Bürgerrechte
Bürgerrechts- und Menschenrechtsorganisationen setzen sich für die Einhaltung der Menschen- und Bürgerrechte ein und versuchen auf Menschenrechtsverletzungen aufmerksam zu machen.
Katastrophenhilfe, Überlebenshilfe, humanitäre Hilfe
In der humanitären Hilfe tätige Organisationen versuchen, Menschen in einer humanitären Notlage (z. B. bei Kriegen und Naturkatastrophen) zu schützen und ihnen zu helfen.


=== Kultureller Bereich ===
Künstlerförderung
Pflege und Erhaltung historischer Bücher Buchpatenschaft und Dokumente
Denkmalschutz-Organisationen setzen sich für einen Erhalt der Kultur- und Baudenkmale aus verschiedenen Epochen ein und fördern zeitgemäße, gute Architektur bei Neubauten.


== Siehe auch ==
Low-Profit-Organisation
Non-Profit-Marketing


== Literatur ==
Christoph Badelt (Hrsg.): Handbuch der Nonprofit-Organisation. 4., überarbeitete Auflage, Schäffer-Poeschel, Stuttgart 2007, ISBN 3-7910-1302-5.
Maria Laura Bono: NPO-Controlling – Professionelle Steuerung sozialer Dienstleistungen. Schäffer-Poeschel, Stuttgart 2006, ISBN 3-7910-2541-4.
Manfred Bruhn: Marketing für Nonprofit-Organisationen. Grundlagen – Konzepte – Instrumente. Kohlhammer, Stuttgart 2005, ISBN 3-17-018281-1.
Walter Fischer: Sozialmarketing für Non-Profit-Organisationen – Ein Handbuch. Orell Füssli, Zürich 2000, ISBN 3-280-02659-8.
Ekkehart Frieling: Kompetenz- und Organisationsentwicklung in Non-Profit-Organisationen: Gewerkschaften, Bildungsträger und öffentliche Verwaltung im wirtschaftlichen Wandel. Waxmann, Münster u. a. 2002, ISBN 3-8309-1184-X.
Marlies W. Fröse (Hrsg.): Management Sozialer Organisationen. Beiträge aus Theorie, Forschung und Praxis. Das Darmstädter Management-Modell. Haupt, Bern (u. a.) 2005, ISBN 3-258-06877-1.
Richard Häusler, Claudia Kerns: Unternehmen Umweltbildung. oekom, München 2006, ISBN 3-86581-047-0.
Stefan Nährlich, Annette Zimmer (Hrsg.): Management in Nonprofit-Organisationen. Eine praxisorientierte Einführung. Leske + Budrich, Opladen 2000, ISBN 3-8100-2295-0.
Michael Mroß: Management in der Sozialwirtschaft – Kompakt! Cp-Verlag, Leipzig 2014, ISBN 978-1-4959-7428-1.
Robert Purtschert: Marketing für Verbände und weitere Nonprofit-Organisationen. 2. Auflage, Haupt, Bern / Stuttgart / Wien 2005, ISBN 3-258-06913-1.
Patrick Renz: Project Governance: Implementing Corporate Governance and Business Ethics in Nonprofit Organizations. Physica, Heidelberg 2007, ISBN 978-3-7908-1926-7.
Ruth Simsa: Gesellschaftliche Funktionen und Einflußformen von Nonprofit-Organisationen. Eine systemtheoretische Analyse. Frankfurt am Main u. a. 2001, ISBN 3-631-36791-0.
Peter Schwarz: Organisation in Nonprofit-Organisationen. Haupt, Bern / Stuttgart / Wien 2005, ISBN 3-258-06885-2.
Armin Wöhrle: Grundlagen des Managements in der Sozialwirtschaft. Nomos, Baden-Baden 2003, ISBN 978-3-8329-0341-1.


== Weblinks ==


== Einzelnachweise ==