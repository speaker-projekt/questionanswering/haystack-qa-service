Das Verteidigungsministerium des Vereinigten Königreichs (englisch Ministry of Defence) ist das Hauptquartier der Streitkräfte des Vereinigten Königreichs und mit der Umsetzung der von der Regierung vorgegebenen Verteidigungspolitik beauftragt.


== Geschichte ==
In der Zwischenkriegszeit begutachteten Politiker und Mitarbeiter des öffentlichen Dienstes die Leistung des britischen Staates während des Ersten Weltkrieges und stellten einen verstärkten Koordinationsbedarf zwischen den drei Teilstreitkräften des Landes fest. Die Koalition des damaligen Premierministers David Lloyd George lehnte 1921 die Schaffung eines vereinten Verteidigungsministeriums ab, richtete aber den Generalstab im Jahre 1923 ein, um dem Wunsch nach verstärkter Zusammenarbeit zwischen den Waffengattungen nachzukommen. Aufgrund der Aktualität der Wiederaufrüstung richtete Stanley Baldwin in den 1930ern dann den Posten des Minister for Coordination of Defence („Minister zur Koordinierung von Verteidigungsangelegenheiten“) ein. Diesen nahm Lord Chatfield bis zum Ende der Regierung Chamberlain ein. Ein erfolgreiches Wirken blieb ihm aufgrund seiner mangelnden Autorität über die Ministerien der Teilstreitkräfte, denen er nicht übergeordnet war, und wegen seines mangelnden politischen Einflusses verwehrt.
Kurze Zeit nach seinem Amtsantritt im Jahr 1940 schuf Winston Churchill das Amt des Verteidigungsministers. Neben der bereits vorhandenen Koordinierungskompetenz wurde es nun, zur Steigerung seiner Wirksamkeit, dem Generalstab übergeordnet. Churchill übte das Amt bis 1946 aus, als die Regierung von Clement Attlee das britische Verteidigungswesen mit dem Ministry of Defence Act von 1946 reformierte. Der Verteidigungsminister nahm nun an Kabinettssitzungen teil, während dies für die Minister der Streitkräfteministerien, die weiterhin ihre direkte Befehlsgewalt behielten, nicht mehr zutraf.
Von 1946 bis 1964 blieb die Arbeit des heutigen Verteidigungsministeriums auf fünf Ministerien aufgeteilt; die Admiralität für die Royal Navy, das für die Britische Armee zuständige War Office und die zivile sowie die militärische Version des Luftfahrtministeriums der Royal Air Force. Diese wurden 1964 zum heutigen Verteidigungsministerium zusammengefasst, 1971 trat die Verteidigungsabteilung des Luftversorgungsministeriums hinzu.
Derzeitiger Verteidigungsminister im Kabinett Boris Johnson ist seit dem 24. Juli 2019 Ben Wallace.


== Siehe auch ==
Auslandseinsätze und -stützpunkte der British Army


== Weblinks ==
Offizielle Website des britischen Verteidigungsministeriums (engl.)