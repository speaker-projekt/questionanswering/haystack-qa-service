Die Norfolkinsel (Norfuk: Norfuk Ailen) ist eine Insel im Pazifischen Ozean, die zu Australien gehört. Nördlich von ihr liegt Neukaledonien, westlich Australien, im Süden Neuseeland. Der Name der Insel geht auf den englischen Seefahrer James Cook zurück, der sie 1774 entdeckte und zu Ehren von Mary Howard, Duchess of Norfolk benannte.


== Geographie ==

Die Norfolkinsel liegt rund 1400 Kilometer östlich des australischen Kontinents und ist Teil des knapp 1100 km langen Norfolk Ridge, der sich von Neukaledonien aus nach Süden hin erstreckt. Die Insel ist bis auf das südliche Gebiet mit der Hauptstadt Kingston von unzugänglichen Klippen umgeben. Höchste Erhebungen der Insel sind die beiden Berge Bates (319 Meter) und Pitt (318 Meter). Zum Territorium gehören neben der Norfolkinsel auch noch die kleineren, unbewohnten Inseln Nepean und Phillip. Die Norfolkinsel ist, ebenso wie Phillip Island, vulkanischen Ursprungs und bietet daher fruchtbaren Boden für die Landwirtschaft. Die größte Stadt der Insel ist Burnt Pine.


== Klima ==
Es herrscht subtropisches Klima mit einer Jahresdurchschnittstemperatur von 19 °C und 1350 mm Jahresniederschlag. Eine besondere Gefahr stellen die Zyklone dar, die besonders in den ersten Monaten des Jahres auftreten.


== Umwelt ==
Innerhalb der Interim Biogeographic Regionalisation for Australia gehört Norfolk Island zur Bioregion Pacific Subtropical Islands (PSI) und bildet dort die Subregion PSI02.


=== Flora ===
Auf der Insel gibt es 174 einheimische Pflanzen. Von 51 endemischen Arten gelten 18 als selten oder gefährdet. Vor der europäischen Kolonisierung war der größte Teil der Insel von subtropischem Regenwald bedeckt, mit der Norfolk-Tanne in exponierten Gebieten, Palmen und Baumfarnen in Feuchtgebieten sowie Lianen und Farnen im Unterholz. Ein Gebiet von fünf Quadratkilometern wurde 1986 zum Norfolk-Island-Nationalpark erklärt. Hier wächst der Norfolk-Baumfarn (Cyathea brownii), der als größter Baumfarn der Welt gilt.Der ursprüngliche Regenwald ist durch zahlreiche Neophyten bedroht.


=== Fauna ===

Nach der Abholzung des einheimischen subtropischen Regenwaldes sind viele Arten und Unterarten einheimischer Vögel ausgestorben. Dazu haben auch eingeführte verwilderte Säugetiere wie Ratten, Katzen, Schweine und Ziegen sowie konkurrierende Vogelarten wie Amseln und Pennantsittiche beigetragen. Zu den ausgestorbenen Arten gehören der endemische Norfolk-Kaka, die Norfolk-Erdtaube und die Norfolk-Inseldrossel. Obwohl die Insel politisch zu Australien gehört, zeigen viele Vogelarten Ähnlichkeiten zu den Arten Neuseelands. Auf der Nepean-Insel, die zu Norfolk gehört, brüten einige Seevogelarten. Der Solandersturmvogel war zu Beginn des 19. Jahrhunderts am Ort ausgestorben, brütet nun aber wieder auf der benachbarten Phillipinsel. Zu weiteren dort brütenden Seevögeln gehören der Kermadec-Sturmvogel, der Australische Tölpel, der Rotschwanz-Tropikvogel und die Rußseeschwalbe.


== Bevölkerung ==
2016 hatte die Insel 1748 Bewohner, was einer Bevölkerungsdichte von 51 Einwohnern pro Quadratkilometer entspricht. Die Bevölkerung setzt sich zu einem Drittel aus den Nachkommen der Bounty-Meuterer (Pitcairner) und zu zwei Dritteln aus im Laufe der Zeit zugewanderten Australiern, Neuseeländern und Polynesiern zusammen. Zwischen den beiden Gruppen kommt es oftmals zu Streitigkeiten. Während die Nachfahren der 1856 auf die Insel übergesiedelten Pitcairner, die sich in der Tradition ihrer Ahnen sehen, für eine konservative Politik mit der Unabhängigkeit der Norfolkinsel als Ziel eintreten, sind die Nachfahren der Zuwanderer progressiv und aufgrund ihrer Herkunft weltoffener – das heißt, sie hegen enge Kontakte zum Festland.
Die Mehrzahl der Bevölkerung spricht Englisch (45,5 %), 40,9 % sind noch des Norf'k-Pitcairn mächtig, einer Mischung aus der englischen Sprache des 18. Jahrhunderts und altem Tahitianisch. Der anglikanischen Kirche gehören weniger als ein Drittel (29,5 %) der Einwohner an, der Rest verteilt sich auf die Uniting Church in Australia (9,6 %) und die römisch-katholische Konfession (12,6 %). 26,8 % sind Atheisten.Der im März 2002 begangene Mord an der 28-jährigen australischen Restaurantleiterin Janelle Patton beschäftigte jahrelang die örtliche Polizei und Justiz und sorgte für einiges Aufsehen, war doch mehr als ein Jahrhundert lang keine solche Straftat auf der Norfolkinsel verübt worden. Im August 2006 begannen die Anhörungen in dem Prozess gegen den Angeklagten. Am 9. März 2007 ging der erste Mordprozess seit 151 Jahren mit einem Schuldspruch für den neuseeländischen Koch Glenn McNeill zu Ende.


== Geschichte ==
Eine Reihe archäologischer Untersuchungen in jüngerer Zeit erbrachte Erkenntnisse über eine frühe polynesische Besiedlung. In den Dünen der Emily Bay wurden Spuren eines ostpolynesischen Dorfes aus der Zeit des 12. bis 15. Jahrhunderts gefunden, die die vermutlich ersten Siedler der Norfolkinsel hinterließen. Sie bauten aus dem lokalen Basalt Häuser und Öfen, wo sie gefangene Fische, Schildkröten und Vögel brieten. Es wurden Artefakte aus Obsidian gefunden, die von den etwa 1300 Kilometer östlich gelegenen Kermadecinseln stammen. Warum diese Bewohner die Insel verlassen haben, ist nicht bekannt.
Als der britische Seefahrer James Cook am 10. Oktober 1774 als erster Europäer das abgelegene Eiland betrat, dem er später zu Ehren von Mary Howard, Duchess of Norfolk den Namen Norfolkinsel gab, fand er eine paradiesische, jedoch unbewohnte Landschaft vor. Die britische Krone nahm das Gebiet erst 1788 in Besitz. Aufgrund ihrer abgelegenen Lage sowie der zahlreichen für die Seefahrt nützlichen Ressourcen (beispielsweise Flachs und Nadelhölzer) war die Insel gut zur Anlage einer Sträflingskolonie geeignet, weshalb am 2. März 1788 eine Gruppe von 15 Gefangenen unter der Führung von Lieutenant Philip Gidley King auf der Norfolkinsel landete und die erste Siedlung gründete.Anfangs ein Sträflingslager für Verbrecher, die sich durch gute Führung auf dem Festland die Versetzung verdient hatten, entwickelte sich das Eiland immer mehr zur am meisten gefürchteten Sträflingsanstalt des Pazifiks. Der permanente Nachschub an Sträflingen sorgte für Platzmangel und Hungersnöte, die Kommandanten reagierten mit drakonischen Strafen auf jede Form von Widerstand. Mit dem Zusammenbruch der Holz- und Flachsverarbeitung wurde die Sträflingskolonie 1813 komplett aufgegeben; alle Gebäude wurden abgerissen, alle Güter in die Heimat verschifft und die Insel quasi in ihren Urzustand zurückversetzt.
Zwölf Jahre vergingen, bis die Norfolkinsel abermals für die britische Regierung interessant wurde. Bedingt durch den sprunghaften Anstieg der Kriminalitätsrate im eigenen Land wurden erneut Rufe nach einer Sträflingskolonie in den Weiten des Ozeans laut. Wieder wurde die Norfolkinsel auserkoren, diesmal jedoch als Gefängnis für Schwerstkriminelle. Fortan war die Umsiedlung auf die Insel mit ihrer 1825 wieder eingerichteten Sträflingskolonie die Höchststrafe für jeden Verbrecher. In einem Arbeitslager ließ man die Gefangenen unter extremen Bedingungen schuften. Es gab Unmengen an Verletzten und Toten zu beklagen. 1844 wurde die Norfolkinsel Teil Van-Diemens-Lands. Damals wusste noch kaum jemand in der britischen Heimat von den Geschehnissen in der Sträflingskolonie. Nach und nach drangen jedoch Berichte an die Öffentlichkeit, es kam zu zahlreichen Protesten, und so musste die Strafanstalt im Mai 1855 geschlossen werden. Durch die Goldfunde auf dem australischen Kontinent hatte der Pazifikraum ohnehin seine abschreckende Wirkung verloren, der Mythos von der unwirtlichen und trostlosen Meeresregion war jetzt nicht mehr aufrechtzuerhalten. Viele Briten zog es nun aus freien Stücken nach Ozeanien.
Am 8. Juni 1856 trafen die 194 Nachkommen der Bounty-Meuterer (sogenannte Pitcainer), die bis dato auf der 6000 km entfernten Insel Pitcairn gelebt hatten, auf der Norfolkinsel ein, da Pitcairn diese Menge Bewohner kaum mehr ernähren konnte. Insgesamt fünf Familien kehrten zwar 1858 und 1864 nach Pitcairn zurück, die meisten blieben jedoch und betrachteten das Gebiet fortan als ihre Heimat.


== Politik ==


=== Gründung ===
Die Insel war im März 1788 von der ersten Sträflingskolonie Australien in der Botany Bay als zweite britische Sträflingskolonie gegründet worden und unterstand den Weisungen des Gouverneurs der ersten Sträflingskolonie New South Wales Arthur Phillip. Nachdem die Insel im Jahr 1813 aufgegeben wurde, blieb sie ins Jahr 1825 menschenleer, bis es auf der Insel zur Errichtung eines Strafgefängnisses kam, das der britischen Krone direkt unterstand. Das Gefängnis wurde 1855 aufgegeben und 1856 ließen sich zahlreiche Nachkommen von Bounty-Meuterern mit Erlaubnis der britischen Kolonialverwaltung auf der Norfolkinsel nieder. Im gleichen Jahr wurde sie zu einem eigenen, New South Wales untergeordneten Territorium mit weitestgehender Autonomie. Erst im Jahr 1979 änderte sich die politischen Verhältnisse erneut.


=== Norfolk Island Act 1979 ===
Als australisches Gebiet wurde die Norfolkinsel ab 1979 vom Ministerium für Umwelt, Sport und Territorien verwaltet. Gültige Verfassung war das Norfolkinsel-Gesetz von 1979, welches der Insel ein bestimmtes Maß an Selbstständigkeit einräumte, so beispielsweise die Einrichtung einer eigenen Legislative, Polizei, Judikative und Zollbehörde. Rechtliches Fundament waren die australischen Gesetze, daneben existierte eine Vielzahl an lokalen Verordnungen. In öffentlichen Bereichen, die nicht durch australisches Recht geregelt werden, griff britisches Recht. Gerichtsorgane waren der Oberste Gerichtshof (Supreme Court) sowie ein Gericht für kleinere Vergehen, war der Court of Petty Sessions.
Staatsoberhaupt ist die britische Königin Elisabeth II., als Vertreter Australiens und der Krone auf der Insel fungiert der vom australischen Generalgouverneur und vom Federal Executive Council ernannte Administrator (seit dem 1. April 2017 Eric Hutchinson). Alle vier Jahre wurde ein neun Sitze starkes Parlament, die Norfolk Legislative Assembly, von allen Personen über 18 Jahren gewählt. Dabei standen jedem Wähler neun gleichwertige Stimmen zur Verfügung, von denen er einem Kandidaten maximal vier zukommen lassen konnte. Das Parlament wählte den Premierminister, welcher an der Spitze einer fünfköpfigen Regierung stand. Der letzte Regierungschef auf der Insel war Lisle Snell, der lediglich im Jahr 2016 ins Norfolk Island Regional Council gewählt wurde, das sich aus fünf Mitgliedern zusammensetzt.

Liste der Regierungschefs und Premierminister
10. August 1979 – 21. Mai 1986: David Ernest Buffett
21. Mai 1986 – 22. Mai 1989: John Terence Brown
22. Mai 1989 – 20. Mai 1992: David Ernest Buffett
20. Mai 1992 – 4. Mai 1994: John Terence Brown
4. Mai 1994 – 5. Mai 1997: Michael William King
5. Mai 1997 – 28. Februar 2000: George Charles Smith
28. Februar 2000 – 5. Dezember 2001: Ronald Coane Nobbs
5. Dezember 2001 – 2. Juni 2006: Geoffrey Robert Gardner
2. Juni 2006 – 28. März 2007: David Ernest Buffett
28. März 2007 – 24. März 2010: Andre Neville Nobbs
24. März 2010 – 20. März 2013: David Ernest Buffett
20. März 2013 – 17. Juni 2015: Lisle Snell


=== Norfolk Island Legislation Amendment Bill 2015 ===
Durch den Norfolk Island Legislation Amendment Bill 2015, der vom australischen Bundesparlament am 14. Mai 2015 erlassen wurde und den Royal Assent am 26. Mai 2015 erhielt, wurde die Selbstverwaltung der Norfolkinsel zum 30. Juni 2016 aufgehoben und die Insel ab dem folgenden 1. Juli der Gesetzgebung des Staates New South Wales unterstellt. Seitdem obliegt die Verwaltung der Insel dem Norfolk Island Regional Council. Die Angliederung an Australien ist in der Bevölkerung umstritten.
Die Insel wird in australischen Parlamentswahlen im Australian Capital Territory repräsentiert.


=== Norfolk Island Legislation (Migration) Transitional Rule 2016 ===
Seit 2016 ist die Norfolkinsel mit der Norfolk Island Legislation (Migration) Transitional Rule 2016 in die Migrationszone Australien einbezogen worden. Dies bedeutet, dass anlandende Boatpeople auf Flüchtlingsbooten keinen Antrag auf Asyl in Australien stellen können und sie in Einwanderungshaft in Australien festgesetzt werden.


== Wirtschaft ==

Das Bruttoinlandsprodukt der Norfolkinsel lag 2016 bei 81,8 Mio. AUD. Die Außenhandelsbilanz ist stark negativ; 2016 standen Importen von rund 60 Mio. AUD Exporte von weniger als 3 Mio. AUD gegenüber. Die Arbeitslosigkeit liegt mit 1,6 % (2016) auf sehr niedrigem Niveau.Wichtigster Wirtschaftsfaktor mit einem Anteil von annähernd 40 % an den wirtschaftlichen Aktivitäten des Landes ist der Tourismus, welcher der Bevölkerung einen gewissen Wohlstand und vor allem den Anschluss an den Rest der Welt gebracht hat. Insbesondere die mittlerweile restaurierten Gebäude der ehemaligen Sträflingssiedlung in Kingston (Kingston and Arthurs Vale Historic Area) und die ein Viertel der Insel ausmachenden Naturschutzgebiete (beispielsweise das Vogelreservat auf der Phillip-Insel, siehe auch Norfolk-Island-Nationalpark) sind beliebte Anlaufstellen für Besucher. Die von der Regierung beschlossene Beschränkung der maximalen Besucherzahl garantiert ganzjährig eine hohe Tourismusqualität. Die Insel ist über einen Flughafen zu erreichen; echte Häfen gibt es keine, sondern lediglich zwei Anlegestellen in Kingston und Cascade. Die aufwendige Entladung mit Hilfsschiffen führt zu vergleichsweise hohen Preisen für Importgüter.Der zweite bedeutsame Wirtschaftszweig ist die Landwirtschaft. Durch den Anbau von Getreide, Obst und Gemüse sowie die Erzeugung von Rindfleisch, Geflügel und Eiern kann sich die Insel größtenteils selbst versorgen. Exportiert werden vor allem die Samen der für die Norfolkinsel charakteristischen Zimmertannen, Rhopalostylispalmen, Avocados und für Philatelisten in aller Welt interessante Briefmarken. Abnehmer finden sich in den anderen Pazifikstaaten, Europa und Asien.


== Infrastruktur ==

Die Norfolkinsel hat keine Wasserstraßen, Häfen und kein Schienennetz. Schiffspiere existieren in Kingston und in der Cascade Bay, jedoch können dort keine großen Schiffe entladen werden, da die Piere hierfür nicht ausgelegt sind. Die Entladung erfolgt durch kleinere Hilfsschiffe, die bis zu fünf Tonnen pro Fahrt laden können. Häufige schnelle Wetterwechsel können dazu führen, dass sich die Entladung über mehrere Tage hinzieht.
Zudem gibt es einen Flughafen, den Norfolk Island International Airport. Die neuseeländische Fluggesellschaft Air New Zealand bietet regelmäßig Flüge nach Brisbane und Sydney an.Das Straßennetz umfasst etwa 80 Kilometer, wovon 53 Kilometer asphaltiert und 27 Kilometer unbefestigt sind. Laut Gesetz haben Kühe Vorrang vor sonstigen Verkehrsmitteln auf der Insel.Für die Versorgung der Insel mit elektrischer Energie ist das regierungseigene Unternehmen „Norfolk Island Electricity“ zuständig. Es betreibt in Burnt Pine ein Kraftwerk mit sechs dieselgetriebenen Generatoren mit einer Leistung von je 1 MW. Da der maximale Leistungsbedarf nur bei etwa 1,7 MW liegt, arbeiten höchstens je zwei der Generatoren gleichzeitig. Außerdem gibt es private Solaranlagen mit einer installierten Leistung von 1,4 MW, die größtenteils in das Stromnetz integriert sind. Weil es keine Speichermöglichkeit gibt, muss der tagsüber produzierte Überschuss an Solarstrom vernichtet werden. Das Stromnetz besteht aus 44 km Hochspannungsleitungen, die mit 6600 Volt betrieben werden, und ebenfalls 44 km Niederspannungsleitungen.Es gibt auf der Insel ein Krankenhaus, das jedoch nur kleinere Operationen ausführen darf. In schwierigeren Fällen müssen Patienten auf das australische Festland geflogen werden, wobei die Regierung die Kosten übernimmt. In Notfällen erfolgen medizinische Evakuierungen durch die Royal Australian Air Force.


== Kultur ==
Landesspezifische Feiertage sind der Gründungstag 6. März (im Jahre 1788 kamen an diesem Tag die ersten Sträflinge auf der Insel an) und der Nationalfeiertag „Bounty Day“ (Ankunft der ehemaligen Bewohner der Pitcairninsel 1856) am 8. Juni. Daneben besitzen zahlreiche australische und britische Feiertage Gültigkeit.
Eine der Hauptattraktionen der Insel ist das Fletcher’s Mutiny Cyclorama, ein realistisches Rundgemälde, welches die Geschichte der Bounty-Meuterei und der Inselbewohner darstellt. Ein Teil der Norfolkinsel und der vorgelagerten Phillip-Insel sind als Nationalpark unter Schutz gestellt (vgl. Norfolk Island National Park). Seit 2010 ist das ursprüngliche Sträflingslager, das Kingston and Arthurs Vale Historic Area, aufgrund seiner historischen Bedeutung in die Liste des UNESCO-Welterbes eingetragen.


== Fotos ==

		
		
		
		
		
		


== Literatur ==
Peter Clarke: Hell and paradise – the Norfolk-Bounty-Pitcairn saga. Viking Press, Ringwood (Victoria) 1986, ISBN 0-670-81521-7 (englisch). 
Merval Hoare: Norfolk Island – an outline of its history 1774–1977. 2. Auflage. University of Queensland Press, Brisbane 1978, ISBN 0-7022-1941-X (englisch). 
Dieter Kreutzkamp: Straßen in die Einsamkeit. Australien. Outback, Queensland und Norfolk Island. Mit Geländewagen, Camper, Kajak, Windjammer, Fahrrad und Kamel durch den fünften Kontinent. 2., überarbeitete und aktualisierte Auflage. Frederking und Thaler, München 1999, ISBN 3-89405-322-4. 
Colleen McCullough: Insel der Verlorenen – Australien-Saga. 1. Auflage. Blanvalet, München 2005, ISBN 3-442-36190-7. 
Peter Mühlhäusler, Joshua Nash (Hrsg.): Norfolk Island – History, People, Environment, Language. Battlebridge Publications, London 2012, ISBN 978-1-903292-25-9 (englisch). 


== Weblinks ==

Norfolk Island Regional Council. Offizielle Website der Inselverwaltung (englisch).
Norfolk Island – The Website. Website der Norfolk Island’s Society of Pitcairn Descendants (englisch, mit virtueller Tour).


== Einzelnachweise ==