MP3, Eigenschreibweise mp3 (Bezeichnung nach der Dateinamenserweiterung; eigentlich MPEG-1 Audio Layer III oder MPEG-2 Audio Layer III) ist ein Verfahren zur verlustbehafteten Kompression digital gespeicherter Audiodaten. MP3 bedient sich dabei der Psychoakustik mit dem Ziel, nur für den Menschen wahrnehmbare Signalanteile zu speichern. Dadurch wird, bei nicht (oder kaum) verringert wahrgenommener Audioqualität, eine starke Reduktion der Datenmenge möglich.
Bei einer Beispiel-Datenrate von 192 kbit/s, die bereits eine hohe Qualität ermöglicht, beträgt die Kompressionsrate einer MP3-Audiodatei etwa 85 % gegenüber einer unkomprimierten Audio-CD. MP3 ist das dominierende Verfahren zur Speicherung und Übertragung von Musik auf Computern, Smartphones, im Internet und auf tragbaren Musikabspielgeräten (MP3-Player), obwohl es mittlerweile eine Anzahl von technisch weiterentwickelten Optionen gibt. Das Verfahren wurde unter der Leitung von Karlheinz Brandenburg und Hans-Georg Musmann im Wesentlichen in Deutschland entwickelt.
Im Mai 2017 stellten die Entwickler die Lizenzierungen für das Format ein, nachdem die letzten Patente in den USA ausgelaufen waren (in Europa war MP3 bereits seit 2012 patentfrei). Es handelt sich somit nun um einen frei verfügbaren Standard.


== Geschichte ==

Entwickelt wurde das Format MP3 ab 1982 unter der Leitung von Hans-Georg Musmann von einer Gruppe um Karlheinz Brandenburg am Fraunhofer-Institut für Integrierte Schaltungen (IIS) in Erlangen sowie an der Friedrich-Alexander-Universität Erlangen-Nürnberg in Zusammenarbeit mit AT&T Bell Labs und Thomson. Ab 1989 wurde die Entwicklung innerhalb der ISO/IEC JTC1 SC29 WG11 (MPEG) fortgeführt. 1992 wurde es als Teil des MPEG-1-Standards festgeschrieben. Die Geschichte der Standardisierung und die Würdigung der Beiträge der Forscher ist in Genesis of the MP3 Audio Coding Standard by Hans Georg Musmann in IEEE Transactions on Consumer Electronics, Vol. 52, Nr. 3, S. 1043–1049, August 2006 dargestellt. Das italienische Forschungszentrum CSELT (Chef von Media: Leonardo Chiariglione) war das Organ, das die Standardisierung erlaubt hat. Die Dateinamenserweiterung .mp3 (als Abkürzung für ISO MPEG Audio Layer 3) wurde am 14. Juli 1995 nach einer institutsinternen Umfrage festgelegt; vorher wurde intern die Dateinamenserweiterung .bit verwendet. Brandenburg wurde für die Entwicklung dieses Datenformates mehrfach ausgezeichnet.
Bereits Mitte der 1990er-Jahre waren Abspielgeräte und Software für PCs im Umlauf, die es ermöglichten, komprimierte MP3-Dateien zu speichern und abzuspielen. Auch der Austausch solcher Dateien über das Internet vereinfachte sich: Selbst bei einfacher ISDN-Geschwindigkeit benötigte man für die Übertragung lediglich das Zwei- bis Dreifache der Abspielzeit; mit DSL-Leitungen lag die Übertragung sogar weit unterhalb der Spieldauer. Das führte bald zu einem regen Tausch der Audiodateien (Filesharing) ohne Beachtung des Urheberrechts der jeweiligen Künstler oder Komponisten. Versuche der Musikindustrie, dagegen vorzugehen, sind bis heute von nur mäßigem Erfolg geprägt, zumal sich auch die Tauschsysteme immer weiter entwickeln und nach dem Peer-to-Peer-Prinzip ohne zentrale, kontrollierbare Instanzen auskommen. Ende der 1990er entstanden bereits große Ansammlungen von Musikdateien im Internet, wie zum Beispiel bei MP3.com oder Napster, was die Anzahl der Nutzer erheblich steigen ließ. Ab 1998 erschienen im Handel die ersten tragbaren MP3-Player.


== Patente und Lizenzstreitigkeiten ==
Die Verfahren für die MPEG-Kodierung („MP3“) sind heute patentfrei und können daher frei genutzt werden. Der ursprüngliche, fast fertige Standard MPEG-1 (Teile 1, 2 und 3) wurde am 6. Dezember 1991 als ISO CD 11172 veröffentlicht. In den meisten Ländern können Patente nicht mehr angemeldet werden, wenn der „Stand der Technik“ bereits veröffentlicht wurde. Patente verlieren 20 Jahre nach der Erstanmeldung ihre Gültigkeit; in manchen Ländern kann diese Frist noch um bis zu 12 Monate verlängert werden, je nach Anmeldedatum. Im Ergebnis haben die zur Umsetzung der MP3-Technik benötigten Patente in den meisten Ländern im Dezember 2012 ihre Gültigkeit verloren, 21 Jahre nach der Veröffentlichung des Standards ISO CD 11172.
Eine Ausnahme stellten die Vereinigten Staaten dar, wo vor dem 8. Juni 1995 angemeldete Patente bereits nach 17 Jahren ihre Gültigkeit verloren. Es war jedoch möglich, durch die Verlängerung der Anmeldefrist das Datum der Erteilung eines Patents deutlich hinauszuzögern. Die verschiedenen mit MP3 zusammenhängenden Patente verloren zwischen 2007 und 2017 in den USA ihre Gültigkeit. Die MP3-Technologie war in den USA allerspätestens am 16. April 2017 patentfrei, als das von der Fraunhofer-Gesellschaft gehaltene (und via Technicolor verwaltete) US-Patent Nr. 6009399 erlosch.
Konsequenterweise stellte die Fraunhofer-Gesellschaft am 23. April 2017 ihr Lizenzprogramm ein. Die von Sisvel, einem großen MP3-Patentpool, verwalteten und beanspruchten US-Patente waren bis April 2017 ebenfalls erloschen (die letzten drei nach 2015 noch gültigen Patente waren: US-Patent Nr. 5878080, im Februar 2017 erloschen, US-Patent Nr. 5850456, im Februar 2017 erloschen, und US-Patent Nr. 5960037, am 9. April 2017 erloschen).Im Mai 2017 kündigte die Linux-Distribution Fedora an, MP3-Decoder und -Encoder offiziell in die Distribution aufzunehmen, da die entsprechenden Patente erloschen seien.Die Fraunhofer-Gesellschaft und andere Unternehmen besaßen bis 2017 Softwarepatente auf Teilverfahren, die für MPEG-Kodierung eingesetzt werden. Ein alles umfassendes MP3-Patent gab es nicht. Die Fraunhofer-Gesellschaft hatte den größten Teil zur Entwicklung des MP3-Standards beigetragen und sich einige Verfahren zur MP3-Kodierung patentieren lassen. In einem Zusammenschluss mit Thomson besaßen beide Unternehmen 18 MP3-bezogene Patente. Von September 1998, nachdem sich der MP3-Standard sechs Jahre lang etablieren konnte, bis April 2017 verlangte FhG/Thomson Lizenzgebühren für die Herstellung von Hard- und Software, die das MP3-Format verwendeten.
Bei der Entwicklung des Formats sollte ursprünglich auf Patente der Bell Laboratories zurückgegriffen worden sein. Diese Rechte lagen damals bei Alcatel-Lucent, welche die Bell Labs übernommen hatten. Das Unternehmen hatte um die Jahrtausendwende Patentklagen gegen Microsoft, Dell und Gateway eingereicht. Im Verfahren gegen Microsoft wurden Lucent im Februar 2007 erstinstanzlich 1,52 Milliarden US-Dollar zugesprochen. Dieses Urteil wurde allerdings im August 2007 vom Bundesbezirksgericht in San Diego aufgehoben. Das Unternehmen Sisvel erhob im Auftrag von Philips ebenfalls Ansprüche aus Patentverletzung.


== Verfahren ==

Wie die meisten verlustbehafteten Kompressionsformate für Musik nutzt das MP3-Verfahren psychoakustische Effekte der menschlichen Wahrnehmung von Tönen und Geräuschen aus. Zum Beispiel kann der Mensch zwei Töne erst ab einem gewissen Mindestunterschied der Tonhöhe voneinander unterscheiden, vor und nach sehr lauten Geräuschen kann er für kurze Zeit leisere Geräusche schlechter oder gar nicht wahrnehmen. Man braucht also nicht das Ursprungssignal exakt abzuspeichern, sondern es genügen die Signalanteile, die das menschliche Gehör auch wahrnehmen kann. Die Aufgabe des Kodierers ist es, das originale Tonsignal nach festgelegten, an der Psychoakustik orientierten Regeln so aufzubereiten, dass es weniger Speicherplatz benötigt, aber für das menschliche Gehör noch genauso klingt wie das Original. Bei subjektiver völliger Übereinstimmung von Original und MP3-Variante spricht man von Transparenz. Prinzipiell jedoch ist aufgrund der verlustbehafteten Kompression das ursprünglichen Signal aus dem MP3-Signal nicht exakt rekonstruierbar. Es gibt auch verlustlose Verfahren zur Audiodatenkompression wie FLAC, diese erreichen jedoch wesentlich geringere Kompressionsraten und sind – besonders im Bereich der Abspielhardware – noch weniger verbreitet.
Beim Abspielen des so erzeugten MP3-Signals erzeugt der Dekoder aus den reduzierten Daten ein für die überwiegende Anzahl von Hörern original klingendes analoges Tonsignal, das aber nicht mit dem Ursprungssignal identisch ist, da bei der Umwandlung in das MP3-Format Informationen entfernt wurden. Wenn man den zeitlichen Signalverlauf des MP3-Tonsignals mit dem Original vergleichen würde, etwa auf dem Schirm eines Oszilloskops, wären daher deutliche Unterschiede zu erkennen. Wegen der oben erwähnten Psychoakustik der menschlichen Wahrnehmung hört sich das MP3-Signal für einen Zuhörer dennoch – unter der Voraussetzung eines ausgereiften Kodierers und einer ausreichend hohen Datenrate (Bitrate) bei der Kodierung – genau wie das Original an.
Während die Dekodierung stets einem festgelegten Algorithmus folgt, kann die Kodierung nach verschiedenen Algorithmen erfolgen (z. B. Fraunhofer-Encoder, LAME-Encoder) und liefert dementsprechend unterschiedliche akustische Ergebnisse. Die Frage, ob dabei von manchen oder auch vielen Zuhörern wahrnehmbare Qualitätsverluste auftreten, hängt unter anderem von der Qualität des Kodierers, von der Komplexität des Signals, von der Datenrate, von der verwendeten Audiotechnik (Verstärker, Lautsprecher) und schließlich auch vom Gehör des Hörers ab. Das MP3-Format erlaubt, neben festen Datenraten von 8 kbit/s bis zu 320 kbit/s, im freeformat-Modus auch beliebige freie Datenraten bis zu 640 kbit/s (Freeform-MP3). Allerdings sind nur wenige MP3-Player-Decoder für höhere Bitraten als die aus dem ISO-Standard (derzeit bis 320 kbit/s) ausgelegt.
Die Qualitätseindrücke sind recht subjektiv und von Mensch zu Mensch sowie von Gehör zu Gehör unterschiedlich. Die meisten Menschen können ab einer höheren Bitrate und bei Nutzung eines ausgereiften Enkodierers auch bei konzentriertem Zuhören das kodierte Material nicht mehr vom Ausgangsmaterial unterscheiden. Dennoch konnten in einem Hörtest des c’t-Magazins gewisse Musikstücke, selbst bei 256 kBit/s, von CD-Qualität unterschieden werden. Allerdings wurde der Test im Jahr 2000 durchgeführt – seitdem haben sich die MP3-Encoder deutlich verbessert. Bei Menschen mit „abnormem“ Gehör (z. B. mit Hörschäden durch Knalltrauma) greifen die eingesetzten Mechanismen aber mitunter nicht wie vorgesehen, so dass ihnen Unterschiede zwischen kodiertem und Ausgangsmaterial eher auffallen (z. B. weil laute Töne, die das geschädigte Gehör schlecht hört, andere Töne nicht mehr gut verdecken können). Die Testperson, die im eben genannten Test auch bei hohen Datenraten am besten Unterschiede ausmachen konnte, hat ein geschädigtes Gehör.Neben der Kodierung mit konstanter Datenrate (= schwankende Qualität, einhergehend mit der im zeitlichen Verlauf wechselnden Komplexität des Tonsignals) ist auch eine Kodierung mit konstanter Qualität (und damit schwankender Datenrate) möglich. Man vermeidet dadurch (weitgehend) Qualitätseinbrüche an schwierig zu kodierenden Musikstellen, spart jedoch andererseits bei ruhigen oder gar völlig stillen Passagen des Audiostromes an der Datenrate und somit an der endgültigen Dateigröße. Die Qualitätsstufe wird vorgegeben, und man erhält auf diese Art die dafür minimal notwendige Dateigröße.


=== Datenkompression ===

Ein erster Schritt der Datenkompression beruht zum Beispiel auf der Kanalkopplung des Stereosignals durch Differenzbildung, da die Daten des rechten und des linken Kanals in hohem Maße korrelieren, sich also sehr ähnlich sind. Das ist ein verlustloses Verfahren, die Ausgangssignale können vollständig reproduziert werden (Mid/Side-Stereo).
Entsprechend der menschlichen Hörkurve werden Signalanteile in weniger präzise wahrnehmbaren Frequenzbereichen mit weniger Präzision dargestellt, indem das fouriertransformierte Datenmaterial entsprechend quantisiert wird.
Sogenannte Maskierungseffekte werden ausgenutzt, um für den Höreindruck minderwichtige Signalanteile mit verringerter Präzision zu speichern. Das können etwa schwache Frequenzanteile in der Nähe von starken Obertönen sein. Ein starker Ton bei 4 kHz kann aber auch Frequenzen bis zu 11 kHz maskieren. Die größte Ersparnis bei der MP3-Enkodierung liegt daher darin, dass die Töne nur gerade so genau (mit so vielen Bits) abgespeichert werden, dass das dadurch entstehende Quantisierungsrauschen noch maskiert wird und somit nicht hörbar ist.
Die Daten, die in sogenannten Frames vorliegen, werden schließlich Huffman-kodiert.Bei starker Kompression werden öfter auch durchaus hörbare Signalanteile von der Kompression erfasst, sie sind dann als Kompressionsartefakte hörbar.
Ein Mangel im Entwurf ist, dass das Verfahren blockweise angewandt wird und so am Ende einer Datei Lücken entstehen können. Das stört beispielsweise bei Hörbüchern oder Live-Aufnahmen, in denen ein zusammenhängender Vortrag in einzelne Tracks zerlegt wurde. Hier fallen die letzten Blöcke als störende Pausen (wahrnehmbar etwa als Knackser oder ein kurzes Drop-out) auf. Abhilfe schafft die Verwendung des LAME-Encoders, der exakte Längeninformationen hinzufügt, in Kombination mit einem Abspielprogramm, das mit diesen umgehen kann, etwa foobar2000 oder Winamp. Einige Abspielprogramme wie Windows Media Player unterstützen dieses Gapless Playback genannte Verfahren jedoch nicht. Apple iTunes unterstützt es ab Version 7.


=== Kompression im Detail ===
Die Kompression besteht aus folgenden Schritten:

Subband-Transformation des Signals
MDCT-Transformation des Signals, danach(!) wird das Signal in Blöcke eingeteilt.
Bei Stereosignalen: Matrizierung: Entscheidung für jeden Block, ob Signal als Links-Rechts- oder als Mitte-Seite-Signal kodiert wird
Quantisierung des Signals
Huffman-Kodierung mit festen CodebüchernDie Schritte 4 und 5 sorgen für die Datenreduktion, wobei die Quantisierung der verlustbehaftete Vorgang ist.
Hinweis: Im weiteren Text beziehen sich angegebenen Spektralbreiten und Zeiten auf ein Audiosignal mit 48 kHz Abtastfrequenz.


==== Subband-Transformation des Signals ====
Bei der Subband-Transformation wird das Signal mithilfe einer polyphasen Filterbank in 32 gleich breite Frequenzbänder zerlegt (wie auch bei MPEG Layer 1, MPEG Layer 2 und dts). Die Filterbank arbeitet auf einem FIFO-Puffer mit einer Größe von 512 Samples, dem in einem Schritt immer 32 neue Samples zugeführt werden. Dadurch überlappen sich immer 16 Filterfenster auf dem Audiosignal.
Die Entscheidung, gleich breite Frequenzbänder zu verwenden, vereinfacht zwar die Filter, spiegelt jedoch nicht das menschliche Hörvermögen wider, dessen Empfindlichkeit nicht-linear von der Frequenz abhängt.
Da in der Praxis keine idealen Filter existieren, überlappen sich die Frequenzbereiche, sodass eine einzige Frequenz nach der Filterung auch in zwei benachbarten Subbändern auftreten kann.
Subbandfilterung ist belastet durch das Patent US 6,199,039.


==== MDCT-Transformation des Signals ====
Die Signale der Subbänder werden nun durch die modifizierte diskrete Kosinustransformation (MDCT) in den Frequenzbereich überführt. Dadurch werden die Frequenzbänder weiter spektral aufgelöst. Die MDCT kann die Bänder entweder in kurzen Blöcken (12 Samples ergibt 6 Frequenzbänder) oder langen Blöcken (36 Samples, 18 Frequenzbänder) transformieren. Alternativ können auch die beiden niedrigsten Frequenzbänder mit langen Blöcken und die restlichen mit kurzen Blöcken transformiert werden. Lange Blöcke besitzen eine bessere Frequenzauflösung und sind geeigneter, wenn sich das Audiosignal im entsprechenden Rahmen nicht plötzlich ändert (Stationarität).Am Ausgang der MDCT wird das Signal in Blöcke eingeteilt. Aus 576 Eingangswerten (wenn man die Fensterbreite der Filter berücksichtigt, sind es eigentlich insgesamt 1663 Eingangswerte) werden durch zwei hintereinandergeschaltete Transformationen entweder

576 Spektralkoeffizienten (lange Blöcke),
3 × 192 Spektralkoeffizienten (kurze Blöcke) oder
36 + 3 × 180 Spektralkoeffizienten (hybrider Block, kaum genutzt)


==== Matrizierung ====
Für 2-Kanal-Stereosignale kann nun entschieden werden, ob das Signal entweder als Mono (Single-Channel), Stereo, Joint-Stereo oder Dual-Channel kodiert werden soll. Im Gegensatz zu AAC oder Ogg Vorbis ist diese Entscheidung global für alle Frequenzen zu treffen.
Das Stereo-Verfahren (nicht Joint-Stereo) ist (wie auch Dual-Channel) durch den Umstand verlustbehaftet, dass auch bei 320 kbit/s nur 160 kbit/s pro Kanal zur Verfügung stehen, jedoch werden je nach Komplexität wahlweise einem der beiden Kanäle unterschiedliche Bitraten zugewiesen. Dual-Channel speichert zwei unabhängige Monospuren (z. B. zweisprachige Textspuren) mit der gleichen Bitratencodierung; jedoch nicht zwingend jeder Decoder gibt beide Spuren auch gleichzeitig wieder.
Beim Joint-Stereo gibt es zwei Kodierverfahren: Intensitäts- und Mid/Side-Stereo, die auch kombiniert angewandt werden; beide Verfahren bilden aus der Summe beider Kanäle einen Mittenkanal (L+R) und aus der Lautstärkedifferenz der beiden Kanäle den Seitenkanal (L−R). Beim Intensitäts-Stereo wird im Gegensatz zum Mid-/Side-Stereoverfahren die Phase (Laufzeitunterschied) des Signals vernachlässigt. Das Joint-Stereoverfahren eliminiert die häufige Redundanz in den Stereokanälen, um die Signale mit höherer Bitrate als beim Stereo-Verfahren kodieren zu können; sind die Kanalsignale aber sehr unähnlich, fällt das Joint-Stereoverfahren auf die normale Stereo-Kodierung zurück.
Da das Tonsignal zunächst in Frequenzbänder ausdifferenziert wird, muss die Stereoinformation, sofern diese überhaupt vom Gehör verwertbar ist, auch ebenso differenziert kodiert werden. Hier kann, z. B. bei Tiefen oder Frequenzen ab 2 kHz, Informationsgehalt eingespart werden, dadurch, dass die betreffenden nicht lokalisierbaren Signale nicht mehr kanalgetreu, sondern mit benachbarten Frequenzbändern subsumiert kodiert (Intensitäts-Stereo), oder aber in die Stereomitte gelegt werden.
Durch andauernde Weiterentwicklung der Codecs wird das Joint-Stereo-Verfahren neuerdings bei musiküblichen stark ähnlichen Stereokanälen durch die bessere Kompressionsrate, höhere Bitratencodierung und das verlustfreie (außer tieffrequenziell) Stereoabbild als beste Lösung angesehen.


==== Quantisierung ====
Die Quantisierung ist der wesentliche Schritt, bei dem Verluste bei der Kodierung auftreten. Er ist hauptsächlich für das Schrumpfen der Datenmenge verantwortlich.
Benachbarte Frequenzbänder werden zu Gruppen von 4 bis 18 Bins zusammengefasst. Diese bekommen einen gemeinsamen Skalenfaktor s=2N/4, mit dem sie quantisiert werden. Der Skalenfaktor bestimmt die Genauigkeit der Kodierung dieses Frequenzbandes. Kleinere Skalenfaktoren ergeben eine genauere Kodierung, größere eine ungenauere (oder gar keine Werte ungleich 0 mehr).
Aus x0, x1, …, x17 werden die Werte N und Q0, Q1, …, Q17 mit der Beziehung xi ~ Qi4/3 2N/4.
Die nichtlineare Kodierung Q4/3 (für negative Werte: −(−Q)4/3) ist erstmals in der MP3-Codierung eingeführt worden. MPEG Layer 1 und 2 nutzen eine lineare Kodierung.
Dieser Schritt ist im Wesentlichen für Qualität wie auch die Datenrate des entstehenden MP3-Datenstroms verantwortlich. Ihm zur Seite steht ein psychoakustisches Modell, das die Vorgänge im durchschnittlichen menschlichen Gehör nachzubilden versucht und die Steuerung der Skalenfaktoren steuert.


==== Huffman-Kodierung ====
Die Skalenfaktoren N und die quantisierten Amplituden Q der einzelnen Frequenzen werden mittels fester Code-Tabellen Huffman-kodiert.
Die finale MP3-Datei besteht aus einer Aneinanderreihung von Frames, die mit einer Startmarke (Sync) beginnen und die einen oder zwei auf die oben beschriebene Art erzeugte Blöcke enthalten.


=== Dekompression ===
Bei der Dekompression werden die Schritte der Kompression in umgekehrter Reihenfolge ausgeführt. Nach der Huffman-Dekodierung werden die Daten mittels inverser Quantisierung für die inverse modifizierte Cosinustransformation (IMCT) aufbereitet. Diese leitet ihre Daten weiter zu einer inversen Filterbank, die nun die ursprünglichen Samples berechnet (verlustbehaftet durch die Quantisierung im Kodierprozess).


=== Weiterentwicklung ===
MP3 ist ein besonders im Internet sehr verbreitetes Format. In der Industrie wird es hauptsächlich für PC-Spiele verwendet. Es handelt sich um ein ehemalig proprietäres Format, das in den ISO-Standard aufgenommen wurde.
In der Industrie wurde zu dieser Zeit schon an dem MDCT-basierten AAC gearbeitet, das bei vergleichbarem Aufwand bessere Ergebnisse liefert.
Daneben (in Richtung einer hochqualitativen Kodierung) gibt es auch Weiterentwicklungen, um bei sehr niedrigen Datenraten (weniger als 96 kbit/s) noch eine akzeptable Klangqualität zu erreichen. Vertreter dieser Kategorie sind mp3PRO sowie MPEG-4 AAC HE beziehungsweise AAC+. Transparenz ist mit diesen Verfahren allerdings nur durch High Definition-(HD-)AAC erreichbar (AAC LC + SLS).

Eine Erweiterung um Multikanalfähigkeiten bietet das MP3-Surround-Format des Fraunhofer-Instituts für Integrierte Schaltungen IIS. MP3-Surround erlaubt die Wiedergabe von 5.1-Ton bei Bitraten, die mit denen von Stereoton vergleichbar sind und ist zudem vollständig rückwärtskompatibel. So können herkömmliche MP3-Decoder das Signal in Stereo decodieren, MP3-Surround-Decoder aber vollen 5.1-Surround-Klang erzeugen.
Dafür wird das Multikanal-Material zu einem Stereosignal gemischt und von einem regulären MP3-Encoder kodiert. Gleichzeitig werden die Raumklanginformationen aus dem Original als Surround-Erweiterungsdaten in das „Ancillary-Data“-Datenfeld des MP3-Bitstroms eingefügt. Die MP3-Daten können dann von jedem MP3-Decoder als Stereosignal wiedergegeben werden. Der MP3-Surround-Decoder nutzt die eingefügten Erweiterungsdaten und gibt das volle Multikanal-Audiosignal wieder.
Weitere Entwicklungen betreffen Verfahren zum Urheberschutz, das unter Umständen in zukünftigen Versionen implementiert werden könnte.


== Anwendung ==
Audio-Rohmaterial benötigt viel Speicherplatz (1 Minute Stereo in CD-Qualität etwa 10 MB) und zum Transfer (beispielsweise über das Internet) hohe Datenübertragungsraten oder viel Zeit. Die verlustlose Kompression reduziert die zu übertragenden Datenmengen nicht so stark wie verlustbehaftete Verfahren, die für die meisten Fälle (Ausnahmen sind beispielsweise Studioanwendungen oder Archivierung) noch annehmbare Qualität liefern. So erlangte das MP3-Format für Audio-Daten schnell den Status, den die JPEG-Komprimierung für Bilddaten hat.
MP3 wurde in der breiten Öffentlichkeit vor allem durch Musiktauschbörsen bekannt. In der Warez-Szene wird bei vielen DVD-Rips als Tonspur das Audioformat MP3 verwendet. Mit CD-Ripper-Programmen ist es möglich, die Musik von Audio-CDs zu extrahieren und in MP3-Dateien auszugeben. Auch gibt es viele Programme, die es ermöglichen, MP3 durch eine Konvertierung in ein anderes Format zu verwandeln, aber auch umgekehrt (Beispiel: Audiospur eines YouTube-Videos (FLV) wird in eine MP3-Datei umgewandelt). Ein weiterer Anwendungsschwerpunkt waren MP3-Player, mit denen man auch unterwegs Musik hören kann. Heutzutage unterstützen auch die meisten Smartphones MP3-Dateien.
Im WWW finden sich zahlreiche Anwendungen für MP3-Technik, von selbstkomponierter Musik über (selbst)gesprochene Hörbücher, Hörspiele, Vogelstimmen und andere Klänge bis hin zum Podcasting. Musiker können nun auch ohne einen Vertrieb ihre Musik weltweit verbreiten und Klangaufnahmen ohne großen Aufwand (abgesehen von den GEMA-Gebühren, auch auf eigene Kompositionen, die bei der GEMA angemeldet sind) auf einer Website zur Verfügung stellen. Nutzer können über Suchmaschinen alle erdenklichen (nicht kommerziellen) Klänge und Musikrichtungen finden.
Auch bei multimedialer Software, vor allem bei PC-Spielen, werden die oft zahlreichen Audiodateien im MP3-Format hinterlegt. Zudem findet MP3 bei zahlreichen – meist kleineren – Online-Musikläden Anwendung.


== Tagging ==
Im Gegensatz zu moderneren Codecs boten MP3-Dateien ursprünglich keine Möglichkeit, Metadaten (beispielsweise Titel, Interpret, Album, Jahr, Genre) zu dem enthaltenen Musikstück zu speichern.
Unabhängig vom Entwickler des Formats wurde dafür eine Lösung gefunden, die von fast allen Soft- und Hardwareplayern unterstützt wird: Die ID3-Tags werden einfach an den Anfang oder das Ende der MP3-Datei gehängt. In der ersten Version (ID3v1) werden sie am Ende angehängt und sind auf 30 Zeichen pro Eintrag und wenige Standard-Einträge beschränkt. Die wesentlich flexiblere Version 2 (ID3v2) wird allerdings nicht von allen MP3-Playern (insbesondere Hardware-Playern) unterstützt, da hier die Tags am Anfang der MP3-Datei eingefügt werden. Auch innerhalb von ID3v2 gibt es noch beträchtliche Unterschiede. Am weitesten verbreitet sind ID3v2.3 und ID3v2.4, wobei erst ID3v2.4 offiziell die Verwendung von UTF-8-kodierten Zeichen zulässt (vorher waren nur ISO-8859-1 und UTF-16 zulässig). Viele Hardwareplayer zeigen aber UTF-8-Tags nur als wirre Zeichen an. Da ID3v2-Tags am Anfang der Datei stehen, lassen sich diese Daten beispielsweise auch bei der Übertragung über HTTP lesen, ohne erst die ganze Datei zu lesen oder mehrere Teile der Datei anzufordern. Um zu vermeiden, dass bei Änderungen die ganze Datei neu geschrieben werden muss, verwendet man üblicherweise Padding, das heißt, man reserviert im Vorfeld Platz für diese Änderungen.
Die Metadaten aus dem ID3-Tag können beispielsweise genutzt werden, um Informationen zum gerade abgespielten Stück anzuzeigen, die Titel in Wiedergabelisten (Playlists) zu sortieren oder Archive zu organisieren.


== Spezifikation ==


=== Frame-Header ===

Tabelle Bitraten (Angaben in kbps)

Tabelle Samplingfrequenz (Angaben in Hz)

Tabelle Mode-Extension


=== Frame-Daten ===
Auf den Frame-Header folgen die Frame-Daten (gegebenenfalls zunächst CRC), in denen die kodierten Audio-Daten enthalten sind. Ein Frame hat eine Spieldauer von 1152 Samples bei einer Samplerate von 32.000 bis 48.000 Samples je Sekunde; bei kleineren Sampleraten (16.000 bis 24.000 Samples je Sekunde) sind es nur 576. Bei 48.000 Samples je Sekunde entsprechen dem 24 ms. Die Datenmenge eines Frames kann gemäß den angegebenen Eigenschaften im Header errechnet werden. Die Größe eines Frames in Byte lässt sich dann mit der folgenden Formel berechnen, wobei die Division als Ganzzahldivision durchzuführen ist:

Framegröße = (144 · Bitrate) : Samplerate + Padding [bytes]Wenn bei komplexen Musikstücken die Menge an Daten nicht in einem Frame gespeichert werden können, bietet MP3 ein sogenanntes bit reservoir. Dieser Speicherbereich ist als zusätzlicher Platz für die Datei bestimmt und erweitert die Daten im entsprechenden Frame. Hierzu kodiert der Encoder vorangegangene Musikpassagen mit geringerer Datenrate und füllt somit frühere Frames nicht vollständig aus, das bit reservoir entsteht. Dieser geschaffene freie Speicherplatz kann nun für die höhere Datenmenge komplexerer Musikpassagen genutzt werden. Die maximale Größe dieses Datenreservoirs beträgt 511 Byte, wobei ausschließlich vorangegangene Frames aufgefüllt werden dürfen.


== Verbreitete Implementierungen ==
Zum Codieren von MP3-Dateien stehen der lizenzpflichtige Encoder der Fraunhofer-Gesellschaft und der Encoder des Open-Source-Projektes LAME zur Verfügung. Daneben existieren der Referenzencoder der ISO dist10 und weitere Projekte wie beispielsweise Xing, blade und Gogo.
Als Decoder gibt es mpg123, MAD, libavcodec und weitere.


== Alternative Formate ==

Neben MP3 existieren zahlreiche weitere Audioformate. Das Format Vorbis ist quelloffen und wurde von den Entwicklern als patentfrei bezeichnet. (Vorbis erschien 15 Jahre vor Ablauf der MP3-Patente.) Vorbis hat sich bei technischen Analysen und in Blindtests gegenüber MP3 vor allem in niedrigen und mittleren Bitratenbereichen als überlegen erwiesen. Der qualitative Vorteil von Vorbis ist im hohen Bitraten-Bereich (um 256 kbit/s) nur noch geringfügig wahrnehmbar. Außerdem bietet Ogg-Vorbis Mehrkanal-Unterstützung, und Ogg kann als Containerformat auch Video- und Textdaten aufnehmen. Letzteres wird aber nur von sehr wenigen MP3-Playern und Radios unterstützt.
RealAudio von RealMedia wurde vorwiegend für Audio-Datenströme (Streaming Audio) eingesetzt.
Das freie, auf MP2-Algorithmen basierende Musepack (früher MPEGPlus) wurde entwickelt, um bei Bitraten über 160 kbit/s noch bessere Qualität als das MP3-Format zu ermöglichen. Es konnte sich aber nicht breit durchsetzen, da es eher auf die Anwendung durch Enthusiasten im High-End-Bereich abzielt und im kommerziellen Bereich kaum unterstützt wird. Dateien im Musepack-Format erkennt man an der Erweiterung mpc oder mp+.Advanced Audio Coding (AAC) ist ein im Rahmen von MPEG-2 und MPEG-4 standardisiertes Verfahren, das von mehreren großen Unternehmen entwickelt wurde. Apple und RealMedia setzen dieses Format für ihre Online-Musikläden ein, und die Nero AG stellt einen Encoder für das Format bereit. Mit faac ist auch ein freier Encoder erhältlich. AAC ist bei niedrigen Bitraten bis etwa 160 kbit/s MP3 in der Klangqualität überlegen – je niedriger die Bitrate, desto deutlicher –, erlaubt Mehrkanal-Ton und wird von der Industrie (zum Beispiel bei Mobiltelefonen und MP3-Playern) breit unterstützt.
Windows Media Audio (WMA) ist ein von Microsoft entwickeltes Audioformat und wird häufig für DRM-geschützte Downloads verwendet. Obwohl es auf vielen üblichen Plattformen abgespielt werden kann, hat es sich nicht gegen das MP3-Format behaupten können.


== Wissenswertes ==
Das Team um Brandenburg machte die ersten Praxistests mit der A-cappella-Version des Liedes Tom’s Diner von Suzanne Vega. Bei seiner Suche nach geeignetem Testmaterial las Brandenburg in einer Hi-Fi-Zeitschrift, dass deren Tester das Lied zum Beurteilen von Lautsprechern nutzten, und empfand das Stück als geeignete Herausforderung für eine Audiodatenkompression.


== Literatur ==
Franz Miller: Die mp3-Story: Eine deutsche Erfolgsgeschichte, Carl Hanser Verlag GmbH & Co. KG, ISBN 978-3-446-44471-3.
Roland Enders: Das Homerecording Handbuch. Der Weg zu optimalen Aufnahmen. 3., überarbeitete Auflage, überarbeitet von Andreas Schulz. Carstensen, München 2003, ISBN 3-910098-25-8.
Thomas Görne: Tontechnik. Fachbuchverlag Leipzig im Carl Hanser Verlag, München u. a. 2006, ISBN 3-446-40198-9.
Hubert Henle: Das Tonstudio Handbuch. Praktische Einführung in die professionelle Aufnahmetechnik. 5., komplett überarbeitete Auflage. Carstensen, München 2001, ISBN 3-910098-19-3.
Michael Dickreiter, Volker Dittel, Wolfgang Hoeg, Martin Wöhr (Hrsg.): Handbuch der Tonstudiotechnik. Walter de Gruyter, Berlin/Boston 2014, ISBN 978-3-11-028978-7 oder e-ISBN 978-3-11-031650-6.


== Weblinks ==

MP3 auf dem Prüfstand
Audio-Interview mit Dr. Karlheinz Brandenburg über die Entstehungsgeschichte und Hintergründe von MP3 vom 16. März 2004 (48 Minuten)
Kurzinterview mit Hans-Georg Musmann zur Entwicklung von MP3 im Rahmen des Experimentes der Woche
Die Geschichte von MP3 – Erinnerungen von Ernst F. Schröder, einem der MP3-Entwickler
Podcast "Grünes Glück": Suzanne Vega über ihre Rolle als "Mutter von mp3"
Die MP3-Revolution begann in Deutschland
Karl-Gerhard Haas: 25 Jahre MP3: Die Klang-Quetsche hat Geburtstag. In: Heise online. 14. Juli 2020.
Imre Grimm: Musik liegt in der Luft: Wie acht deutsche Ingenieure vor 25 Jahren mit der mp3 Geschichte schrieben. Sonntag - Wochenendmagazin der Leipziger Volkszeitung, Online-Portal, 25. Juli 2020. Abgerufen am 2. August 2020. 


== Einzelnachweise ==