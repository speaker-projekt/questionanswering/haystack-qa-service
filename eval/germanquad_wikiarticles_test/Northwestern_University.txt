Die Northwestern University (NU oder einfach Northwestern) ist eine Privatuniversität im Staat Illinois in den USA mit einem Doppelcampus in Evanston und Chicago. Die Elitehochschule ist Mitglied der Association of American Universities, einem seit 1900 bestehenden Verbund nordamerikanischer Forschungsuniversitäten und gehört zu den führenden Universitäten weltweit.Der Hauptcampus in Evanston misst 970.000 m² und befindet sich am Ufer des Lake Michigan. Der Campus in Chicago misst 101.000 m² und befindet sich in der Nähe der Magnificent Mile. Die Chicago El, die städtische U-Bahn, verbindet den Campus in Evanston mit Chicago. 
An der Northwestern sind etwa 15.000 Studenten eingeschrieben und sie beschäftigt fast 7.100 Angestellte. Die Universitätsstiftung verfügte 2019 über Geldmittel von etwa 11,1 Mrd. US-Dollar. Das akademische Schuljahr ist in vier Quartale unterteilt, wobei im Summer Quarter nur ein reduzierter Betrieb stattfindet.
Die Studenten führen eine eigene Zeitung, The Daily Northwestern, einen Nachrichtensender, Northwestern News Network, und einen Radiosender, WNUR. 


== Geschichte ==

Die Universität wurde 1851 von Methodisten aus Chicago (darunter John Evans, nach dem Evanston benannt wurde) gegründet und entstand mit Billigung des Gründungsakts durch die Legislative offiziell am 28. Januar 1851. Im Jahr 1855 eröffnete sie mit zwei Professoren und 10 Studenten. Der Name der Universität, Northwestern, kam vom Wunsch seiner Gründer, Einwohnern der Staaten zu dienen, die den Bereich des ehemaligen Nordwestterritoriums umfassen: Ohio, Indiana, Illinois, Michigan, Wisconsin und Minnesota. Der ursprüngliche Campus 1855 bestand nur aus einem Gebäude, dem provisorischen Old College. Das erste dauerhafte Gebäude, die University Hall, wurde 1869 gebaut.
1873 wurde das Evanston College for Ladies in die Universität integriert und Frances Willard, die später durch ihr Engagement für das Frauenwahlrecht berühmt wurde, erhielt am College die Position des Dekans für Frauen. Northwestern wurde 1869 nach Insistieren des Dekans Erastus Haven koedukativ, die erste Frau machte ihren Abschluss 1874.In den 1920er Jahren wurde die Universität mit einem Campus in Chicago erweitert. In den 1930er Jahren fusionierte die Northwestern beinahe mit ihrer akademischen Rivalin, der University of Chicago. Das Vorhaben wurde schließlich vom Überwachungsausschuss der Northwestern abgelehnt.1948 gründete der Anthropologe Melville J. Herskovits an der Northwestern ein Programm für Afrikanistik. Dies war das erste seiner Art an einer akademischen Einrichtung in den USA.
Im Juni 1978 explodierte die erste Briefbombe des Unabombers an der Northwestern University. Im darauf folgende Jahr war die Universität ebenfalls Ziel seines zweiten Attentats. Es blieb bei geringfügigen Verletzungen.
Am 11. Januar 2003 kündigte George Ryan, der Gouverneur von Illinois, in einer Rede an der Northwestern die Begnadigung aller zum Tode verurteilten Gefängnisinsassen von Illinois an. Ryan sagte: „Es ist passend, dass wir heute hier, in der Northwestern University, mit jenen Studenten, Lehrern, Anwälten und Ermittlern zusammentreffen, die zuerst die traurigen Bedingungen des Todesstrafensystems ins Licht der Öffentlichkeit gebracht haben.“ In den späten 1990er haben Journalismus-Studierende der Northwestern Informationen aufgedeckt, die zur Begnadigung von Anthony Porter geführt haben.
Der Ausdruck auf dem Logo der Northwestern University ist Quaecumque sunt vera (lat. „Was auch immer wahr ist“, Philipper 4,8).
Die Gründungs-Charta der Universität gesteht ihr dauerhafte Befreiung von Vermögenssteuern zu. Aus diesem Grund hat die Regierung von Evanston oftmals ein schwieriges Verhältnis zur Northwestern. Spannungen traten bei Baubestimmungen und in der Politik auf. Vor kurzem haben Parteien der Stadtregierung versucht, den Campus der Northwestern in unterschiedliche Bezirke einzuteilen, um die Stimmkraft der Studenten zu verringern.


== Studienangebot ==


=== Undergraduate Programme ===
Der akademische Grad Bachelor wird angeboten durch:

Weinberg College of Arts and Sciences (WCAS) (gegründet 1851)
School of Communication (1878)
Bienen School of Music (1895)
McCormick School of Engineering and Applied Science (1909)
Medill School of Journalism (1921)
School of Education and Social Policy (1926)


=== Graduate Programme ===
Die akademischen Grade Master und Ph.D. werden angeboten durch:

Feinberg School of Medicine (1859)
School of Law (1859)
School of Communication (1878)
School of Music (1895)
Kellogg School of Management (1908)
McCormick School of Engineering and Applied Science (1909) mit dem Farley Center for Entrepreneurship and Innovation (FCEI)
The Graduate School (1910)
Medill School of Journalism (1921)
School of Education and Social Policy (1926)
School of Continuing Studies (1933)


== Sport ==

Die Northwestern ist Gründungsmitglied und einzige Privatuniversität der Big Ten Conference im Hochschulsport. Sie wird durch acht Männerteams und elf Frauenteams in der NCAA Division I vertreten. Ihre Sportteams nennen sich The Wildcats. Die offiziellen Schulfarben sind purpur und weiß und bis 1924 waren die Teams auch bekannt als The Purple. Als 1924 ein Reporter der Chicago Tribune die Football-Mannschaft als „a wall of purple wildcats“ beschrieb, wurde dieser Name sehr populär und kurz darauf als offizielle Bezeichnung übernommen. Das Maskottchen nennt sich Willie the Wildcat.
Die Footballmannschaft existiert seit 1876 und feierte ihren bisher größten Erfolg 1949 mit dem Gewinn des Rose Bowl. Seither fiel die Mannschaft durch einige Negativrekorde auf, wie die Serie von 34 Niederlagen zwischen 1979 und 1982. 1995 gewann das Team überraschenderweise die Big Ten Conference und qualifizierte sich wieder einmal für den Rose Bowl, den sie aber gegen die University of Southern California verloren. Die Mannschaft hält heute noch den Allzeit-Rekord für Niederlagen und erhaltene Punkte in der Division I-A. Im Oktober 2006 verloren sie gegen die Michigan State University ein Spiel mit 41-38, nachdem sie im dritten Viertel, 10 Minuten vor Ende, noch mit 38-3 geführt haben.
Das momentan erfolgreichste Sportteam an der NU ist dasjenige im Frauen-Lacrosse. Es gewann 2007 die nationale Meisterschaft zum dritten Mal in Folge.
Außerdem absolvierte der Profigolfer Luke Donald sein Studium an der Northwestern und gewann 1999 den Herren-Einzeltitel der NCAA-Golfmeisterschaft.


== Rankings ==
2020 belegte die Northwestern University gemäß dem Ranking des U.S. News & World Report den 9. Rang der US-amerikanischen Universitäten, vor den Universitäten Brown, Dartmouth und Cornell. Im Wall Street Journal-Ranking 2021 gehört die Northwestern zu den Top 10 Universitäten der USA. Northwestern belegt gemäß Times-Higher-Education-World-Ranking 2020 den 22. Rang der besten Universitäten weltweit und gehört somit zu den Top 30 Universitäten der Welt. Die Northwestern ist weltweit insbesondere für ihre 1908 gegründete Kellogg School of Management (kurz: Kellogg) bekannt. Deren MBA-Programm belegte im weltweiten Ranking der Financial Times 2020 Platz 11 sowie Platz 4 im nationalen Ranking des U.S. News & World Report 2021.


== Persönlichkeiten ==


=== Präsidenten ===
Clark T. Hinman, DD (1853–54)


=== Dozenten ===
Stephen H. Davis, Engineering Sciences, Angewandte Mathematik
Ken Forbus, Computer Science and Artificial Intelligence
Lois Wilfred Griffiths, Mathematikerin
Michael Honig, Elektrotechnik, Computer Science
Donald Norman, Computer Science, Cognitive Science
Monica Olvera de la Cruz, Professorin für Materialwissenschaft und Werkstofftechnik, Chemie, Chemieingenieurwesen und Biotechnik sowie  Physik und Astronomie
Sarah Hackett Stevenson, Ärztin, Professorin für Geburtshilfe


=== Berühmte Absolventen ===
Hans Peter Anvin, schwedischer Programmierer der Open-Source-Szene
Ali Babacan, ehemaliger türkischer Außenminister
Warren Beatty, Schauspieler, Drehbuchautor und Regisseur
Saul Bellow, erhielt 1976 den Nobelpreis für Literatur
Rod Blagojevich, Gouverneur des Bundesstaates Illinois
Zach Braff, Schauspieler (Scrubs)
Willa Brown, Pilotin, Lobbyistin, Lehrerin und Bürgerrechtlerin
William Jennings Bryan, ehemaliger US-Außenminister
Elisabeth Bumiller, White House-Korrespondentin der New York Times
Richard Scott Cohen, Musikdirektor und Professor
Stephen Colbert, Komiker (The Colbert Report)
Cindy Crawford, Model (nie diplomiert)
Paddy Driscoll, American-Football-Spieler und -Trainer, Baseballspieler
Anne Dudek, Schauspielerin (Dr. House)
Rahm Emanuel, Bürgermeister von Chicago und ehemaliger Stabschef des Weißen Hauses unter US-Präsident Barack Obama
Robert Francis Furchgott, erhielt 1998 den Nobelpreis für Medizin
Annie Marie Watkins Garraway, Mathematikerin und Philanthropin
Richard Gephardt, ehemaliger US-Kongressabgeordneter
Gene Gossage, American-Football-Spieler
Otto Graham, American-Football-Spieler und -Trainer, Basketballspieler
Mamie Gummer, Schauspielerin (Emily Owens) und Tochter von Meryl Streep
Anna Gunn, Schauspielerin (Breaking Bad)
Marg Helgenberger, Schauspielerin (CSI: Den Tätern auf der Spur)
Charlton Heston, Schauspieler und ehemaliger Präsident der National Rifle Association
Chris Hinton, American-Football-Spieler
Luke Johnsos, American-Football-Spieler und -Trainer
Robert Knepper, Schauspieler (Prison Break)
John Logan, Schriftsteller und Drehbuchautor
Julia Louis-Dreyfus, Schauspielerin (Veep – Die Vizepräsidentin)
Meghan Markle, Schauspielerin (Suits) und Herzogin von Sussex
Garry Marshall, Regisseur, Schauspieler und Filmproduzent
George McGovern, ehemaliger Senator
Seth Meyers, Schauspieler und Drehbuchautor (Saturday Night Live)
Robert Reed, Schauspieler (The Brady Bunch)
Dave Rempis, Jazzmusiker
Karen Russell, Schriftstellerin
David Schwimmer, Schauspieler (Friends)
John Siegal, American-Football-Spieler und Zahnarzt
Jerry Springer, Moderator von The Jerry Springer Show
John Paul Stevens, Richter am Obersten Gerichtshof der Vereinigten Staaten
Margret Suckale, deutsche Managerin
Harold Washington, erster afroamerikanischer Bürgermeister von Chicago
Kimberly Williams-Paisley, Schauspielerin (Immer wieder Jim)
Veronica Roth, Schriftstellerin (Die Bestimmung)


== Weblinks ==

Offizielle Webpräsenz
The Daily Northwestern
Northwestern Wildcats Hochschulsport


== Einzelnachweise ==