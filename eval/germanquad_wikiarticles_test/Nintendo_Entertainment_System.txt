Das Nintendo Entertainment System (kurz meist NES genannt) ist eine stationäre 8-Bit-Spielkonsole des japanischen Spielkonsolenherstellers Nintendo. Die Konsole erschien in Nordamerika, Brasilien, Europa, Asien und Australien. Die japanische Version wurde als Famicom (kurz für Family Computer) vermarktet. Das NES war mit über 60 Millionen verkauften Einheiten lange Zeit die meistverkaufte Konsole überhaupt. Nach dem Zusammenbruch der Videospielebranche im Jahre 1983, dem sogenannten „Video Game Crash“, belebte Nintendo den Heimkonsolenmarkt mit ihrer Konsole neu. Der Controller führte das Steuerkreuz (kurz: D-Pad für Direction-Pad) ein und definierte die grundsätzliche Tastenanordnung der Gamepads. Bekannte Spieleserien wie Super Mario Bros., The Legend of Zelda, Metroid oder Castlevania gaben auf dem NES ihr Heimkonsolen-Debüt.


== Geschichte ==

Nach einigen erfolgreichen Spielhallenautomaten in den frühen 1980er-Jahren plante Nintendo eine Videospielkonsole für den Heimgebrauch mit austauschbaren Spielmodulen. Masayuki Uemura entwarf schließlich das Famicom, das in Japan am 15. Juli 1983 veröffentlicht wurde und zu einem Preis von 14.800 Yen zu erstehen war. Bis Ende 1984 mauserte sich das Famicom zur bestverkauften Videospielkonsole in Japan. Zu dieser Zeit plante Nintendo bereits einen Verkaufsstart in den USA, wo der Videospielmarkt aufgrund des Branchenzusammenbruchs von 1983 praktisch nicht existent war. Zunächst gab es die Überlegung, die Konsole in den USA unter Ataris Namen zu veröffentlichen, doch deren Interesse richtete sich mehr auf den Heimcomputermarkt. Deswegen nahm Nintendo den Vertrieb selbst in die Hand.
Erstmals wurde die Konsole als Nintendo Advanced Video System vorgestellt, mit Zubehör wie einer Tastatur, Kassetten-Rekorder, Infrarot-Joystick und einem speziellen BASIC Programming Language-Modul. Das kompliziert und überladen wirkende AVS wurde nochmals überarbeitet und in abgespeckter Version auf der CES (Consumer Electronics Show) im Juni 1985 als Nintendo Entertainment System vorgestellt. Nach einem erfolgreichen Start in New York mit ca. 50.000 verkauften Einheiten veröffentlichte man am 18. Oktober 1985 das NES schließlich landesweit; zunächst nur in ausgesuchten Verkaufsstellen, später gab es die Konsole überall zu kaufen. Die Konsole wurde in zwei verschiedenen Paketen angeboten: Das NES Deluxe Set beinhaltete neben der Konsole den R.O.B. (siehe unten), einen Zapper (siehe unten), zwei Controller und die Spiele Duck Hunt und Gyromite und war für 249 US-Dollar zu erstehen; das NES Action-Set mit einem Super-Mario-Bros.-/Duck-Hunt-Multimodul gab es für 199 USD. Für den Rest der Dekade war das NES die unangefochtene Nr. 1 des amerikanischen und japanischen Marktes, und die Spiele brachen einen Verkaufsrekord nach dem anderen.
Als ausschlaggebend für den Erfolg des NES in den USA gilt vor allem Nintendos Geschäftspolitik, die in vielfacher Hinsicht auch auf den Zusammenbruch der Spieleindustrie reagierte und eine Wiederholung der Fehler, die zu diesem Zusammenbruch geführt hatten, vermied: Entscheidend war vor allem, dass Nintendo durch Lizenzierung selber die weitgehende Kontrolle über die Veröffentlichung von Spielen für sein System behielt. Dies geschah einerseits mittels des hierfür eingeführten Nintendo Seal of Quality, das die Abnahme eines Spiels durch Nintendo selber attestierte, andererseits über die exklusive Produktion kompatibler NES-Spielmodule durch Nintendo und deren kontingentierte Zuteilung an Dritthersteller. Diese Geschäftspraktik wurde von den Entwicklern gelegentlich als restriktiv betrachtet und führte mehrfach auch zu juristischen Auseinandersetzungen, beispielsweise mit Tengen, brachte umgekehrt aber auch Vorteile, da die Spiele selber teils vom Nintendo-Marketing profitieren konnten.
In Europa und Australien – den Regionen mit PAL-Fernsehnorm – wurden die Märkte aufgeteilt in PAL A und PAL B. Mattel (PAL A) war für den Vertrieb des NES und dessen Spielen in Großbritannien, Australien und Italien zuständig; der PAL-B-Markt, welcher aus dem restlichen Europa bestand, wurde unter verschiedenen Distributoren aufgeteilt (beispielsweise die Firma Bienengräber in Deutschland, welche schon die Game-&-Watch-Spiele erfolgreich vertrieben hatte). Mit der Eröffnung von Nintendo of Europe übernahm Nintendo selbst den Vertrieb.
Als in den 1990ern die ersten 16-Bit-Konsolen (wie beispielsweise das Sega Mega Drive) Einzug hielten, ging die Dominanz des NES langsam zu Ende. Auch Nintendo veröffentlichte zu Beginn der 1990er Jahre die Nachfolgekonsole Super Nintendo Entertainment System, kurz SNES. Nintendo unterstützte das NES aber weiterhin und veröffentlichte noch eine neue, kleinere Version der Konsole in den USA, Japan und Australien. 1995 nahm Nintendo of America das NES nach stets sinkenden Verkaufszahlen schließlich offiziell aus dem Programm. In Europa erschienen die letzten Spiele 1996. In Japan wurde die Produktion des AV Famicom noch bis 2003 fortgesetzt.
Nach dem offiziellen Ende der Konsole im Westen entstand eine rege Sammlergemeinde rund um das 8-Bit-Gerät. Zusammen mit der wachsenden Emulation-Szene erlebte die Konsole in den späten 1990er Jahren sozusagen ihren zweiten Frühling. Auch heute erfreuen sich das NES und dessen Spiele nicht nur bei Videospielsammlern großer Beliebtheit. Nicht selten wird für begehrte Module und Zubehör weit mehr bezahlt, als sie bei ihrem Erscheinen gekostet haben.


== Spiele ==

Das erfolgreichste Spiel auf dem NES ist Super Mario Bros., welches im Westen zusammen mit der Konsole verkauft wurde. Mit über 40 Millionen verkauften Einheiten war es das mit Abstand meistverkaufte Videospiel überhaupt, bis es 2008 vom 82,69 Millionen Mal verkauften Wii Sports übertroffen wurde.

Einige weitere bekannte Spiele auf dem NES:


== Zubehör für das NES ==


=== Famicom Disk System ===

In Japan erschien für das Famicom ein Diskettenlaufwerk, das über den Modulschacht an die Konsole angeschlossen wird. Zahlreiche bekannte Spiele wurden auf Diskette umgesetzt oder lieferten dort gar ihr Debüt ab.


=== Power Pad ===

Das Power Pad unterstützt vor allem Sportspiele. Die verschiedenen auf der Matte befindlichen Knöpfe werden mit den Füßen betätigt, wodurch die Spielfigur am TV beispielsweise zu rennen beginnt oder zum Sprung ansetzt. In Europa erschienen mit Athletic World und Stadium Events lediglich zwei Spiele, die von der Fitnessmatte Gebrauch machen, in den USA war die Auswahl hingegen etwas größer. Als Konzept ist sie ein direkter Vorgänger des Wii-Balance-Boards, das erfolgreich für die Wii vermarktet wurde (Wii Fit).


=== NES Advantage ===

Der NES Advantage ist ein einem Spielhallenautomaten nachempfundenes Joyboard inklusive Knöpfen und Joystick. Vor allem für klassische Arcade-Spiele, wie beispielsweise Shoot ’em ups, soll sich das Eingabegerät bestens eignen. Neben dem Joystick bietet das Gerät auch regulierbare Autofeuer-Knöpfe und eine Zeitlupenfunktion, die allerdings nur einen wiederholten Aufruf des Start-Knopfes, der bei den meisten Arcade-Portierungen für das NES die Pause-Funktion aktiviert, darstellt, so als würde man immer wieder das Spiel für eine kurze Zeit unterbrechen, sodass der Spielvorgang langsamer voranschreitet. Der NES Advantage hat zwei Anschlusskabel, um es zwei Spielern abwechselnd zu ermöglichen damit zu spielen. Auf der Oberseite befindet sich der Schiebeschalter zum Wechseln des aktiven Spielers.
In der US-amerikanischen Science-Fiction-Komödie Ghostbusters II wird ein NES Advantage als Requisite zur Steuerung der Freiheitsstatue benutzt.


=== NES Four Score ===

Der NES Four Score ist ein Mehrspieleradapter und lässt bis zu vier Spieler gleichzeitig mit- oder gegeneinander spielen. Spiele, die den Four Score unterstützen, sind zum Beispiel Nintendo World Cup, Gauntlet 2 oder auch Super Spike's V'Ball. Neben den vier Controlleranschlüssen gibt es auch eine manuelle Autofeuerfunktion für den A- und B-Knopf.


=== NES Max ===
Der offizielle NES Max-Controller bietet, wie viele spätere Controller, links und rechts einen Griff, um das Halten des Steuerelements komfortabler zu gestalten. Das auffälligste Merkmal des Eingabegerätes ist jedoch der sogenannte Cycloid, eine Scheibe, welche das Standard-Steuerkreuz ersetzt und sich umherschieben lässt und auf Druck die Richtung angibt. Zusätzlich wurden noch Autofeuer-Knöpfe von A und B angebracht.


=== Power Glove ===

Der Power Glove ist ein Datenhandschuh für den rechten Arm. Mittels am TV anzubringender Sensoren kann man durch Bewegungen des Armes das Spiel steuern. Für zahlreiche Titel war bereits eine Steuerung programmiert, für viele muss man diese jedoch noch selbst kalibrieren. Das Spiel Super Gloveball war speziell auf den Power Glove ausgerichtet, ansonsten hielt sich der praktische Nutzen sehr in Grenzen. In der Computerspielkultur gelangte er zu zweifelhafter Berühmtheit, die sich auf eines der frühesten Beispiele für einen „Hype-Backlash“ zurückführen lässt: Nintendo ließ den Powerglove (neben vielen anderen allzu offensichtlichen Produktplatzierungen) in dem Hollywood-Blockbuster Joy Stick Heroes (englisch The Wizard) vor dem eigentlichen Verkaufsstart bewerben. Die Dissonanz zwischen Werbeversprechen und Realität und die unfreiwillige Komik des Filmdialogs, die diese a posteriori reflektiert (Zitat: „I love the Powerglove. It's so bad!“; wobei bad gleichzeitig als „voll krass“ oder „grottenschlecht“ verstanden werden kann), hat dem Gerät vorläufig einen festen Platz im Humor der Nerd-Kultur gesichert.
Im Kurzfilm Kung Fury, dessen Handlung im Jahr 1985 angesiedelt ist, benutzt der Hacker "Hackerman" den Power Glove, um eine Zeitreise durchzuführen.


=== R.O.B. ===

Der Robotic Operation Buddy, kurz R.O.B., der ursprünglich OTTO heißen sollte, gehörte in den USA beim Deluxe Set zur Grundausstattung. Für den Betrieb werden vier AA-Batterien sowie eine D-Batterie für den Gyro-Motor benötigt. Mittels Sensoren in den Augen kann der kleine Roboter Informationen und Befehle vom TV-Bildschirm empfangen, welche via Tastendruck ausgelöst werden. So hebt R.O.B. beispielsweise Klötzchen hoch und öffnet damit im Spiel gleichzeitig eine versperrte Tür. Er wurde auf der Summer Consumer Electronics Show 1985 in Chicago vorgestellt und ließ die Verkaufszahlen des NES in den USA in die Höhe steigen. Insgesamt machten lediglich zwei Spiele von dem skurrilen Zubehör Gebrauch: Gyromite (im Lieferumfang des Deluxe Set enthalten) und Stack Up. Obwohl R.O.B. nur sehr langsam arbeitete und vergleichsweise klobig zu bedienen war, gilt er unter Retro-Spielern heute als ein seltenes Sammlerstück und hat einen gewissen Kultstatus erlangt. Er ist in Mario Kart DS, Super Smash Bros. Brawl, Super Smash Bros. for Nintendo 3DS / for Wii U und Super Smash Bros. Ultimate als Figur spielbar.


=== Zapper ===

Der Zapper ist eine Lightgun für das NES, die bereits beim Start mitausgeliefert wurde (je nach Set). Mit der in grau oder orange erhältlichen Lichtpistole wird auf den TV-Bildschirm gezielt und beispielsweise auf Tontauben geschossen. Die bekanntesten Spiele für dieses Eingabegerät sind Duck Hunt, Gumshoe und Wild Gunman; weitere Titel sind beispielsweise Barker Bill's Trick Shooting, Shooting Range oder Operation Wolf. Prinzipbedingt funktioniert diese Art der Lightguns nur mit klassischen Röhrenfernsehern. Für Nintendos neuere Heimkonsole Wii gibt es eine neuere Version des Zappers.


=== Family Computer Network System ===
In Japan erschien mit dem Family Computer Network System ein Modem, mit dem es möglich war, verschiedene Online-Dienste an der NES zu nutzen. Ein Server stellte Services wie Wetterberichte, Aktienkurse oder auch Cheats für die Nutzer bereit. Spiele, die über das Internet gespielt werden konnten waren zwar geplant, wurden jedoch nie realisiert.


=== Weiteres Zubehör ===

Vor allem in Japan gab es noch etliches weiteres, teils skurriles Hardware-Zubehör. Dazu zählt ein Mikrofon für Karaoke-Spiele, ein Modem plus zugehöriger Tastatur, Plattenspieler, Kassettenlaufwerk, eine 3D-Brille mit dem Namen Famicom 3D System und sogar eine Strickmaschine. Zudem erschienen in der ganzen Welt verschiedenste Controller-Varianten und Import-Adapter, die das Abspielen von Spielen anderer Regionen auf der eigenen Konsole ermöglichen. Ferner gab es Cheating-Module wie Action Replay oder Game Genie, welche durch Eingabe von Codes beispielsweise unbegrenzte Leben im Spiel verschaffen können.


== Technische Daten ==

CPU: Ricoh 2A03 (ein 6502-Derivat) getaktet mit 1,77 MHz (PAL) / 1,79 MHz (NTSC)
Grafikchip: PPU Ricoh-Chip (NTSC: RP2C02, PAL: RP2C07) getaktet auf 5,37 MHz bzw. 5,32 MHz
Soundchip: Die Geräuschfunktionen sind in die CPU integriert (5 Soundkanäle). Zwei Rechteck-Wellenformen mit variabler Pulsbreite, eine Dreieck-Wellenform, ein Rauschgenerator und ein digitaler 6-Bit-Delta-PCM-Soundkanal.
Speicher:
RAM: 2 kB (manche Spiele wie z. B. Super Mario Bros. 3 (8 KB) enthalten zusätzlichen RAM)
Video-RAM: 2 kB
Modulgröße: 192 Kbit – 6 Mbit
Auflösung:
256×224 Pixel (NTSC)
256×240 Pixel (PAL)
Farben und Sprites:
16 aus 52 Farben gleichzeitig darstellbar (Sprites)
25 aus 52 Farben gleichzeitig darstellbar (Hintergründe, 16 Hauptfarben, 8 Schattierungen + Grundfarbe dunkelgrau / schwarz)
Spritegröße 8×8 Pixel oder 8×16 Pixel
64 Sprites gleichzeitig darstellbar (max. 8 pro Bildschirmzeile)
Abmessungen: 255 mm × 85 mm × 202 mm
Gewicht: 1247,9 g


== Redesign des NES ==

In Nordamerika und Australien erschien gegen Ende der 8-Bit-Ära eine kompaktere neuere Version des NES. In Nordamerika wurde die Konsole 1993 zu einem Preis von 49,99 USD veröffentlicht. Von Nintendo selber wurde das Gerät nach wie vor als Nintendo Entertainment System verkauft, unter den Käufern etablierten sich Top Loading NES und NES 2 als gängige Bezeichnungen.


=== Änderungen gegenüber dem Standard-NES ===
Die Hardware ist fast identisch, es fehlen jedoch die Composite-Ausgänge.
Neuer Schacht für die Module, sogenannter Toploader, da das Modul nicht mehr waagerecht hineingeschoben und dann nach unten gedrückt werden muss, sondern senkrecht von oben eingesteckt wird.
Das Äußere wurde völlig überarbeitet und ähnelt sehr der amerikanischen Version des SNES
Das Gamepad des redesignten NES ist ebenfalls dem des SNES sehr ähnlich und ist aufgrund des Designs als DogBone-(Hundeknochen-)Controller bekannt.
Der Toploader ist universell benutzbar, d. h., er kann US-Spiele sowie europäische Spiele abspielen.
Ein Kritikpunkt an dem Gerät sind vertikale Streifen, die während des Spielens auf dem Bildschirm zu sehen sind. Nintendo brachte als Reaktion eine verbesserte Version der Konsole (in Japan als Famicom "AV") auf den Markt.


=== Nintendo Classic Mini: Nintendo Entertainment System ===

Am 14. Juli 2016 kündigte Nintendo mit dem Nintendo Classic Mini: Nintendo Entertainment System eine neue, kleinere Version des NES an, die mit 30 vorinstallierten Spielen ausgestattet und am 11. November 2016 erschienen ist. Sämtliche Spiele sind vorinstalliert, das Gerät bietet keine Datenschnittstelle zur Erweiterung.Folgende 30 Titel sind auf dem Gerät gespeichert:


== NES-kompatible Nachbauten („Famiclones“) ==
Das NES zählt zu den am meisten unlizenziert nachgebauten Spielkonsolen aller Zeiten. Als Sammelbegriff für diese Konsolen hat sich das Wort „Famiclone“ durchgesetzt, welches aus „Famicom“ (der japanischen NES-Version) und „Clone“ (Nachbau, Klon) abgeleitet ist. Selbst im Jahre 2010, also 25 Jahre nach Erscheinen der Konsole in den USA und trotz dessen, dass wesentlich leistungsfähigere 3D-Spielkonsolen in ihrer vierten Generation verfügbar sind, werden NES-kompatible Spielkonsolen weiterhin hergestellt. Da einige NES-Patente in den USA bereits 2005 abgelaufen sind, können dort nun legal sogenannte „Famiclones“ vertrieben werden, insofern diese keine urheberrechtlich geschützten Spiele enthalten, da deren Urheberrechtsschutz noch immer andauert. Zu den offiziell als NES-kompatibel vertriebenen Konsolen zählen unter anderem „Generation NEX“, „Yobo FC“ und „Famicom Portable“.
Allerdings sind diese sogenannten Famiclones nicht ohne weiteres mit US- und Pal-NES-Spielen kompatibel, da die japanischen Famicom-Spiele weniger Anschlusspins und damit kürzere PCBs haben. Auch sind Famiclones fast nie mit offiziellem Zubehör kompatibel, da andere Anschlüsse für die Controller benutzt werden (meist 9Pin-DSub-Buchsen).


== Nintendo Entertainment System – Nintendo Switch Online ==
Im Zuge des kostenpflichtigen Online-Zugangs Nintendo Switch Online für die Nintendo Switch am 19. September 2018 veröffentlichte Nintendo das virtuelle Nintendo Entertainment System. Kunden des Online-Services können aus einer monatlich wachsenden Bibliothek an NES-Spielen auf der Nintendo Switch wählen. Passend dazu veröffentlicht Nintendo Controller im NES-Design, die kabellos mit der Switch-Konsole verbunden werden.


== Siehe auch ==
Dendy (Spielkonsole), in Russland populärer Famiclone
PlayChoice-10 (Arcade-Automat auf NES-Basis)
NES Sound Format
NES Remix, Spielemix für Wii U und Nintendo 3DS


== Literatur ==
Rainer Babiel, Ulrich Krockenberger: NES Game Power. Sybex, Köln 1991, ISBN 3-88745-899-0. 
Pat Contri: Ultimate Nintendo: Guide to the NES Library (1985-1995). The Punk Effect, USA 2016, ISBN 978-0-9973283-0-1. 
Nick von Esmarch, Garitt Rocha: Playing With Power: Nintendo NES Classics. Prima Games, USA 2016, ISBN 978-0-7440-1767-0. 
Timothy L. Hearn: The Nintendo Entertainment System: A Comprehensive Look at the History, Technology, and Success of the NES. CreateSpace Independent Publishing Platform, USA 2015, ISBN 978-1-5177-7921-4. 
Jeremy Parish: Good Nintentions: A 30th Anniversary of Nintendo Entertainment System. CreateSpace Independent Publishing Platform, USA 2015, ISBN 978-1-5121-0974-0. 
Jeremy Parish: Good Nintentions 1985: The Definitive Unauthorized Guide to Nintendo's NES Launch. CreateSpace Independent Publishing Platform, USA 2016, ISBN 978-1-5370-8079-6. 
David Sheff: Game Over. How Nintendo Zapped an American Industry, Captured Your Dollars, and Enslaved Your Children. Random House, New York 1993, ISBN 0-679-40469-4. 


== Weblinks ==

Technische Daten des NES. In: Nintendo.de
NES Center: Größte deutsche NES-Website. Bietet Testberichte, Tipps & Tricks, Reportagen u. a. (deutsch)
Bob Rost: NES-Spieleentwicklung (englisch)
Technische Dokumentation zur Funktionsweise der NES-Architektur (englisch)
Offizielle deutschsprachige Website des Four Score


== Einzelnachweise ==