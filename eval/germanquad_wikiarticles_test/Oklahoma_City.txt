Oklahoma City ist die Hauptstadt des US-amerikanischen Bundesstaates Oklahoma und dessen wirtschaftliches und kulturelles Zentrum. Die Stadt ist County Seat des gleichnamigen Oklahoma County. Sie ist eine der größten Städte der Great Plains und liegt am North Canadian River im Zentrum des Bundesstaates Oklahoma. Offiziellen Schätzungen zufolge hatte sie 2016 etwa 640.000 Einwohner, sie ist damit die bevölkerungsreichste Stadt Oklahomas. In der Metropolregion lebten 2016 circa 1,37 Millionen Menschen.
Von 1954 bis 1956 befand sich mit dem KWTV-Sendemast das höchste Bauwerk der Welt in Oklahoma City.
Die Stadt wurde im April 1995 Schauplatz eines Bombenanschlags, bei dem 168 Menschen ihr Leben verloren. Es war der schwerste Terroranschlag in der Geschichte der Vereinigten Staaten vor den Terroranschlägen vom 11. September 2001.


== Geschichte ==
Im Laufe des 19. Jahrhunderts wurden Indianerstämme aus dem ganzen Land von der amerikanischen Zentralregierung gewaltsam in das Indianer-Territorium verdrängt. Das Gebiet der heutigen Oklahoma City galt als nicht zugewiesenes Land (Unassigned Lands), da es keinem Stamm zugeordnet war. Im Februar 1887 wurde in diesem ein Eisenbahndepot mit dem Namen Oklahoma Station eröffnet, bevor die Infrastruktur durch den Bau eines Postamts im Dezember desselben Jahres erweitert wurde.Am 22. April 1889 wurde das heutige Oklahoma City dann im Zuge des Oklahoma Land Run von über 10.000 Angloamerikanern besiedelt. Wenige Tage nach dem Land Run wurde eine provisorische Regierung installiert. Oklahoma City wurde am 15. Juli 1890, etwa zwei Monate nach der Errichtung des Oklahoma-Territoriums, offiziell gegründet. Darauf folgten die ersten Wahlen, aus denen William J. Gault als Sieger hervorging. Am 12. August 1890 wurde er der erste Bürgermeister der Stadt, der dieses Amt nicht provisorisch ausübte.
Zunächst fungierte Oklahoma City als Sitz des County Two und Guthrie als Hauptstadt des Territoriums. Bis zur Mitgliedschaft in den Vereinigten Staaten im Jahr 1907 versuchte Oklahoma City, die bisherige Hauptstadt als wirtschaftliches Zentrum des neuen Staates zu verdrängen. In einer Petition wurde Oklahoma City als neue Hauptstadt vorgeschlagen und aus einer am 11. Juni 1910 abgehaltenen Wahl ging die Stadt siegreich hervor.Im Lauf der ersten zehn Jahre verdoppelte sich die Bevölkerung. Auch danach wuchs die neue Stadt beständig, bis am 4. Dezember 1928 in dem Gebiet Öl entdeckt wurde. Überall wurde das „schwarze Gold“ aus der Erde gepumpt. Sogar auf der Rasenfläche des Capitols wurde Öl gefördert. Der Einfluss der Ölgelder beschleunigte das Wachstum. Oklahoma City wurde eine attraktive viktorianische Stadt mit Anschluss an das Bahnnetz und Industrieansiedlungen. Die Bevölkerung wuchs auf 64.000 Einwohner.
Während viele das große Geld mit Öl machten, hatte die Mehrheit der Amerikaner nicht das Glück, der Great Depression zu entkommen. Bis 1935 entstand Hooverville am Südufer des North Canadian River, benannt nach Herbert C. Hoover, eine Barackensiedlung, gebaut von ländlichen Auswanderern und Arbeitslosen. Der Fluss hatte oft Hochwasser und brachte Krankheiten und Zerstörung für die dort lebenden Menschen. Als Teil des sogenannten New Deal wurden erste Erfahrungen im öffentlichen Wohnungsbau gesammelt und der Wasserspiegel des Flusses durch Maßnahmen des Works Progress Administration und Civilian Conservation Corp gesenkt, was im Übrigen später zum Problem für die Stadt wurde, da der Fluss in manchen Jahren kaum Wasser führte. Ebenfalls 1935 wurde in der Stadt die erste, von Carl Magee erfundene Parkuhr aufgestellt.
In den 1970er Jahren erlebte die Stadt dank des Ölbooms und des Baus einer Autofabrik durch den Konzern General Motors eine Phase des Aufschwungs. In den 1980er Jahren verursachten die Pleite der Penn Square Bank (1982) und der Verfall des Ölpreises Mitte des Jahrzehnts eine schwere Wirtschaftskrise. Viele Häuser im Zentrum der Stadt standen leer; im Zentrum gab es lediglich ein Hotel. Die Arbeitslosigkeit lag bei etwa 10 Prozent. Die Rezession hielt bis in die 1990er Jahre an.Am 14. Dezember 1993 stimmten die Bürger für das Projekt MAPS (Metropolitan Area Projects), das eine temporäre Steuererhöhung vorsah. Mit den zusätzlichen Einnahmen finanzierte die Stadt Investitionen in die Infrastruktur, ohne Schulden aufnehmen zu müssen. So wurden unter anderem eine Bibliothek, ein Kanal und eine Sportarena gebaut. Der Plan, die Stadt auf diese Weise für potenzielle Arbeitnehmer attraktiver zu machen, um in der Konsequenz Unternehmen anzulocken, ging auf. Das Projekt gilt heute als Erfolg und mitverantwortlich für die positive Entwicklung von Wirtschaft und Kultur, die sich im Bau von Hotels und Museen widerspiegelt.
Die meisten Banken der Stadt reagierten auf die Krise der 80er Jahre und hielten sich fortan an eine konservative, weniger risikobehaftete Kreditvergabepolitik. Dieser Umstand erklärt, weshalb die Stadt von der Weltwirtschaftskrise ab 2007 in nur geringem Maße betroffen waren.Am 20. Mai 2013 wurde die Stadt durch den Moore-Tornado heimgesucht. Das Ergebnis waren schwere Schäden an Gebäuden und 24 Tote.


=== Bombenanschlag ===

Am 19. April 1995 wurde das Alfred P. Murrah Federal Building Ziel des sogenannten Oklahoma City Bombing, bei dem 168 Menschen getötet und über 800 verletzt wurden. Am Morgen des 19. April detonierte ein mit ca. drei Tonnen Sprengstoff beladener Van an der 5th Street vor dem Gebäude. Ein Drittel des Gebäudes wurde weggesprengt.
Bei der Suche nach den Tätern kam man nur wenige Stunden nach der Explosion auf die Spur des Fluchtfahrzeuges. Durch eine in den Gebäudetrümmern gefundene Fahrzeugachse mit Seriennummer konnte der Autovermieter ausfindig gemacht werden. Dessen Beschreibung führte zu Timothy McVeigh, der auf dem Phantombild von einem Motelbesitzer erkannt wurde. Bei der Fahndung nach ihm stellte sich heraus, dass er kurz nach dem Anschlag wegen fehlenden Kennzeichens von der Polizei angehalten und aufgrund illegalen Waffenbesitzes verhaftet worden war.
Es wurden noch zwei Mittäter festgenommen, darunter Terry Nichols, ein guter Freund McVeighs, die Mitglieder der Michigan-Miliz waren, einer paramilitärischen Vereinigung.
Als Motive nannten die Täter, dass mit dem Anschlag Vergeltung für die Geschehnisse in Waco und Ruby Ridge, die sich beide einige Jahre zuvor ereignet hatten, geübt werden sollte.
Timothy McVeigh wurde von einem Gericht zum Tode verurteilt und am 11. Juni 2001 in Indiana hingerichtet. Terry Nichols wurde im August 2004 zu einer lebenslangen Haftstrafe verurteilt.
Am 19. April 2000 wurde das Oklahoma City National Memorial eröffnet, das an die Opfer dieses Terroranschlags erinnern soll.


== Demografie ==
Der letzten Volkszählung zufolge hatte die Stadt 2010 etwa 580.000 Einwohner. Diese Zahl ist nach Schätzung der Behörden bis 2014 auf etwa 620.000 angewachsen. Die Bevölkerungsdichte betrug demnach 395 Einwohner je km2 für das Jahr 2013. Das rasche Bevölkerungswachstum wird zum Teil auf das – seit Jahren anhaltende – Wirtschaftswachstum der Region und die damit verbundene Entstehung von Arbeitsplätzen zurückgeführt.Das Medianalter lag 2014 bei 34,4 Jahren.
Die Bevölkerungszusammensetzung ist uneinheitlicher als im gesamten Bundesstaat; die Migrationsrate deutlich höher. 12 % der Stadtbewohner wurden außerhalb der Vereinigten Staaten geboren. So sind die Weißen – bei einem Anteil von 63 % an der Gesamtbevölkerung – die am stärksten vertretene Rasse. Sie machen allerdings einen erheblich kleineren Teil der Einwohner aus als in anderen Regionen Oklahomas. Unter den Weißen sind 43 % hispanischer oder lateinamerikanischer Herkunft. Darunter sind größtenteils Mexikaner. Die zweitgrößte Bevölkerungsgruppe nach den Weißen stellen Schwarze und Afroamerikaner mit 15 %. Asiaten stellen 4 % der Einwohner. Die beiden zuletzt genannten Gruppen sind in der Stadt deutlich überrepräsentiert: Ihr Anteil ist jeweils doppelt so hoch wie im Bundesstaat. Dagegen liegt der Anteil der amerikanischen Ureinwohner mit 3,5 % deutlich unterhalb des Schnitts von knapp 9 %. Dennoch stellt die Stadt nach absoluten Zahlen die viertgrößte indianische Gemeinde der USA. Die relative Mehrheit (13,0 %) der Bewohner Oklahoma Citys hat deutsche Vorfahren. In der Statistik folgen die Menschen mit irischer (10,0 %), englischer (7,4 %) und amerikanischer Abstammung (7,0 %) auf den weiteren Rängen.


=== Kriminalität ===
Die Kriminalitätsrate von Oklahoma City lag 2012 im hohen Bereich. Sie betrug 591,5 Punkte (US-Durchschnitt: 301,1). In den Vororten war sie dagegen deutlich niedriger. In der Liste der gefährlichsten Städte mit mindestens 500.000 Einwohnern belegte Oklahoma City einer Studie zufolge 2014 landesweit den siebten Rang.


== Geographie ==

Oklahoma City liegt im Zentrum Oklahomas, einer Region, die unter den Namen Central Oklahoma und Frontier Country bekannt ist. Die Stadt liegt 157 Kilometer Luftlinie westsüdwestlich von Oklahomas zweiter Metropole Tulsa und 304 Kilometer nördlich von Dallas. Sie erstreckt sich über die Countys Oklahoma, Cleveland, Canadian und Pottawatomie. Die Fläche von 1.608,8 Quadratkilometern macht sie zur achtgrößten Stadt der USA nach Fläche.Oklahoma City liegt am südöstlichen Rand der Great Plains. Unmittelbar westlich der Stadt beginnt die baumarme Hochgrasprärie. 
Durch das Stadtgebiet verläuft der North Canadian River, ein Nebenfluss des Canadian Rivers. Der sieben Meilen lange Abschnitt des Flusses in Stadtnähe wird Oklahoma River genannt und zu Erholungs- und Sportzwecken genutzt.Zusammen mit seinen Vororten bildet die Stadt die Oklahoma City Metropolitan Area, eine Agglomeration mit 1,3 Millionen Einwohnern. Die größten Vororte sind Norman im Süden und Edmond im Norden. Weitere Orte mit mindestens 20.000 Einwohnern sind Choctaw, Del City, Forest Park, Midwest City, Moore, Shawnee, und Yukon. Einzelne Nachbargemeinden sind vollständig vom Kernstadtgebiet umschlossen.


=== Stadtgliederung ===
Es gibt zwei verschiedene Definitionen der Stadtgliederung: Politisch ist Oklahoma City in acht Wahlbezirke eingeteilt (siehe unten), deren Grenzen flexibel sind, da sie vor jeder Wahl der aktuellen Bevölkerungsstruktur angepasst werden. Zum anderen kann die Stadt in sechs Regionen mit festen Grenzen gegliedert werden: den Nordwesten (NW), den Nordosten (NE), das Zentrum (CC), den Südwesten (SW), den Süden (S) und den Südosten (SE). An dieser Definition orientieren sich auch die Straßennamen.


=== Klima ===

Die Stadt befindet sich nach Köppen in der feucht-subtropischen Klimazone (Cfa). Dieses liegt in der Form des Ostseitenklimas vor. Das Klima ist ganzjährig humid mit einem Niederschlagsmaximum im Juni.
Charakteristisch sind lange und heiße Sommer: Die höchste Durchschnittstemperatur liegt bei 28 °C im Juli, die Tagestemperaturen erreichen dann im Mittel 34 °C, können aber auch wesentlich höher ausfallen. So wurde am 11. August 1936 ein Hitzerekord von 45 °C aufgestellt. Die Winter dauern nur wenige Monate an. Obwohl die Temperaturen häufig mild sind, ist das Auftreten von Nachtfrost nicht ungewöhnlich. Tagsüber steigen die Temperaturen aber fast immer über den Gefrierpunkt. Der kälteste Monat ist mit einer Durchschnittstemperatur von 4 °C der Januar. Der Kälterekord stammt vom 12. Februar 1899, als −27 °C gemessen wurden. Die Jahresmitteltemperatur lag in der Periode 1981–2010 bei 16,4 °C.
Da Oklahoma City in der Tornado Alley liegt, besteht zu bestimmten Jahreszeiten erhöhte Gefahr durch Tornados. Besonders groß ist diese in den Monaten April bis Juni, wenn die meisten und heftigsten Stürme auftreten. Seit Beginn der Aufzeichnungen im Jahre 1890 sind aber  – mit Ausnahme von Dezember und Januar – in jedem Monat des Jahres Tornados aufgetreten.


== Politik ==

Oklahoma City ist aktuell eine Hochburg der Republikanischen Partei. Seit 1987 wird die Stadt ununterbrochen von einem Republikaner regiert. Dieser Umstand ist auf die konservative Wählerschaft zurückzuführen: Einer Studie aus dem Jahr 2014 zufolge liegt Oklahoma City in der Liste der konservativsten Städte der USA mit mindestens 250.000 Einwohnern auf dem zweiten Rang.Oklahoma City ist als Hauptstadt des Bundesstaates Oklahoma ihr Regierungssitz und damit Standort des Oklahoma State Capitols. Das Kapitol vereint die beiden Kammern der Legislative des Staates (Repräsentantenhaus und Senat). Die Stadt fungiert auch als Sitz des Oklahoma Countys und beherbergt in dieser Funktion das Oklahoma County Courthouse. Bei den Kongresswahlen gehört Oklahoma City zum Wahlbezirk Nr. 5.


=== Stadtverwaltung ===
Seit dem Jahr 1927 hat die Stadt ein Council Manager Government. Von den Bürgern Oklahoma Citys werden der Bürgermeister (Mayor) und der achtköpfige Stadtrat (City Council) gewählt. Jedes Mitglied des Stadtrats repräsentiert einen der acht Wahlbezirke (Wards). Die Wahlperiode des Bürgermeisters beträgt vier Jahre; die des Stadtrats zwei. Sie ernennen einen hauptberuflichen Stadtgeschäftsführer (City Manager), der die Vorgaben des Stadtrates umsetzt.
Seit 2004 hat der Republikaner Mick Cornett die Position des Stadtoberhaupts inne. Nach der Wahl im März 2014, als er eine Zustimmung von 65,7 % erhielt, regiert er in vierter Amtszeit.


=== Partnerstädte ===
China Volksrepublik Haikou, China
Mexiko Puebla, Mexiko
Taiwan Tainan, Taiwan
Taiwan Taipei, Taiwan
Israel Jehud, Israel
Russland Uljanowsk, Russland


== Infrastruktur ==


=== Verkehr ===

Das übergeordnete Fortbewegungsmittel ist das Auto, mit dem etwa 94 % der Einwohner ihren Arbeitsweg bestreiten. Öffentliche Verkehrsmittel spielen dagegen nur bedingt eine Rolle: Von lediglich 0,7 % (Stand: 2013) der Arbeitnehmer werden sie auf dem Weg zum Arbeitsplatz genutzt (ohne Taxis).


==== Straßennetz ====
Oklahoma City ist gut an das US-amerikanische Fernstraßennetz angebunden. Durch das Stadtgebiet verlaufen drei Interstate Highways. Die Interstate 35 verbindet die Stadt mit Wichita, Kansas City und Minneapolis im Norden, sowie Dallas, Fort Worth, Austin und San Antonio im Süden. Die Interstate 40 stellt Anschluss nach Amarillo und Albuquerque im Westen, sowie Little Rock, Memphis, Nashville und Knoxville im Osten her. Die Interstate 44 schafft Anschluss nach Wichita Falls im Südwesten und Tulsa, Springfield und St. Louis im Nordosten.
Zudem verläuft die weltberühmte Route 66 durch das Stadtgebiet.


==== Luftverkehr ====
In der Nähe befinden sich der Will Rogers World Airport und der kleinere Wiley Post Airport, der momentan umgebaut wird. Die Flughäfen sind nach zwei Personen benannt, die in Alaska bei demselben Flugzeugabsturz ums Leben gekommen sind. Der Will Rogers World Airport ist mit täglich 12.000 Reisenden (Stand: 2014) Oklahomas wichtigster Zivilflughafen und fliegt die größten Städte der USA an, darunter New York City, Los Angeles, Chicago, Houston und Phoenix.


==== Öffentliche Verkehrsmittel ====
Am Santa Fe Depot fährt zweimal täglich der Heartland Flyer der Bahngesellschaft Amtrak nach Fort Worth, Texas ab. Dort existieren Anschlüsse nach Dallas, Chicago und San Antonio.Innerhalb des Stadtgebiets hat die Busgesellschaft Embark ein Liniennetz aufgebaut, das vom Knotenpunkt im Zentrum Downtowns bis zu den Vororten reicht. Von den insgesamt 19 Linien fahren 16 von Montag bis Samstag und 3 nur werktags.Im September 2013 verabschiedete der Stadtrat den Beschluss zur Wiedereinführung einer Straßenbahn. Es soll im ersten Bauabschnitt ein Schienennetz von etwa sieben Kilometern Länge geschaffen werden. Baubeginn war am 7. Februar 2017. Inzwischen wurden weitere Ausbaupläne gutgeheißen, Anschlussverbindungen an den Fährverkehr seien geplant.Die Straßenbahn Oklahoma City wurde am 14. Dezember 2018 eröffnet.


==== Fährverkehr ====
Von April bis Dezember wird der Oklahoma River mit Fähren befahren.


=== Wasserversorgung ===
In den 1910er-Jahren wurde mit der Planung eines ersten Stausees zur Wasserversorgung der Bevölkerung begonnen. Im Jahr 1918 wurde der Lake Overholser fertiggestellt. Der Bau des Lake Hefners wurde während des Zweiten Weltkriegs unterbrochen und 1947 vollendet. 15 Jahre später folgte der Lake Stanley Draper. Diese drei Seen, die sich im Stadtgebiet befinden, sind noch heute bedeutend für die Wasserverteilung. Anfang der 1960er-Jahre ließ die Stadt einen See in 160 Kilometer Entfernung bauen und das Wasser mithilfe einer Pipeline nach Oklahoma City transportieren. Im Jahr 1983 wurde mit dem Sardis Lake ein weiterer Stausee fertiggestellt, der außerhalb der Stadt liegt.


=== Bildung ===
In Oklahoma City gibt es mehrere Hochschulen. Das größte College der Stadt und eines der größten im Staat ist das Oklahoma City Community College, an dem 22.000 Studenten eingeschrieben sind.Mit einer Studentenzahl von 7.000 ist die Oklahoma State University–Oklahoma City die größte Universität der Stadt. Hinzu kommt die Oklahoma City University, an der insgesamt 4.000 Studenten eingeschrieben sind, und die Oklahoma Christian University mit 2.000 Studenten.Außerdem sind in der Stadt mehrere Außenstellen anderer großer Universitäten des Staates angesiedelt. So befindet sich in Downtown die Musikakademie der University of Central Oklahoma und in der Nähe ist auch die medizinische Fakultät der University of Oklahoma, deren Hauptcampus im nahegelegenen Norman liegt.


== Wirtschaft ==

Die Stadt Oklahoma City gilt aktuell als besonders starker Industriestandort. Seit Jahren verzeichnet die Wirtschaft Oklahoma Citys ein rasches Wachstum, das 2011 und 2012 bei jeweils über vier Prozent lag und im Jahr darauf 3,9 % betrug. Die Arbeitslosenquote, die dem American Community Survey zufolge bei 3,9 % liegt, ist eine der niedrigsten in den USA. Der Trend ist seit Jahren rückläufig.Das Medianeinkommen eines Arbeitnehmers beträgt leicht unterdurchschnittliche 28.000 US-$ im Jahr. Laut amerikanischem Arbeitsministerium beträgt der durchschnittliche Stundenlohn innerhalb der Metropolregion Oklahoma Citys 20,47 $ (Stand: 2013). Dieser Wert liegt um 8 % unterhalb des Bundesschnitts.Die wichtigsten Arbeitgeber des Großraums Oklahoma City sind (Stand: Juli 2014) der Staat Oklahoma (42.000 Arbeitsplätze), die Tinker Air Force Base (27.000) und die University of Oklahoma (11.900).Die Metropolregion von Oklahoma City erbrachte 2016 eine Wirtschaftsleistung von 65,7 Milliarden US-Dollar. Im Großraum der Stadt wird ein großer Teil der ökonomischen Aktivität des Bundesstaats Oklahoma erbracht.


=== Industrie ===
Oklahoma City liegt direkt über einem Ölfeld, weswegen der Förderung von Erdöl und Erdgas bereits seit den 1930er-Jahren eine Schlüsselrolle zukommt. Insgesamt gibt es etwa 1.400 Öl- und Gasquellen innerhalb der Stadtgrenzen (Stand: 2014), von denen etwa 1.100 aktiv sind. Sie befinden sich größtenteils auf Privatgrundstück. Jährlich nimmt die Stadt 3,7 Millionen US-$ durch Lizenzvergaben ein (Mittelwert von 2004 bis 2014). Die Mineralölunternehmen Devon Energy und Chesapeake Energy, die zu den größten Erdgasproduzenten der USA zählen, gehörten 2014 zu den 500 umsatzstärksten Unternehmen der Welt. Hinzu kommt OGE Energy, das in den Top 1000 gelistet wird. Kritisch sehen Experten und Politiker die übermäßige Abhängigkeit der Stadt von der Energieindustrie. Auch der Bürgermeister hat dieses Problem eingeräumt.Darüber hinaus war die Automobilindustrie mit einer Produktionsstätte von General Motors fast 30 Jahre in der Stadt vertreten. In der Fabrik waren in der Spitze bis zu 5.000 Arbeiter angestellt. Im Zuge einer Neuausrichtung wurde das Werk im Jahre 2001 vorübergehend geschlossen. Ab 2002 wurden nur noch Geländelimousinen produziert. Im November 2005 verkündete der Konzern das Aus der Produktionsstätte für den Anfang des folgenden Jahres. Dabei gingen mehr als 2.000 Arbeitsplätze verloren.


=== Landwirtschaft ===

Die Viehzucht zählt zu den traditionsreichsten Wirtschaftszweigen der Region. Die Oklahoma National Stock Yards Company nahm im Oktober 1910 ihren Betrieb auf. An den Viehmärkten der Stadt wird besonders mit Rindern gehandelt. Die Auktionen, die noch heute regelmäßig stattfinden, sind offen für Besucher.


=== Ansässige Unternehmen ===
OGE Energy, Elektrizitätsversorger
Devon Energy, Energieunternehmen
Love’s Travel Stops & Country Stores, Tankstellenbetreiber
Taco Mayo, Franchise-Systemgastronomie-Kette
Chesapeake Energy, Erdöl- und Ergasproduzent
SandRidge Energy, Erdöl- und Ergasproduzent


== Kultur und Sehenswürdigkeiten ==


=== Museen ===
Zu den bedeutendsten Museen der Stadt gehören das Science Museum Oklahoma, das Oklahoma City National Memorial Museum, das National Cowboy & Western Heritage Museum, das Sam Noble Oklahoma Museum of Natural History und das Museum of Osteology.


=== Sport ===


==== Mannschaftssport ====
Aushängeschild der Stadt ist das Basketballteam Oklahoma City Thunder, das in der NBA spielt. Seine Heimspiele trägt es in der Chesapeake Energy Arena aus. Die Oklahoma City Barons sind die bekannteste Eishockeymannschaft der Stadt und nehmen am Spielbetrieb der American Hockey League teil. Ihre Spiele finden im Cox Convention Center statt. Die Oklahoma City Dodgers spielen in der Minor League Baseball. Ihre Heimspiele werden im Chickasaw Bricktown Ballpark ausgetragen. Im Profifußball wird die Stadt vom Franchise Oklahoma City Energy, das in der USL Professional Division spielt, repräsentiert. Der Klub trägt seine Spiele im Pribil Stadium aus.


==== Weitere Sportarten ====
Beliebt ist hier außerdem der Pferdesport. Die Stadt bezeichnet sich selbst als Welthauptstadt der Pferdeshows („Horse Show Capital of the World“). So findet jedes Jahr im November die Weltmeisterschaft der American Quarter Horse Association in Oklahoma City statt. Austragungsort der meisten Wettkämpfe ist die Oklahoma State Fair Arena. Der Oklahoma River ist offizieller Trainingsplatz der olympischen und paralympischen Ruder- und Kanumannschaften der USA und Schauplatz bedeutsamer Wettkämpfe dieser Sportarten.


=== Medien ===
Mit der konservativen Tageszeitung The Oklahoman ist das größte Printmedium des Staates in Oklahoma City ansässig. Nach werktäglicher Auflage liegt es mit etwa 125.000 Exemplaren bundesweit auf Platz 58 der größten Tageszeitungen (Stand: Juni 2013).Alle großen Fernsehnetworks haben in Oklahoma City lokale Ableger:

NBC: KFOR-TV (Channel 4)
ABC: KOCO-TV (Channel 5)
CBS: KWTV-DT (Channel 9)
PBS: KETA-TV (Channel 13)
FOX: KOKH-TV (Channel 25)
CW: KOCB (Channel 34)
MyNetworkTV: KSBI (Channel 52)


=== Freizeit und Erholung ===

Der 1999 fertiggestellte Bricktown Canal ist eine Touristenattraktion. In Wassertaxis werden Rundfahrten angeboten.
Der öffentlich-rechtliche Oklahoma City Zoo zählt jährlich bis zu eine Million Besucher. Er ist der Lebensraum von 500 Arten und wurde bereits im Jahr 1902 als kleine Menagerie eröffnet.Der Vergnügungspark Frontier City hat das Thema Western und liegt direkt an der Interstate 35. Er bietet unter anderem Achterbahnen, ein Riesenrad, Liveshows und Konzerte.


=== National Park Service ===

Der National Park Service weist 107 Bauwerke und Stätten der Stadt im National Register of Historic Places (NRHP) aus (Stand 10. November 2018).


== Persönlichkeiten der Stadt ==


== Weblinks ==

City of Oklahoma City (englisch)
Tourismuswebseite von Oklahoma City (englisch)
Informationen auf deutsch


== Einzelnachweise ==