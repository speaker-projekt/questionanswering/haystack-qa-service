Grün ist der Farbreiz, der wahrgenommen wird, wenn Licht mit einer spektralen Verteilung ins Auge fällt, bei der fast nur Wellenlängen zwischen 520 und 565 nm vorkommen. Licht mit dieser Eigenschaft kann auch als Körperfarbe remittiert sein.


== Etymologie ==
In der deutschen Sprache ist das Wort grün mit althochdeutsch gruoen verbunden, das „wachsen“, „sprießen“ oder „gedeihen“ bedeutete.


== Farblehre ==

Der Zusammenhang zwischen Farbbezeichnung und Farbton ist sprachlich nicht bindend, die hier angezeigten Muster sollen eine „ungefähre“ Vorstellung beim Betrachter ergeben.
Die „Farbe Grün“ ist (neben Rot und Blau) eine Grundfarbe der additiven Farbmischung. Bei einer subtraktiven Farbmischung erreicht man Grün durch Mischung von Cyan und Gelb. Die Komplementärfarbe ist Magenta.
Die Listen grüner Farbmittel und Pigmente enthalten Farbkörper zum Einfärben von Substraten.


== Grün der Natur ==


=== Ableitungen ===

In der Natur ist Grün eine häufige Farbe. Durch die große Anzahl von Pflanzen mit Chlorophyll sind Farbnamen wie Grasgrün, Lindgrün, Moosgrün, Apfelgrün, Laubgrün oder Maigrün definiert. Dabei ist leicht eine Breite der natürlichen Grüntöne festzustellen (siehe Galerie ganz unten). Am Ende der Vegetationszeit verringert sich die Chlorophyllmenge und die gelben und roten Naturfarbstoffe im Laub kommen zur Wirkung. Wegen dieser Färbung der Vegetation wird mit der Farbe Grün Leben und Wachstum assoziiert. Insbesondere im Frühling kündigt „das Grün“ den Beginn des neuen Wachstums an. Grün ist sichtbar die Farbe der Frische und der Natürlichkeit und im übertragenen Sinn der Hoffnung und der Zuversicht. Im Winter ist Tannengrün im Haus und die damit dekorierten Stuben und Fenster und der Weihnachtsbaum der Ersatz des Sommergrüns.
Das Ergrünen (im Frühjahr) ist mit der Farbe Grün verbunden. Daraus ist die im Mittelalter bekannte Symbolik für eine beginnende Liebe entstanden. Folgend die Worte „die grüne Seite“ als das Bessere des Menschen und – als Verneinung – die Redensart „jemandem nicht grün sein“. Der älteste Beleg findet sich in einer mittelalterlichen Passion über die heilige Katharina, in der es über ihre Feinde heißt: „Was gegen ir vil ungrune“.
Zuweilen wird „Grün“ aufgrund der Farbe unreifer Früchte mit Unerfahrenheit oder Unreife auch negativ assoziiert. Darauf beziehen sich Redensarten wie „grün hinter den Ohren sein“ oder die Bezeichnung „Grünschnabel“. Auch das „Greenhorn“ für den Neuling entstammt dieser Farbdeutung.


=== Umweltschutz, Politik und Wirtschaft ===
Das grüne Chlorophyll der Pflanzen, oft grafisch als Blatt dargestellt, ergab die Symbolik für Natur und Umweltschutz. So wurde es in Deutschland die Symbolfarbe und Namensgeber der Partei Die Grünen. International nutzen aus der Umweltbewegung hervorgegangene Parteien das Grün ebenfalls als Symbolfarbe.
In Anlehnung an diesen Naturbezug wird Grün als Symbol für Nachhaltigkeit und Menschen- und Bürgerrechte genutzt. Der englische Begriff Green Goal in der Doppelbedeutung von „grünes (Fußball-)Tor“ und „grüne Zielsetzung“ wird für Umweltschonung bei Fußballgroßveranstaltungen verwendet. Weitere Begriffe mit diesem Bezug sind „grüne Ökonomie“, „grüne Informationstechnologie“ oder „green technology“.


== Grüne Pigmente ==
Grüne Pigmente, die zum Beispiel als Farbmittel in der Malerei verwendet werden, sind oft anorganische Substanzen, die Chrom (etwa Chromdioxid), Kupfer (basisches Kupferkarbonat, 2CuCO3.Cu(OH)2) oder Kobalt (Cobalt-Chrom-Grün-Spinell, CoCr2O4) enthalten. Dafür wurden vor allem in der Vergangenheit natürlich vorkommende Mineralien feinst vermahlen wie etwa Malachit, Chrysokoll oder Dioptas. Auch natürliche mineralische Verwitterungsprodukte wie Veroneser oder Böhmische Grüne Erde finden Anwendung. Das berühmt-berüchtigte Schweinfurter Grün, ein Kupferarsenitacetat (C4H6As6Cu4O16), ist hingegen im Handel nicht mehr frei verkäuflich.


== Grün als Symbol ==


=== Christentum ===
Im Christentum ist Grün mit dem Bezug zum Frühlingsgrün die Farbe der Auferstehung, es ist die Osterfarbe. Noch weiter verallgemeinernd steht Grün in Irland für den Katholizismus und im Besonderen für den Saint Patrick’s Day. Bischöfe führen in ihrem Wappen einen grünen Prälatenhut, weil die Bischofsfarbe bis 1867 grün war.
Grün ist außerdem eine liturgische Farbe. In der römisch-katholischen Kirche wird Grün seit dem Zweiten Vatikanischen Konzil in der Zeit im Jahreskreis getragen. Die evangelischen Kirchen verwenden Grün in der Epiphaniaszeit, der Vorfastenzeit und der Trinitatiszeit.


=== Islam ===

Grün ist die Farbe des Islam. Der Prophet Mohammed soll sich bevorzugt grün gekleidet haben. Zweifellos hat diese Vorliebe mit Grün als Farbe von Vegetation zu tun, die in einer Wüstenregion, wie dem Ursprungsgebiet des Islam in besonderer Weise für Leben, Überleben und das Paradies steht. Dementsprechend sind Schmuckelemente in Moscheen bevorzugt in Grün gehalten. Viele Moscheen im Nahen Osten werden nachts grün beleuchtet. Die Flaggen vieler islamischer Staaten enthalten Grün, darunter prominent die Flaggen von Mauretanien, Saudi-Arabien und zwischen 1977 und 2011 die Flagge Libyens.


=== China ===
In China ist die Farbe Grün gleichfalls Symbol für Leben, Frühling und Osten. Im Rahmen der traditionellen Fünf-Elemente-Lehre wurden – und werden teilweise immer noch – bestimmte Farben bestimmten Dingen, Formen und Erscheinungen zugeordnet.


== Signalfarbe ==
Grün bezeichnet als Signalfarbe das Normale, Unproblematische, Positive oder Ordnungsgemäße. Grün wird benutzt, um Vorgänge zu kennzeichnen, die funktionieren oder erlaubt sind. So steht grünes Licht geben allgemeiner für die Freigabe eines Vorganges, wie an der Verkehrsampel. Bei einer grünen Welle sind mehrere Ampeln eines Straßenzuges in Grünphase geschaltet.
Auf der Messskala von Anzeigeinstrumenten gibt es häufig den grünen Bereich, der den ordnungsgemäßen Betrieb markiert, im Gegensatz zum roten Bereich für einen unerlaubten Zustand. Bei technischen Geräten signalisiert meist eine grüne Leuchtdiode den Betrieb und eine rote Stillstand. Auch bei Bedientasten, beispielsweise am Handy, hat sich diese Farbgebung durchgesetzt.
In deutschen Behörden und Ämtern herrscht ein streng hierarchischer Farbcode für Aktenvermerke, Paraphen oder Anweisungen. Dabei ist Grün stets der obersten Hierarchiestufe vorbehalten, gefolgt von Rot und Blau. Für ein Ministerium bedeutet dies also, dass Grün dem Minister zugeordnet ist, Rot seinen Staatssekretären.


== Gift ==

Intensive Grüntöne werden als Giftgrün bezeichnet. Lange Zeit waren die farbintensiven und dauerhaften Grünpigmente allen voran das arsenhaltige Schweinfurter Grün giftige Pigmente, wie auch Chromgrün, Kupferacetat (auch Grünspan genannt) und andere Kupferpatinen. Weniger giftig waren nur wenig farbintensive grüne Erden. Die Bezeichnung „giftgrün“ wurde von den Brüdern Grimm in deren Wörterbuch anhand der Schrift Siegmund Suevus Spiegel des menschlichen Lebens bereits für das Jahr 1588 nachgewiesen.Die Gesichtsfarbe grün (= fahl) steht wegen des fehlenden Blutrots für Krankheit, sowohl von Körper als auch für die kranke Seele. In diesem Sinne ist die Farbe Grün als „giftig“ zusätzlich in der traditionellen abendländischen Kultur besetzt. Übertragend steht Grün sodann für Gier und Neid, wie in der Redensart „Grün ist die Gier“ oder umgangssprachlich auch „Grün vor Neid“ (neben „Gelb vor Neid“).


== Verkehr ==
Automobile

Anfang des 20. Jahrhunderts wurden bei Rennsportveranstaltungen für die Wagen der einzelnen Nationen Farben vorgegeben. Für britische Fahrzeuge war grün vorgeschrieben und das British Racing Green gehört noch zu den klassischen Farben britischer Fahrzeuge.
Ampel
Das Ampelgrün signalisiert Erlaubnis im Sinne der bejahenden Wirkung des Farbtones.
Eisenbahn
Ein dunkler Grünton (flaschengrün, chromoxidgrün, tannengrün) wurde seit Ende des 19. Jahrhunderts bei vielen Eisenbahngesellschaften zur Standardfarbe für Reisezugwagen, teilweise auch für Lokomotiven, vor allem für Elektrolokomotiven. Diese Farbgebung beruht hierbei auf der Dauerhaftigkeit und Wirtschaftlichkeit der einsetzbaren Pigmente, zudem gute Lichtbeständigkeit und geringe Schmutzanfälligkeit. Seit den 1970er Jahren wurden die grünen Anstriche zunehmend von helleren und kräftigeren Farben, dazu unter Eisenbahnfarben, verdrängt.


== Kampfsport ==

In vielen Kampfkünsten – wie Jiu Jitsu, Ju-jutsu, Judo, Taekwondo und Karate oder Kung Fu – wird ein Gürtel (jap. Obi), als Teil der Kampfsportkleidung (jap. Keikogi) getragen. Der grüne Gurt repräsentiert den Kenntnisstand des Budōka und ist den unteren bis fortgeschrittenen Schülergraden (jap. Kyū-Grad) vorbehalten.


== Heraldik, Phaleristik und Vexillologie ==

In der Heraldik (Wappenkunde) zählt Grün zu den klassischen Tinkturen und gilt im Unterschied zu den Metallen Gold und Silber als Farbe. Das Ordenszeichen des Lazarus-Ordens besteht aus einem grünen Kreuz. Auch das Kreuz des Ritterorden von Avis war grün. Grün kommt zudem in zahlreichen Flaggen vor, so der Brasiliens, Indiens, Irlands, Italiens, Mexikos, Nigerias, Portugals oder Saudi-Arabiens.


== Kartenspiel ==
Im Deutschen Blatt wird die Spielfarbe Pik des französischen Blattes als „Grün“ bezeichnet. Es sind daneben Worte als grüne Kartenfarbe auch beschreibende Begriffe „Laub“, „Blatt“ und in Bayern „Gras“ im Gebrauch.


== Bekleidung und Design ==
Die Kleidung im Operationssaal von Krankenhäusern ist in neuerer Zeit grün. Nach allgemeiner Ansicht fallen somit rote Blutflecke nicht so auf wie auf weißen Kitteln. Insbesondere hat grüne Kleidung den Vorteil, das grelle Licht im OP nicht so stark zu reflektieren. Der Nachbildeffekt auf weißem Hintergrund, wenn Chirurgen lange auf ihre meist rote Arbeit gesehen haben, fällt auf Grün geringer aus.
Die Tarnkleidung vieler Armeen ist grün und imitiert damit das grüne Chlorophyll. Es sind dafür Pigmente im Einsatz, die das gesamte Absorptionsspektrum einschließlich des infraroten Anteils des Chlorophyllgrüns imitieren. Es wurden zunächst spezielle Pigmentmischungen genutzt und im Weiteren gezielt laubfarbige Einzelpigmente entwickelt. Durch entsprechende Kupplungsgruppen werden aus diesen Textilfarbstoffe für unterschiedliche Substrate produziert.
Rotweinflaschen bestehen meist aus grünem Glas, das starken Lichteinfall und den Rotwein verfälschende Farbanteile verhindert.


== Galerie (Naturtöne) ==
hellgrün (Birke)
		
		
dunkelgrün (Efeu)
		
		
lindgrün / zartgrün (Linde)
		
		
silbergrün (Silberweide)
		
		
moosgrün (Moos)
		
		
apfelgrün
		
		
grasgrün (Gras)
		
		
tannengrün und blaugrün (Tannen)
		
		


== Siehe auch ==
Grün und Blau in verschiedenen Sprachen
Grüner Knopf
Grünland ist die landwirtschaftlich genutzte Fläche mit Gräsern als Dauerkultur
Grünspan ist ein gebräuchlicher Begriff für Kupferacetat, er wird fälschlich für die Überfärbung von Kupferblechen durch basische Kupferverbindungen, die Kupferpatina genutzt.
Grüner Blitz ist ein Naturphänomen
Die Grüne Wiese bezeichnet im Jargon der Stadtplanung eine Planung und Bebauung auf Flächen, die zuvor nicht zum Siedlungsbereich der Stadt oder der Gemeinde gehörten.
Dasselbe in Grün ist eine bekannte Redewendung.
Die Esperantoflagge ist in grün gehalten.
Im Golf ist Grün das Zielgebiet.
Als Tschitscheringrün bezeichnet man umgangssprachlich verschiedene „undefinierbare“ Grüntöne.


== Literatur ==
Eva Heller: Wie Farben wirken. Farbpsychologie, Farbsymbolik, Kreative Farbgestaltung. 3. Auflage. Rowohlt, Reinbek bei Hamburg 2006, ISBN 3-499-61960-1, S. 71–85 (Erstausgabe:  1999). 
Michael Kröger: Grün stört. Im Fokus einer Farbe. Marta Herford, Herford 2016, ISBN 978-3-938433-24-9 (Ausstellungskatalog).


== Weblinks ==

T. Seilnacht: Grün: Geschichte und Bedeutung. In: Naturwissenschaftliches Arbeiten – Seilnachts Didaktik der Naturwissenschaften. seilnacht.com; abgerufen am 9. November 2009 
Katharina Andres-Wilhelm: Vom Rätsel und der Pracht der Farbe Grün. modeschule-metzingen.de; abgerufen am 9. November 2009 
Assoziationen zur Farbe Grün. metacolor.de; abgerufen am 9. November 2009 
Farbimpulse.de: Was giftgrün mit ungesunden Farben zu tun hat. Abgerufen am 12. Mai 2012 
Farbanmutungen nach Nationen – kulturspezifische Farbbedeutungen. Abgerufen am 9. November 2009 


== Einzelnachweise ==