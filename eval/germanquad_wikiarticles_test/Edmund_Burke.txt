Edmund Burke (Aussprache: [bə:k]) (* 1. Januarjul. / 12. Januar 1729greg. in Dublin; † 9. Juli 1797 in Beaconsfield) war ein irisch-britischer Schriftsteller, früher Theoretiker der philosophischen Disziplin der Ästhetik, Staatsphilosoph und Politiker in der Zeit der Aufklärung. Er gilt als geistiger Vater des Konservatismus.


== Der Literat und Ästhetiker ==
Burke studierte von 1743 bis 1750 klassische Literatur und Geschichte am Trinity College Dublin. Ein begonnenes Rechtsstudium vollendete er nicht. Sein schriftstellerisches Wirken begann 1756 mit dem Werk A Vindication of Natural Society, das von manchen als Satire, von anderen als „eine anarchistische Kritik der bestehenden Gesellschaftsordnung“ gelesen wird. 1757 veröffentlichte er A philosophical enquiry into the origin of our ideas of the sublime and beautiful (deutsch: Philosophische Untersuchung über den Ursprung unserer Ideen vom Erhabenen und Schönen), in dem er erstmals den Begriff des Erhabenen als eine ästhetische Kategorie neben der des Schönen einführte. Insbesondere Immanuel Kant griff diese Einteilung auf, ersetzte aber die empirische Begründung durch eine transzendentale.


== Der Politiker ==
Burke arbeitete von 1765 bis 1766 als Privatsekretär von Lord Rockingham, dem Ersten Schatzhalter (First Lord of the Treasury). Im Laufe seiner politischen Karriere wurde Burke in London ein Mitglied im Bund der Freimaurer. Seine Loge war die Jerusalem Lodge Nr. 44. Ab 1765 war Burke für verschiedene Wahlkreise Abgeordneter des britischen Unterhauses. Er profilierte sich als scharfsinniger Vordenker und brillanter Rhetoriker.
Ein zusammenhängend geschriebenes politisches Werk gibt es von ihm nicht. Seine politischen Schriften setzen sich aus Kampfschriften und -reden zusammen:

„Thoughts on the case of the present discontent“ von 1770, eine Programmatik der oppositionellen Whigs-Fraktion gegen vermeintliche Verfassungsverstöße des Königs
mehrere Schriften gegen die Steuer- und Kolonialpolitik der britischen Regierung in Amerika
sein wichtigstes Werk: „Reflections on the Revolution in France“, „Betrachtungen über die Revolution in Frankreich“ von 1790
weitere Schriften richten sich gegen die Politik des Generalgouverneurs in Indien.In seinem Hauptwerk, den „Reflexionen“ von 1790, formulierte Burke eine scharfe Kritik an den damals in Frankreich nach der Revolution von 1789 herrschenden Zuständen und Entwicklungen, die schließlich in der Terrorherrschaft der Jakobiner 1793/1794 mündeten.
Burke hielt es für unvorstellbar, dass eine Regierung von „500 Advokaten und Dorfpfarrern“ dem Willen einer Masse von 24 Millionen und deren recht verschieden gearteten Belangen gerecht werden könne. Die damaligen Machthaber betrachtete er mit Geringschätzung und bezeichnete sie als untragbar, aber immerhin als aufgewertet bezeichnet durch die abgefallenen Angehörigen höherer Stände, die nun an der Spitze dieses Kreises stehen. Die naturgegebenen Verhältnisse, und damit Recht und Ordnung, sind für ihn einer ochlokratischen Ordnung zum Opfer gefallen, und schließlich bleibe auch die Vernichtung des Eigentums unvermeidlich.
Bei dem Versuch der siegreichen Vordenker der Aufklärung, Frankreich in eine demokratische Form zu pressen, sei es zerstückelt worden. Für höchst bedenklich hält Burke die Einteilung in 83 Départements, die er als Republiken aufgefasst wissen will, die ihrerseits autonome Bestrebungen hegen und kaum von einer Zentralherrschaft unterworfen werden und auch nicht zugunsten der Republik von Paris verordnete Einschränkungen in Kauf nehmen wollten. Dabei werde die Republik Paris nichts unversucht lassen, um ihren Despotismus erstarken zu lassen.
Burke stand mit seiner skeptischen, den Rationalismus in der Politik ablehnenden Haltung in scharfem Gegensatz zu Jean-Jacques Rousseau, auf den sich die Vordenker der Französischen Revolution beriefen. Der Versuch, die Grundsätze des gesellschaftlichen Zusammenlebens a priori festzulegen, müsse an der objektiven Realität und der menschlichen Natur scheitern, so Burke.


== Der Staatsphilosoph ==


=== Menschenbild ===
Burke sieht den Menschen als unvollkommenes Wesen, das erst in der Gemeinschaft, im Staat, sein volles Menschsein erlangt. Der Mensch sei geprägt durch seine Vernunfts- und Gefühlsnatur. Seine Vernunft sei allerdings begrenzt und auch innerhalb der Menschheit unterschiedlich ausgeprägt. Die Menschen seien nicht gleich. Nur im gegliederten Staat sei es möglich, die Vernunft zu vervollkommnen. Burke lehnt das unbegrenzte Vertrauen der Aufklärer in die Vernunft des einzelnen Menschen ab. Seine Konzeption der Menschenrechte wurde in der Folgezeit von Thomas Paine scharf kritisiert.


=== Staatsvorstellung ===
Burkes Menschenbild relativiert die Gesellschaftsvertragstheorien der Aufklärer. Die Hierarchie eines Staates sei natur- und gottgegeben. Der Ursprung des Staates liege somit hinter einem „heiligen Schleier“. Der Staat mit seiner Form und Gliederung gedeihe und wachse mit der Gesellschaftsstruktur. Burke sieht die Regierungsmitglieder als Repräsentanten des gesamten Volkes, die jedoch nur ihrem Gewissen unterliegen (Trustees) und demnach ein freies Mandat innehaben. Statt revolutionärer Umwälzungen bevorzugt Burke den permanenten Wandel der Verfassung, weshalb er die Französische Revolution entschieden ablehnt.
Wichtig ist Burke ein historisch langsames Wachsen und Verändern, das von der Politik nicht behindert werden soll – aus diesem Grund unterstützt er die Emanzipationsbestrebungen in Nordamerika. In der Dreiteilung von Krone, Ober- und Unterhaus sieht er den besten Schutz vor Despotismus, aber auch vor der Herrschaft des Pöbels. Das freie Mandat des Abgeordneten dient als Schutz vor weitergehender Demokratisierung. In politischen Parteien (hier nur in Fraktionsform) sieht er eine wirksame Eindämmung der Monarchie.
Burke war es, der den in Britisch-Ostindien residierenden Generalgouverneur Warren Hastings 1786 anklagte, den guten Ruf Englands zu schänden. 1795 wurde Hastings freigesprochen.


=== Vater des Konservatismus ===
Da Burke erstmals die Maximen der Konservativen in all ihren Facetten umriss, wird er auch als Vater des Konservatismus bezeichnet. Für die Konservativen, die sich Burke anschließen, existiert eine göttliche oder naturgegebene Weltordnung, die sich auch in der Gesellschaft niederschlägt. In seinen Vorstellungen ist der Mensch unvollkommen und sündig. Es gibt eine körperliche und geistige Ungleichheit unter den Menschen. Eigentum, auch ungleich verteiltes, und das Recht darauf, ist im Sinne des Konservativen ein wichtiger Eckpfeiler einer funktionierenden Gesellschaftsform. Der Konservative erkennt die Kehrseiten des Fortschritts und weiß den Menschen an Tradition, Religion, Mythen und Verfassung gebunden. Bei Burke verläuft die Bindung an eine Tradition über die Idee einer generationenübergreifenden Gemeinschaft. Es gelte, die Erfahrung und das Wissen, die in den überlieferten Institutionen und Gebräuchen gespeichert seien, zu nutzen und weiterzuentwickeln, statt potenziell verheerende radikale Neuerungen umzusetzen: „Wut und Verblendung können in einer halben Stunde mehr niederreißen, als Klugheit, Überlegung und weise Vorsicht in hundert Jahren aufzubauen imstande sind.“Daher kann der demokratischen Mehrheit, die nur die Gegenwart vertritt, das Recht auf einschneidende Neuerungen nach Burkes Ansicht nicht zukommen.


== Fälschlich zugeschriebenes Zitat ==
Einer der bekanntesten (angeblichen) Aussprüche Burkes, „Für den Triumph des Bösen reicht es, wenn die Guten nichts tun!“ („The only thing necessary for the triumph of evil is for good men to do nothing“) wurde u. a. im Vorspann des Films Hitler – Aufstieg des Bösen (2003), im Film Tränen der Sonne (2003) und von Martin Schulz (SPD) in einer Sondersendung des ZDF, das sich dem Europa-Wahlkampf widmete, zitiert. Die Schauspielerin Emma Watson zitierte ebenfalls diesen Ausspruch Burkes im September 2014 in ihrer Rede vor der UNO. Obwohl Burke sich mehrfach sinnverwandt geäußert hat, findet sich der zitierte Satz in keiner seiner Schriften.Das berühmte Zitat stammt aus dem Werk Thoughts on the Cause of the Present Discontents (1770) und lautet: “When bad men combine, the good must associate; else they will fall, one by one, an unpitied sacrifice in a contemptible struggle.”


== Ehrungen ==
Nach ihm ist das Burke County in Georgia benannt. 1784 wurde er zum Fellow der Royal Society of Edinburgh gewählt.


== Schriften ==
Philosophische Untersuchungen über den Ursprung unserer Ideen vom Erhabenen und Schönen. In der klassischen Übersetzung von Friedrich Bassenge neu hrsg. von Werner Strube. Philosophische Bibliothek, Band 324. Meiner, Hamburg 1989. ISBN 978-3-7873-0944-3. Zuerst wahrscheinlich 1757 in London erschienen als A philosophical inquiry into the origin of our ideas of the sublime and beautiful ; with an introductory discourse concerning taste.
Edmund Burke: Über die Französische Revolution. Betrachtungen und Abhandlungen. Manesse, Zürich 1987 ISBN 3717580884; Akademie, Berlin 1991 ISBN 3-05-001755-4. Häufige Reprints. Zuerst 1790 als Reflections on the Revolution in France, And on the Proceedings in Certain Societies in London Relative to that Event, in London. Die bis heute klassische Übersetzung von Friedrich Gentz stammt dem Jahre 1793 (Digitalisat der 2. Aufl.; Digitalisat der neuen Aufl., erschienen in zwei Teilen)
Olaf Asbach, Dirk Jörke (Hrsg.): Edmund Burke: Tradition – Verfassung – Repräsentation: Kleine politische Schriften. De Gruyter: Berlin/Boston 2019, ISBN 978-3-05-004492-7.


== Literatur ==
David Bromwich: The intellectual life of Edmund Burke. From the sublime and beautiful to American independence, Cambridge, Mass. [u. a.]: The Belknap Press of Harvard Univ. Press, 2014, ISBN 978-0-674-72970-4
Emily Jones: Edmund Burke and the Invention of Modern Conservatism, 1830–1914. An Intellectual History Oxford University Press, 2017.
Dieter Oberndörfer, Wolfgang Jäger (Hrsg.): Klassiker der Staatsphilosophie 2. Band. Stuttgart 1971.
Roger Scruton: Burke's Relevance Today (Vortrag in Den Haag 2001) – deutsch als Konservatismus oder Die Aktualität Edmund Burkes in: Sezession. 3/2003, S. 14ff.
Robert Zimmer: Edmund Burke zur Einführung. Junius, Hamburg 1995.


== Weblinks ==

Literatur von und über Edmund Burke im Katalog der Deutschen Nationalbibliothek
Werke von und über Edmund Burke in der Deutschen Digitalen Bibliothek
Eintrag in Edward N. Zalta (Hrsg.): Stanford Encyclopedia of Philosophy.Vorlage:SEP/Wartung/Parameter 1 und weder Parameter 2 noch Parameter 3
Andreas Westendorf (PDF; 74 kB), Exzerpt aus den "Gedanken zur Frz. Revolution", Suhrkamp-Ausgabe S. 28–63


== Anmerkungen ==