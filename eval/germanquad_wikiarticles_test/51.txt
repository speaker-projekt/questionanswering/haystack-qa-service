
== Ereignisse ==


=== Politik und Weltgeschehen ===


==== Römisches Reich ====

Servius Cornelius Scipio Salvidienus Orfitus wird an der Seite von Kaiser Claudius ordentlicher Konsul, wird aber im späteren Verlauf des Jahres von weiteren Suffektkonsuln abgelöst.


==== Partherreich ====
Nach dem Tod von Gotarzes II. folgt ihm Vonones II. als König der Parther auf den Thron. Tacitus beschreibt seine nur wenige Monate dauernde Regentschaft in seinem Werk Annales als „kurz und unrühmlich“. Noch im gleichen Jahr stirbt er und sein Sohn Vologaeses I. kommt auf den Thron. Am Beginn seiner Regierungszeit gibt er seinem Bruder Pakoros den Thron von Medien und einem anderen Bruder Tiridates den Thron von Armenien. Das letztere liefert den Römern einen Vorwand einzugreifen, da sie Anspruch auf Armenien erheben. Der römische Promagistrat Kappadokiens, Julius Paelignus, fällt in Armenien ein und verwüstet das Land. Noch im gleichen Jahr wird Armenien vom iberischen Usurpator Rhadamistos besetzt, der seinen Onkel Mithridates von Armenien getötet und die Macht an sich gerissen hat.


=== Religion ===
Paulus von Tarsus gründet die erste christliche Gemeinde in Korinth.


== Geboren ==
24. Oktober: Domitian, römischer Kaiser, († 96)


== Gestorben ==
Gotarzes II., König der Parther
Sosibius, römischer Gelehrter
Vonones II., König der Parther
um 50: Phaedrus, römischer Fabeldichter (* um 15 v. Chr.)


== Weblinks ==