### Multilingual QA pipeline based on [Haystack](https://haystack.deepset.ai/)

This repository contains a simple Question Answering system based on Haystack: an open-source framework for building search systems that work over document collections.

To use the QA system: first, one must upload a document collection and provide a list of languages (can be single) of the documents to look into. Once the document database is created, it's possible to query the system (ask questions on a natural language).

#### Setting up

1. cd /path/to/haystack_qa_service/
2. docker-compose build haystack (if you're pulling changes)
3. docker-compose -f docker-compose.yml up
(wait until it stops producing logs)

*Note:* if you restart the containers (QA service and Elasticsearch) multiple times, an error from the elasticsearch server may occur. Make sure to run "docker-compose -f docker-compose.yml down" after stopping the system. 

#### Usage

##### **First step**: populating the document database with your data

First, load the model in the service.

```
curl -d "multilingual" localhost:5000/load_model
```

*Notes:*

- it may take a while.

##### **Second step**: populating the document database with your data

* From local folder (a shared volume) 

First, put your data (either file(s) or folder) to data/user_data - it is a shared volume visible for the system. 
Then, initialize the service with a desired parameter ("monolingual" (at the moment for German) or "multilingual"):
```
curl -d "multilingual" localhost:5000/init_with_local_data
```

*Notes:*

- it may take a while, just wait for a "QA pipeline initialized successfully" response before going to the next step;
- you can initialize the multilingual pipeline and still work with the system as it was monolingual, the only difference will be for the German language: a monolingual pipeline employs [gelectra](https://huggingface.co/deepset/gelectra-base-germanquad) model when a multilingual pipeline relies on the one-shot [XLM-RoBERTa](https://huggingface.co/deepset/xlm-roberta-large-squad2).


##### **Third step**: setting the desired languages of the documents to search in:
In case you are interested in answers on multiple languages, provide a list of languages for the system and it will look into documents on those languages. Alternatively, you can provide a single language and the system will work in monolingual manner. You can also ignore this step and the system will detect the language of the provided question automatically, however the detection module is currently quite inaccurate (especially on short questions) and it can lead to misinterpretation of the user intent.
```
curl -d "en, es, de, ro" localhost:5000/set_languages
```

Possible state replies:
- "The system will look into documents on the following languages: *all of the provided languages*."
- "The system will look into documents on the following languages: *valid languages*. There is no documents on languages *absent languages*." It means that there is no documents on one or more languages provided.
- "It is possible to set desired document languages for multilingual pipelines only." Change the pipeline type to multilingual on the initialization step.
- "The provided input is invalid. Correct format: \"en, de, es\" (for example)."


##### **Fourth step**: asking the system a question:
```
curl -d "Who framed Roger Rabbit?" localhost:5000/get_answer
```

Answer predictions are returned as a json. By default, it is not verbose and has the form of \[\{"answer": text span supposedly containing the answer, "score": certainty score, "lang": the language of the document the answer was found in}]. The list is ranked by score value. If "verbose" parameter is set on "True", the output will contain all metadata (more detailed information [here](https://haystack.deepset.ai/components/ready-made-pipelines#extractiveqapipeline-1)).

