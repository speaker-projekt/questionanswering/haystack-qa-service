import subprocess
import time
import os
from pathlib import Path
from haystack import Document


def convert_Michael_file_to_docs(file_path):
    with open(file_path, encoding='cp1252') as f:
        lines = f.readlines()
    docs = []
    counter = 1
    for line in lines:
        doc = Document(line)
        doc.meta['name'] = "document"+str(counter)
        '''
        doc = docobj.from_json(
        	{
			"name": "document"+str(counter), 
			"content": line,
			"content_type": "text", 
			"score": null, 
			"meta": {
				"name": "document"+str(counter)
			}, 
			"embedding": null, 
			"id": "document"+str(counter)
		}
        )
        '''
        docs.append(doc)
        counter = counter +1
    return docs
