import wikipedia
import json
import re
import os
from collections import defaultdict
from pathlib import Path

from haystack.preprocessor.cleaning import clean_wiki_text
from haystack.file_converter.txt import TextConverter

# wikipedia.set_lang("de")
example_titles = ["Berlin", "Bier", "Deutschland"]


"""
for germanquad (handle in a different place):
if title == 'St._John_s (Neufundland)':
    page = wikipedia.page('St._John\'s (Neufundland)', auto_suggest=False, redirect=True)
elif title == 'Karneval: Fastnacht_und_Fasching':
    page = wikipedia.page('Karneval, Fastnacht_und_Fasching', auto_suggest=False, redirect=False)
"""
def download_pages(titles: list, lang: str, path_to_folder: str):
    """
    Downloads Wikipedia articles content into txt files (in format article_title.txt)
    :param titles: list of titles of articles to download
    :param lang: language of target Wikipedia part
    :param path_to_folder: path to the destination folder
    """
    wikipedia.set_lang(lang)
    bad_titles = {"wiki": [], "file": []}
    for ind, title in enumerate(titles):
        print(ind, title)
        page = wikipedia.page(title, auto_suggest=False, redirect=False)
        if ':' in title:
            title = title.replace(':', '_')
        f_out = open(f"{path_to_folder}/{title}.txt", 'w')
        f_out.write(page.content)
        f_out.close()


def extract_titles_germanquad(path_to_data: str) -> list:
    """
    Extracts titles of Wikipedia articles mentioned in the dataset
    and normalizes some peculiar instances found in GermanQuAD
    :param path_to_data: path to the dataset in json format (either test or train)
    :return: list of titles of mentioned Wikipedia articles
    """
    titles = []
    with open(path_to_data) as dataset:
        data = json.load(dataset)
    paragraphs_dicts = data["data"]
    for par_dict in paragraphs_dicts:
        par_dict = par_dict["paragraphs"][0]
        context = par_dict["context"]
        title = re.search(r'^[\w._-]+', context)[0]

        # normalization to the suitable for 'wikipedia' query format
        if re.search(r'__[\w._-]+_$', title):  # St._Helena__Insel_ -> St._Helena (Insel)
            splits = title.split('__')
            title = splits[0] + f' ({splits[1][:-1]})'
        elif re.search(r'__[\w._-]+$', title):  # James_Bond_007__Spectre' -> James_Bond_007: Spectre
            splits = title.split('__')
            title = splits[0] + f': {splits[1]}'

        titles.append(title)
    return titles


def extract_titles_mlqa(path_to_data: str, languages: tuple) -> list:
    """
    Extracts titles of Wikipedia articles mentioned in the dataset
    :param path_to_data: path to the dataset folder containing files in json format (either test or train)
    :return: list of titles of mentioned Wikipedia articles
    """
    title_langs = []
    for data_file in os.listdir(path_to_data):
        data_file_name = data_file.split('context-')[1]
        data_file_name = data_file_name[:-5]
        if data_file_name.startswith(languages) and data_file_name.endswith(languages):
            with open(path_to_data + data_file) as f:
                data = json.load(f)
            paragraphs_dicts = data["data"]
            for par_dict in paragraphs_dicts:
                title_langs.append((par_dict["title"], data_file_name[:2]))
    return list(set(title_langs))


def extract_passages_mlqa(path_to_data: str, save_path: str, languages: tuple):
    """
    Extracts passages of Wikipedia articles mentioned in the dataset
    :param path_to_data: path to the dataset folder containing files in json format (either test or train)
    :return: list of titles of mentioned Wikipedia articles
    """
    path = Path(save_path)
    if not path.exists():
        path.mkdir(parents=True)
    for data_file in os.listdir(path_to_data):
        data_file_name = data_file.split('context-')[1]
        data_file_name = data_file_name[:-5]
        if data_file_name.startswith(languages) and data_file_name.endswith(languages):
            with open(path_to_data + data_file) as f:
                data = json.load(f)
            data_articles = data["data"]
            for article in data_articles:
                title = article["title"]
                title = title.replace('/', '_')
                f_out = open(f"{save_path}/{title}.txt", 'w')
                for ind, paragraph in enumerate(article["paragraphs"]):
                    f_out.write(f'=={ind}==\n')
                    f_out.write(paragraph["context"])
                    f_out.write("\n\n")
                f_out.close()


def check_avg_para_length(path_to_docs: str = '../eval/germanquad_wikiarticles_test'):
    file_paths = [p for p in Path(path_to_docs).glob("**/*")]
    para_count = 0
    total_len = 0
    for path in file_paths[:1]:
        converter = TextConverter()
        doc = converter.convert(file_path=path, meta=None)
        text = doc["text"]
        text = clean_wiki_text(text)
        para_count += len(text.split("\n\n"))
        total_len += len(text)
    return total_len/para_count


# change it to the corresponding one for your machine
# path_to_germanquad = "/home/yana/Documents/qa_service/"
# testset_titles = extract_titles_germanquad(path_to_germanquad + 'GermanQuAD/GermanQuAD_test.json')
# download_pages(list(set(testset_titles)), "de", '../eval/germanquad_wikiarticles_test')

# path_to_mlqa = "/home/yana/Documents/qa_service/test_data/MLQA_V1/test/"
# languages = ("en", "de", "es")
# testset_titles = extract_titles_mlqa(path_to_mlqa, languages)
#
# save_path = '../eval/new_mlqa_wikiarticles_test'
# existing_titles = os.listdir(save_path)
# existing_titles = [tit[:-4] for tit in existing_titles]
#
# remaining_titles = [el for el in testset_titles if el[0] not in existing_titles]
# print(len(remaining_titles))
#
# titles = defaultdict(list)
# for k, v in remaining_titles:
#     titles[v].append(k)
# titles = list(titles.items())
#
# for lang, title_names in titles:
#     download_pages(title_names, lang, save_path)

# ----------------------------------
# monolingual data
# path_to_mlqa = "/home/yana/Documents/qa_service/test_data/MLQA_V1/test/"
# languages = ("en", "de", "es")
# for lang in languages:
#     extract_passages_mlqa(path_to_mlqa, f'../eval/{lang}_mlqa_contexts_test', (lang,))

# ----------------------------------
# bilingual data
path_to_mlqa = "/home/yana/Documents/qa_service/test_data/MLQA_V1/test/"
languages = [("en", "de"), ("de", "es"), ("es", "en")]
for lang_pair in languages:
    extract_passages_mlqa(path_to_mlqa, f'../eval/{lang_pair[0]}-{lang_pair[1]}_mlqa_contexts_test', lang_pair)


