import os
import requests
from flask import Flask, request
from flask_cors import CORS
from service_qa import ServiceQA
from utils import start_es_instance, load_model

app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)

service = ServiceQA()


@app.route('/init_with_local_data', methods=['POST'])
def upload_local_data():
    """
    Initialize the system with the data from local source
    :return: a message containing the status, str
    """
    pipeline_type = request.stream.read().decode("utf-8")
    valid_pipeline_types = ("monolingual", "multilingual")
    if pipeline_type not in valid_pipeline_types:
        msg = f"Pipeline type can be either of {valid_pipeline_types}"
    else:
        msg = service.init_with_local_data(pipeline_type)
    return msg


@app.route('/set_languages', methods=['POST'])
def set_languages():
    """
    Sets a list of desired languages for the documents to search in.
    Required input: list of languages in form "en, es, de" (with any kind of delimiter).
    :return: a message containing the status, str
    """
    langs_str = request.stream.read().decode("utf-8")
    msg = service.set_document_languages(langs_str)
    return msg


@app.route('/load_model', methods=['POST'])
def load_language_model():
    """
    Loads an extractive (reader) model for the provided language.
    Required input: language in form "de".
    :return: a message containing the status, str
    """
    lang = request.stream.read().decode("utf-8")
    msg = load_model(lang)
    return msg


@app.route('/get_answer', methods=['POST'])
def get_answer():
    """
    Returning a list of answers for the question stated in the request
    :return: answers with metadata, json
    """
    query = request.stream.read().decode("utf-8")
    # verbose = bool(request.args.get("verbose", default=False, type=bool))
    result = service.predict_answer(query, False)
    return result


@app.route('/start_es', methods=['GET'])
def start_es():
    """
    Starting an ES server instance (outside Docker env)
    :return: connection status, str
    """
    start_es_instance()
    return requests.get('http://localhost:9200').content


@app.route('/es_conn_check', methods=['GET'])
def es_connection_check():
    """
    Checking if the dockerized Flask app has a connection to the ES server
    :return: connection status, str
    """
    return requests.get('http://elasticsearch:9200').content


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='localhost', port=port, debug=True)


