import csv
import time
from qa_pipeline.monolingual_qa_pipeline import BasicQAPipeline
from service_qa import start_es_instance

# BEFORE RUNNING THE SCRIPT: change ES server name in qa_pipeline/monolingual_qa_pipeline.py to localhost:
# self.document_store = ElasticsearchDocumentStore(host="localhost", username="", password="",
#                                                  index="document", analyzer=lang)


readers = ["roberta"]
retrievers = ["dense"]
reader_path = {"gelectra": "./local_models/gelectra", "roberta": "./local_models/roberta"}


def read_questions(in_path: str) -> list:
    """
    Reading questions from file.
    :param in_path: path to questions in csv format
    :return: list of questions
    """
    questions = []
    with open(in_path, "r") as in_file:
        csv_reader = csv.reader(in_file, delimiter=',')
        for lines in csv_reader:
            questions.append(lines[0])
    return questions


def write_results(predictions: list, out_path: str):
    """
    Write predictions into resulting file.
    :param predictions: predictions from the pipeline
    :param out_path: path to output file with results
    """
    with open(out_path, 'w') as out_file:
        writer = csv.writer(out_file)
        header = ['question', 'answer', 'probability', 'context', 'doc_name']
        writer.writerow(header)
        for qa_pair in predictions:
            writer.writerow('\n\n')
            rows = format_qa_row(qa_pair)
            for row in rows:
                writer.writerow(row)


def initialize_qa_pipeline_eval(data_path: str, reader_model: str, retriever: str) -> BasicQAPipeline:
    """
    Initialization of the pipeline with custom reader and retriever.
    :param data_path: path to source document folder
    :param reader_model: reader model name
    :param retriever: retriever model name
    :return: initialized QA pipeline object
    """
    lang = "german"
    qa_pipeline = BasicQAPipeline(reader_model, retriever, lang)
    qa_pipeline.populate_document_store(data_path)
    return qa_pipeline


def get_predictions(qa_pipeline: BasicQAPipeline, questions: list) -> list:
    """
    Get predictions for the evaluation questions.
    :param qa_pipeline: QA pipeline object
    :param questions: list of questions
    :return: predictions (dictionary)
    """
    preds = []
    for question in questions:
        prediction = qa_pipeline.predict(question)
        answers = prediction.get('answers', None)
        if answers:
            q_a = (question, answers)
            preds.append(q_a)
    return preds


def format_qa_row(qa_pair: tuple) -> list:
    """
    Format a row for output in form of ['question', 'answer', 'probability', 'context', 'doc_name']
    :param qa_pair: tuple of question and prediction
    :return: list of formatted rows (list of strings) ready for output
    """
    qa_rows = []
    question, answers = qa_pair
    for ans in answers:
        qa_row = [question, ans['answer'], ans['probability'], ans['context'], ans['meta']['name']]
        qa_rows.append(qa_row)
    return qa_rows


eval_path = './eval/'
data_path = 'documents'
questions_path = 'eval_questions.csv'
results_path = 'eval_results/'
time_path = 'time_taken.txt'

eval_questions = read_questions(eval_path + questions_path)

try:
    start_es_instance()
except Exception:
    print('ES instance already launched, pay no heed')

with open(eval_path + time_path, 'w') as time_file:
    for reader in readers:
        for retriever in retrievers:
            start_pipeline_time = time.time()
            eval_qa_pipeline = initialize_qa_pipeline_eval(eval_path + data_path, reader_path[reader], retriever)
            end_pipeline_time = time.time()
            pred_list = get_predictions(eval_qa_pipeline, eval_questions)
            end_prediction_time = time.time()
            write_results(pred_list, eval_path + results_path + f'results_{reader}_{retriever}.csv')
            time_file.write(f'Experiment {reader}+{retriever}: \n')
            time_file.write(f'Initialization time: {str(end_pipeline_time - start_pipeline_time)} s \n')
            time_file.write(f'Prediction time: {str(end_prediction_time - end_pipeline_time)} s \n\n')
            eval_qa_pipeline.clear_document_store()  # is it necessary?
