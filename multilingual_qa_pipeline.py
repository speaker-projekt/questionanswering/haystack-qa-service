#from haystack.document_store.elasticsearch import ElasticsearchDocumentStore
from haystack.document_stores import ElasticsearchDocumentStore
#from haystack.preprocessor.cleaning import clean_wiki_text
from haystack.utils import clean_wiki_text
#from haystack.preprocessor.utils import convert_files_to_dicts
from haystack.utils import convert_files_to_docs
from haystack.pipelines import ExtractiveQAPipeline
from haystack.nodes import ElasticsearchRetriever
from haystack.nodes import FARMReader
from langdetect import detect
from translate import Translator
from utils import iso_to_language
from utils2 import convert_Michael_file_to_docs


class MultilingualQAPipeline:
    """A class describing the multilingual Question Answering Pipeline based on deepset's Haystack framework"""
    def __init__(self, reader_model_name_or_path: str):
        """
        :param reader_model_name_or_path: directory of a saved model or the name of a public model
        """
        self.languages = None
        self.document_stores = {}
        # the reader model can work on CPU, but it is recommended that they are run using GPUs to keep query times low
        self.reader = FARMReader(model_name_or_path=reader_model_name_or_path)

    def set_document_languages(self, langs):
        self.languages = langs
        lacking_doc_langs = [lang for lang in langs if lang not in self.document_stores.keys()]
        return lacking_doc_langs

    def populate_document_store(self, data_path: str):
        """
        Populates the document store with documents located in the folder
        :param data_path: path to documents, str
        """
        # # todo/tocheck: do we need to clear all documents?
        # self.clear_document_store()
        # 1) we here assume that  the documents are mixed with no language specified
        #doc_dicts = convert_files_to_docs(dir_path=data_path, clean_func=clean_wiki_text, split_paragraphs=True)
        data_path = 'origin.txt'
        doc_dicts = convert_Michael_file_to_docs(file_path=data_path)
        detected_doc_dicts = {}
        for doc_dict in doc_dicts:
            #lang = detect(doc_dict["text"])
            #print(doc_dict)
            #document_dict = doc_dict.to_dict()
            #print('DICT: ',document_dict)
            #document_json = doc_dict.to_json()
            #print('JSON: ',document_json)

            #break
            #print("-----------------------------------------------------")
            #print(doc_dict.content)
            lang = detect(doc_dict.content)
            #doc_dict["meta"]["lang"] = lang
            doc_dict.meta["lang"] = lang
            print(doc_dict.meta)
            if lang not in detected_doc_dicts:
                detected_doc_dicts[lang] = [doc_dict]
            else:
                detected_doc_dicts[lang].append(doc_dict)
        for lang in detected_doc_dicts.keys():
            language_name = iso_to_language[lang]
            doc_store = ElasticsearchDocumentStore(host="elasticsearch", username="", password="",
                                                   index=f"document_{language_name}", analyzer=language_name)
            doc_store.write_documents(detected_doc_dicts[lang])
            self.document_stores[lang] = doc_store

    # def clear_document_store(self, index):
    #     """
    #     Clears the document index.
    #     """
    #     self.document_store.delete_all_documents(index=index)

    def predict(self, query: str) -> dict:
        """
        Calculates predictions for the given user query for all requested languages (question + filters (TBD))
        :param query: user query containing question
        :return: predictions, {language: answers along with prediction metadata}
        """
        predictions = {}
        if not self.languages:  # results only from docs which are on the same lang as the query
            query_lang = detect(query)  # todo: doesnt work well with short str, change
            print("QUERY LANG", query_lang)
            if query_lang not in self.document_stores.keys():
                # todo turn into an exception (goes into docker log)?
                return f'No documents on {query_lang} language found. If {query_lang} is not the language' \
                       f' the query is in, please set it explicitly with \'set_languages\' command'
            retriever = ElasticsearchRetriever(document_store=self.document_stores[query_lang])
            pipe = ExtractiveQAPipeline(self.reader, retriever)
            predictions[query_lang] = pipe.run(query=query)["answers"]
        else:  # results from docs on the specified languages, the original query language may be excluded
            present_langs = [lang for lang in self.languages if lang in self.document_stores.keys()]
            for lang in present_langs:
                translator = Translator(to_lang=lang)
                translated_query = translator.translate(query)
                retriever = ElasticsearchRetriever(document_store=self.document_stores[lang])
                # todo specify gelectra-based reader for german language
                pipe = ExtractiveQAPipeline(self.reader, retriever)
                predictions[lang] = pipe.run(translated_query).get("answers", [])

        return predictions
