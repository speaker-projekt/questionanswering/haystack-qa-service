#!/usr/bin/python
import argparse
from haystack.reader.farm import FARMReader

if __name__ == '__main__':
    # default: german language model
    parser = argparse.ArgumentParser(description='A script to download a public language model.')
    parser.add_argument("--model_name", default="deepset/gelectra-large-germanquad", type=str, help="Name of a public model.")
    parser.add_argument("--save_dir", default="gelectra", type=str, help="Name of dir where to load the model.")
    args = parser.parse_args()
    FARMReader(model_name_or_path=args.model_name, use_gpu=False).save(args.save_dir)

